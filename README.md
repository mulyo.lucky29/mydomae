# My Domae
##

Domae adalah aplikasi mobile yang menghandle proses Delivery Order pesanan sparepart. 
Aplikasi ini mengawal semua proses dari loading barang sampai dengan pengirimannya yang semuanya di handle secara digital artinya: tadinya mengunakan faktur/kertas berlembar-lembar dan harus ditanda-tangani pada saat serah terima barang, kini hanya menggunakan tablet yang ringkas dibawa-bawa dan bisa beroperasi meskipun tanpa ada koneksi. Proses sinkronisasi data dilakukan setelah kembali ke pool untuk docking tablet.  
 

## Screen Example 

![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mydomae/domae_menu.png?raw=true)

![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mydomae/do_service.png?raw=true)

![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mydomae/do_sign.png?raw=true)

![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mydomae/do_list.png?raw=true)


## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.