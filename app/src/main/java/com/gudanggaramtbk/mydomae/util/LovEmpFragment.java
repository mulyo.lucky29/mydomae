package com.gudanggaramtbk.mydomae.util;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.activity.TProcessDOReceive;
import com.gudanggaramtbk.mydomae.adapter.CustomGEmpLovAdapter;
import com.gudanggaramtbk.mydomae.model.CEmployee;

import java.util.List;


public class LovEmpFragment extends DialogFragment {
    Button btn;
    ListView lv;
    SearchView sv;
    private CustomGEmpLovAdapter adapter;
    List<CEmployee>                   lst_data;
    TextView                          Txt_selected;
    TextView                          TxtTitle;
    TProcessDOReceive activity;
    Dialog                            d;
    Context                           context;

    public void LovEmpFragment(){
        Log.d("[GudangGaram]", "LovEmpFragment");
    }
    public void setContext(Context ctx){
        this.context = ctx;
    }
    public void SetListOfValue(List<CEmployee> lst){
        this.lst_data = lst;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_lov_emp, null);
        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);

        d = getDialog();
        d.setTitle("Lov Title");
        d.getWindow().setLayout(width, height);

        lv  = (ListView) rootView.findViewById(R.id.listView1);
        sv  = (SearchView) rootView.findViewById(R.id.searchView1);
        btn = (Button) rootView.findViewById(R.id.cmd_lov_close);
        TxtTitle = (TextView) rootView.findViewById(R.id.txt_lov_title);

        activity = (TProcessDOReceive) getActivity();
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        adapter = new CustomGEmpLovAdapter(activity, lst_data);
        lv.setAdapter(adapter);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                CEmployee o = adapter.getItemAtPosition(position);
                activity.setLbl_DOS_ReceivedByID(o.getPersonID().toString());
                activity.setTxt_DOS_ReceivedBy(o.getPersonName().toString());
                dismiss();
            }
        });

        // search filter
        sv.setQueryHint("Search Here..");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String txt) { return false; }
            @Override
            public boolean onQueryTextChange(String txt) {
                Log.d("[GudangGaram]", "LovGLokasiFragment :: onQueryTextChange : " + txt);
                adapter.getFilter().filter(txt);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        return rootView;
    }
}
