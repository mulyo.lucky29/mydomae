package com.gudanggaramtbk.mydomae.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.gudanggaramtbk.mydomae.BuildConfig;
import com.gudanggaramtbk.mydomae.service.DownloadTaskAsync;

import java.io.File;

/**
 * Created by luckym on 1/31/2019.
 */

public class DownloadUpdate {
    private SharedPreferences config;
    private Context context;
    private String apkbaseurl;
    private ProgressDialog pdLoading;

    // override constructor
    public DownloadUpdate(SharedPreferences PConfig){
        this.config = PConfig;
    }
    public void setContext(Context context){
        this.context = context;
    }

    public void InstalApk(){
        Intent intent;
        Uri myuri;
        File fileapk;
        String PathName;
        Log.d("[GudangGaram]: ", "APK Install");

        try{
            PathName = Environment.getExternalStorageDirectory().toString() + File.separator + "app-release.apk";
            fileapk  = new File(PathName);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.d("[GudangGaram]: ", "APK Install Higher Version ( >= 7.0)");
                myuri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", fileapk);
                Uri packageURI = Uri.parse("com.gudanggaramtbk.myscanit");

                intent = new Intent(android.content.Intent.ACTION_VIEW, packageURI);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(myuri, "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            else {
                Log.d("[GudangGaram]: ", "APK Install Lower Version");
                myuri = Uri.fromFile(fileapk);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(myuri, "application/vnd.android.package-archive");
            }
            context.startActivity(intent);
            Log.d("[GudangGaram]", "APK Install Done");
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "APK Install Exception : " + e.getMessage());
        }
    }

    public void DownloadApk()
    {
        Log.d("[GudangGaram]", "DownloadUpdate :: DownloadAPK");
        this.apkbaseurl = config.getString("URLUpdater","");
        DownloadTaskAsync d = new DownloadTaskAsync(new DownloadTaskAsync.DownloadTaskAsyncResponse() {
            @Override
            public void PostDownloadAction(String output) {
                Log.d("[GudangGaram]", "APK Download Status " + output);
                if(output == "Finish"){
                    InstalApk();
                }
                //pdLoading.dismiss();
            }
        });
        d.setContext(context);
        d.execute(apkbaseurl);
    }
}


/*
 // deprecated in version 1.4.6

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

public class DownloadUpdate {

    private SharedPreferences config;
    private Context context;
    private String apkbaseurl;
    private ProgressDialog pdLoading;

    // override constructor
    public DownloadUpdate(SharedPreferences PConfig){
        this.config = PConfig;
    }
    public void setContext(Context context){
        this.context = context;
    }

    public void InstalApk(){
        Log.d("[GudangGaram]: ", "APK Install");

        try{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.setData(Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString() + File.separator + "app-release.apk")));
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                context.startActivity(intent);
                Log.d("[GudangGaram]: ", "APK Install Versi >= 7");
            }
            else{
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString() + File.separator + "app-release.apk")), "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                Log.d("[GudangGaram]: ", "APK Install Versi < 7");
            }

        //Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString() + File.separator + "app-release.apk")), "application/vnd.android.package-archive");
        //context.startActivity(intent);
            Log.d("[GudangGaram]: ", "APK Install Done");
        }
        catch(Exception e){
            Toast.makeText(context, "APK Install Exception " + e.toString(), Toast.LENGTH_SHORT).show();
            Log.d("[GudangGaram]: ", "APK Install Exception " + e.toString());
        }
    }

    public void DownloadApk()
    {
        this.apkbaseurl = config.getString("URLUpdater","");
        DownloadTaskAsync d = new DownloadTaskAsync(new DownloadTaskAsync.DownloadTaskAsyncResponse() {
            @Override
            public void PostDownloadAction(String output) {
                Log.d("[GudangGaram]: ", "APK Download Status " + output);
                if(output == "Finish"){
                    InstalApk();
                }
                //pdLoading.dismiss();
            }
        });
        d.setContext(context);
        d.execute(apkbaseurl);
    }
}
*/
