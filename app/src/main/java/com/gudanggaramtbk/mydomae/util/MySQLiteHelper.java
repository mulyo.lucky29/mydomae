package com.gudanggaramtbk.mydomae.util;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.mydomae.holder.HolderDOS;
import com.gudanggaramtbk.mydomae.holder.HolderManifestList;
import com.gudanggaramtbk.mydomae.holder.HolderManifestListLO;
import com.gudanggaramtbk.mydomae.model.CEmployee;
import com.gudanggaramtbk.mydomae.model.ManifestDetail;
import com.gudanggaramtbk.mydomae.model.ManifestHeader;
import com.gudanggaramtbk.mydomae.model.MasterEmployee;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by LuckyM on 9/28/2017.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

    private int CPersonID;
    private String CEmployeName;
    private String CEmployeeNumber;
    private String CEmailAddress;

    private static final String TAG = "MyActivity";

    // ========== init object =====================================
    private static final int DATABASE_VERSION        = 1;
    private static final String DATABASE_NAME        = "MyDomaeDB.db";
    private static final String TABLE_M_EMPLOYEE     = "GGGG_DOMAE_EMPLOYEE";
    private static final String TABLE_M_LOOKUP       = "GGGG_DOMAE_LOOKUP";
    private static final String TABLE_T_MANIFEST_D   = "GGGG_DOMAE_DEVICE_D";
    private static final String TABLE_T_MANIFEST_LO  = "GGGG_DOMAE_DEVICE_LO";
    private static final String TABLE_T_MANIFEST_DO  = "GGGG_DOMAE_DEVICE_DO";

    public String Get_DatabaseName(){
        return DATABASE_NAME;
    }

    // =========== master table employee ==========================
    private static final String KEY_ME_PERSON_ID       = "ME_PersonID";
    private static final String KEY_ME_EMPLOYEE_NAME   = "ME_EmployeeName";
    private static final String KEY_ME_EMPLOYEE_NUMBER = "ME_EmployeeNumber";
    private static final String KEY_ME_EMAIL_ADDRESS   = "ME_EmailAddress";
    private static final String KEY_ME_ATTRIBUTE       = "ME_Attribute";

    // ============ master table lookup ===========================
    private static final String KEY_ML_CONTEXTS        = "ML_Context";
    private static final String KEY_ML_VALUE           = "ML_Values";
    private static final String KEY_ML_DESCRIPTION     = "ML_Description";

    // =============================================================
    private static final String KEY_MLO_ID               = "MLO_ID";
    private static final String KEY_MLO_RAK_PERSIAPAN    = "MLO_RAK_PERSIAPAN";
    private static final String KEY_MLO_VEHICLE_NO       = "MLO_VEHICLE_NO";
    private static final String KEY_MLO_CREATEDATE       = "MLO_CREATED_DATE";
    private static final String KEY_MLO_LOADED_BY        = "MLO_LOADED_BY";
    private static final String KEY_MLO_LOADED_BY_ID     = "MLO_LOADED_BY_ID";

    // =============================================================
    private static final String KEY_MDO_ID                   = "MDO_ID";
    private static final String KEY_MDO_VEHICLE_NO           = "MDO_VEHICLE_NO";
    private static final String KEY_MDO_LOCATION             = "MDO_LOCATION";
    private static final String KEY_MDO_REQUESTOR            = "MDO_REQUESTOR";
    private static final String KEY_MDO_TRX_NUMBER           = "MDO_TRX_NUMBER";
    private static final String KEY_MDO_RECEIVED_BY          = "MDO_RECEIVED_BY";
    private static final String KEY_MDO_RECEIVED_BY_ID       = "MDO_RECEIVED_BY_ID";
    private static final String KEY_MDO_SIGNATURE            = "MDO_SIGNATURE";
    private static final String KEY_MDO_SIGNATURE_FN         = "MDO_SIGNATURE_FN";
    private static final String KEY_MDO_SIGNATURE_FS         = "MDO_SIGNATURE_FS";
    private static final String KEY_MDO_CREATEDATE           = "MDO_CREATED_DATE";
    private static final String KEY_MDO_REAL_RECEIVED_BY     = "MDO_REAL_RECEIVED_BY";
    private static final String KEY_MDO_REAL_RECEIVED_BY_NIK = "MDO_REAL_RECEIVED_BY_NIK";

    // =============================================================
    private static final String KEY_MD_ID                   = "MD_ID";
    private static final String KEY_MD_VEHICLE_NO           = "MD_VEHICLE_NO";
    private static final String KEY_MD_LOCATION             = "MD_LOCATION";
    private static final String KEY_MD_REQUESTOR            = "MD_REQUESTOR";
    private static final String KEY_MD_TRX_NUMBER           = "MD_TRX_NUMBER";
    private static final String KEY_MD_RECEIVED_BY_ID       = "MD_RECEIVED_BY_ID";
    private static final String KEY_MD_RECEIVED_BY          = "MD_RECEIVED_BY";
    private static final String KEY_MD_RAK_PERSIAPAN        = "MD_RAK_PERSIAPAN";
    private static final String KEY_MD_BIN_PERSIAPAN        = "MD_BIN_PERSIAPAN";
    private static final String KEY_MD_ORG_ID               = "MD_ORG_ID";
    private static final String KEY_MD_ITEM_ID              = "MD_ITEM_ID";
    private static final String KEY_MD_ITEM_CODE            = "MD_ITEM_CODE";
    private static final String KEY_MD_ITEM_DESC            = "MD_ITEM_DESC";
    private static final String KEY_MD_QTY                  = "MD_QTY";
    private static final String KEY_MD_QTY_RECEIVED         = "MD_QTY_RECEIVED";
    private static final String KEY_MD_UOM                  = "MD_UOM";
    private static final String KEY_MD_REF_LOADING          = "MD_REF_LOADING";
    private static final String KEY_MD_CHECK_LOADING        = "MD_CHECK_LOADING";
    private static final String KEY_MD_LOADING_DATE         = "MD_LOADING_DATE";
    private static final String KEY_MD_SYNC_LOADING_DATE    = "MD_SYNC_LOADING_DATE";
    private static final String KEY_MD_REF_SENT             = "MD_REF_SENT";
    private static final String KEY_MD_CHECK_SENT           = "MD_CHECK_SENT";
    private static final String KEY_MD_SENT_DATE            = "MD_SENT_DATE";
    private static final String KEY_MD_SYNC_SENT_DATE       = "MD_SYNC_SENT_DATE";
    private static final String KEY_MD_REASON_CODE          = "MD_REASON_CODE";
    private static final String KEY_MD_CREATEDATE           = "MD_CREATED_DATE";
    private static final String KEY_MD_REAL_RECEIVED_BY     = "MD_REAL_RECEIVED_BY";
    private static final String KEY_MD_REAL_RECEIVED_BY_NIK = "MD_REAL_RECEIVED_BY_NIK";
    private static final String KEY_MD_LOADED_BY_ID         = "MD_LOADED_BY_ID";
    private static final String KEY_MD_LOADED_BY            = "MD_LOADED_BY";

    private static Cursor ME_Cursor;
    private static Cursor ML_Cursor;
    private static Cursor XMHCursor;
    private static Cursor XMICursor;

    // =============================================================
    private static final String KEY_ME_ORDER_BY = KEY_ME_PERSON_ID;
    private static final String KEY_ML_ORDER_BY = KEY_ML_CONTEXTS;
    private static final String KEY_MD_ORDER_BY = KEY_MD_ID;

    private static final String[] ME_COLUMNS = {KEY_ME_PERSON_ID,KEY_ME_EMPLOYEE_NAME,KEY_ME_EMPLOYEE_NUMBER,KEY_ME_EMAIL_ADDRESS,KEY_ME_ATTRIBUTE};
    private static final String[] ML_COLUMNS = {KEY_ML_CONTEXTS, KEY_ML_VALUE, KEY_ML_DESCRIPTION};
    // "SELECT MDO_ID, MDO_VEHICLE_NO, MDO_LOCATION, MDO_TRX_NUMBER, MDO_RECEIVED_BY, MDO_SIGNATURE, MDO_SIGNATURE_FN, MDO_SIGNATURE_FS, MDO_CREATED_DATE "

    public boolean isNetworkConnected(SharedPreferences config, Context ctx)
    {
        String xfactor;
        String Message;
        Message = "Checking Connection ... Please Wait ...";

        xfactor = "S";
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        String SSoapAddress = config.getString("SoapAddress", "");
        String SSoapNamespace = config.getString("SoapNamespace", "");
        String SSoapMethodTest = "Get_Test_Result";

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SSoapAddress);
            httpTransport.debug = true;
            httpTransport.call(SSoapNamespace + SSoapMethodTest, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        catch(Exception ex){
            xfactor = ex.getMessage().toString();
            Toast.makeText(ctx, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
        }
        if(xfactor == "S"){
            Toast.makeText(ctx, "Test Connection Succeed ", Toast.LENGTH_LONG).show();
            return true;
        }
        else{
            return false;
        }

        /*
        ConnectivityManager cm;
        NetworkInfo info = null;
        try
        {
            cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            info = cm.getActiveNetworkInfo();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if (info != null) {return true;}
        else {return false;}
 */
    }

    private static final String[] MDOD_COLUMNS = {
            KEY_MD_ID,
            KEY_MD_VEHICLE_NO,
            KEY_MD_LOCATION,
            KEY_MD_TRX_NUMBER,
            KEY_MD_RECEIVED_BY_ID,
            KEY_MD_RECEIVED_BY,
            KEY_MD_RAK_PERSIAPAN,
            KEY_MD_BIN_PERSIAPAN,
            KEY_MD_ORG_ID,
            KEY_MD_ITEM_ID,
            KEY_MD_ITEM_CODE,
            KEY_MD_ITEM_DESC,
            KEY_MD_QTY,
            KEY_MD_UOM,
            KEY_MD_LOADING_DATE,
            KEY_MD_SENT_DATE,
            KEY_MD_REASON_CODE,
            KEY_MD_CREATEDATE };

    private static final String[] MH_COLUMNS = {
            KEY_MDO_ID,
            KEY_MDO_VEHICLE_NO,
            KEY_MDO_REQUESTOR,
            KEY_MDO_LOCATION,
            KEY_MDO_TRX_NUMBER,
            KEY_MDO_RECEIVED_BY_ID,
            KEY_MDO_RECEIVED_BY,
            KEY_MDO_SIGNATURE,
            KEY_MDO_SIGNATURE_FN,
            KEY_MDO_SIGNATURE_FS,
            KEY_MDO_CREATEDATE,
            KEY_MDO_REAL_RECEIVED_BY,
            KEY_MDO_REAL_RECEIVED_BY_NIK
    };

    private static final String[] MD_COLUMNS = {
            KEY_MD_ID,
            KEY_MD_VEHICLE_NO,
            KEY_MD_REQUESTOR,
            KEY_MD_LOCATION,
            KEY_MD_TRX_NUMBER,
            KEY_MD_RECEIVED_BY_ID,
            KEY_MD_RECEIVED_BY,
            KEY_MD_RAK_PERSIAPAN,
            KEY_MD_BIN_PERSIAPAN,
            KEY_MD_ORG_ID,
            KEY_MD_ITEM_ID,
            KEY_MD_ITEM_CODE,
            KEY_MD_ITEM_DESC,
            KEY_MD_QTY,
            KEY_MD_QTY_RECEIVED,
            KEY_MD_UOM,
            KEY_MD_REF_LOADING,
            KEY_MD_CHECK_LOADING,
            KEY_MD_LOADING_DATE,
            KEY_MD_SYNC_LOADING_DATE,
            KEY_MD_REF_SENT,
            KEY_MD_CHECK_SENT,
            KEY_MD_SENT_DATE,
            KEY_MD_SYNC_SENT_DATE,
            KEY_MD_REASON_CODE,
            KEY_MD_CREATEDATE,
            KEY_MD_REAL_RECEIVED_BY,
            KEY_MD_REAL_RECEIVED_BY_NIK,
            KEY_MD_LOADED_BY_ID,
            KEY_MD_LOADED_BY
    };

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final String CREATE_ME_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_EMPLOYEE + " ( " +
            KEY_ME_PERSON_ID        + " INTEGER PRIMARY KEY, " +
            KEY_ME_EMPLOYEE_NAME    + " TEXT, " +
            KEY_ME_EMPLOYEE_NUMBER  + " TEXT, " +
            KEY_ME_EMAIL_ADDRESS    + " TEXT," +
            KEY_ME_ATTRIBUTE        + " TEXT); ";

    public static final String CREATE_ML_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_LOOKUP + " ( "  +
            KEY_ML_CONTEXTS     + " TEXT, " +
            KEY_ML_VALUE        + " TEXT, " +
            KEY_ML_DESCRIPTION  + " TEXT); ";

    public static final String CREATE_MLO_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_T_MANIFEST_LO + " ( " +
            KEY_MLO_ID            + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_MLO_VEHICLE_NO    + " TEXT, " +
            KEY_MLO_RAK_PERSIAPAN + " TEXT, " +
            KEY_MLO_CREATEDATE    + " TEXT, " +
            KEY_MLO_LOADED_BY     + " TEXT, " + "" +
            KEY_MLO_LOADED_BY_ID  + " NUMBER " + "); ";

    public static final String CREATE_MDO_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_T_MANIFEST_DO + " ( " +
            KEY_MDO_ID                   + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_MDO_VEHICLE_NO           + " TEXT, " +
            KEY_MDO_REQUESTOR            + " TEXT, " +
            KEY_MDO_LOCATION             + " TEXT, " +
            KEY_MDO_TRX_NUMBER           + " TEXT, " +
            KEY_MDO_RECEIVED_BY_ID       + " NUMBER, " +
            KEY_MDO_RECEIVED_BY          + " TEXT, " +
            KEY_MDO_SIGNATURE            + " BLOB, " +
            KEY_MDO_SIGNATURE_FN         + " TEXT, " +
            KEY_MDO_SIGNATURE_FS         + " NUMBER, " +
            KEY_MDO_CREATEDATE           + " TEXT," +
            KEY_MDO_REAL_RECEIVED_BY     + " TEXT," +
            KEY_MDO_REAL_RECEIVED_BY_NIK + "); ";

    public static final String CREATE_MD_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_T_MANIFEST_D + " ( " +
            KEY_MD_ID                + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_MD_VEHICLE_NO           + " TEXT, " +
            KEY_MD_REQUESTOR            + " TEXT, " +
            KEY_MD_LOCATION             + " TEXT, " +
            KEY_MD_TRX_NUMBER           + " TEXT, " +
            KEY_MD_RECEIVED_BY_ID       + " NUMBER, " +
            KEY_MD_RECEIVED_BY          + " TEXT, " +
            KEY_MD_RAK_PERSIAPAN        + " TEXT, " +
            KEY_MD_BIN_PERSIAPAN        + " TEXT, " +
            KEY_MD_ORG_ID               + " INTEGER, " +
            KEY_MD_ITEM_ID              + " INTEGER, " +
            KEY_MD_ITEM_CODE            + " TEXT," +
            KEY_MD_ITEM_DESC            + " TEXT," +
            KEY_MD_QTY                  + " NUMERIC, " +
            KEY_MD_QTY_RECEIVED         + " NUMERIC, " +
            KEY_MD_UOM                  + " TEXT," +
            KEY_MD_REF_LOADING          + " INTEGER, " +
            KEY_MD_CHECK_LOADING        + " TEXT," +
            KEY_MD_LOADING_DATE         + " TEXT," +
            KEY_MD_SYNC_LOADING_DATE    + " TEXT," +
            KEY_MD_REF_SENT             + " INTEGER, " +
            KEY_MD_CHECK_SENT           + " TEXT," +
            KEY_MD_SENT_DATE            + " TEXT," +
            KEY_MD_SYNC_SENT_DATE       + " TEXT," +
            KEY_MD_REASON_CODE          + " TEXT," +
            KEY_MD_CREATEDATE           + " TEXT," +
            KEY_MD_REAL_RECEIVED_BY     + " TEXT," +
            KEY_MD_REAL_RECEIVED_BY_NIK + " TEXT," +
            KEY_MD_LOADED_BY_ID         + " NUMBER," +
            KEY_MD_LOADED_BY            + " TEXT," +
            "UNIQUE (" + KEY_MD_LOCATION      + "," +
                         KEY_MD_TRX_NUMBER    + "," +
                         KEY_MD_BIN_PERSIAPAN + "," +
                         KEY_MD_RAK_PERSIAPAN + "," +
                         KEY_MD_ITEM_ID + "));";

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("[GudangGaram]", "OnCreate MySQLiteHelper");
        db.execSQL(CREATE_ME_TABLE);
        Log.d("[GudangGaram]", "CREATE ME TABLE");
        db.execSQL(CREATE_ML_TABLE);
        Log.d("[GudangGaram]", "CREATE ML TABLE");
        db.execSQL(CREATE_MLO_TABLE);
        Log.d("[GudangGaram]", "CREATE MLO TABLE");
        db.execSQL(CREATE_MDO_TABLE);
        Log.d("[GudangGaram]", "CREATE MDO TABLE");
        db.execSQL(CREATE_MD_TABLE);
        Log.d("[GudangGaram]", "CREATE MD TABLE");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older master table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_EMPLOYEE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_LOOKUP);
        onCreate(db);
    }

    public Cursor GetEmployeeCursor(){
        return ME_Cursor;
    }
    public Cursor GetLookupCursor() { return ML_Cursor; }
    public Cursor GetCursorMH(){
        return XMHCursor;
    }

    //---------------------------------------------------------------------

    public long addManifestDOLoading(
            String pVehicleNo,
            String pRakPersiapan,
            String pLoadBy,
            String pLoadByID)
    {
        // ====================================================================================================
        // this function is purpose to create DO Header after user click Submit in Form DO Loading
        // ====================================================================================================

        long savestatus;
        String sysdate = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_MLO_VEHICLE_NO    , pVehicleNo);
        values.put(KEY_MLO_RAK_PERSIAPAN , pRakPersiapan);
        values.put(KEY_MLO_CREATEDATE    , sysdate);
        values.put(KEY_MLO_LOADED_BY     , pLoadBy);
        values.put(KEY_MLO_LOADED_BY_ID  , pLoadByID);

        savestatus = db.insert(TABLE_T_MANIFEST_LO,null,values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public void dataCleansing(){
        String update_d, update_do, update_lo;

        update_d = "UPDATE GGGG_DOMAE_DEVICE_D " +
                "SET md_loading_date = replace(replace(replace(replace(md_loading_date,'Mei','May'),'Agt','Aug'),'Okt','Oct'),'Des','Dec')," +
                " md_sent_date = replace(replace(replace(replace(md_sent_date,'Mei','May'),'Agt','Aug'),'Okt','Oct'),'Des','Dec')," +
                " md_created_date = replace(replace(replace(replace(md_created_date,'Mei','May'),'Agt','Aug'),'Okt','Oct'),'Des','Dec');";

        update_do = "UPDATE GGGG_DOMAE_DEVICE_DO SET mdo_created_date = replace(replace(replace(replace(mdo_created_date,'Mei','May'),'Agt','Aug'),'Okt','Oct'),'Des','Dec');";
        update_lo = "UPDATE GGGG_DOMAE_DEVICE_LO SET mlo_created_date = replace(replace(replace(replace(mlo_created_date,'Mei','May'),'Agt','Aug'),'Okt','Oct'),'Des','Des');";

        SQLiteDatabase db   = this.getWritableDatabase();
        try{
            // cleansing date format for GGGG_DOMAE_DEVICE_D
            db.execSQL(update_d);
            // cleansing date format for GGGG_DOMAE_DEVICE_Do
            db.execSQL(update_do);
            // cleansing date format for GGGG_DOMAE_DEVICE_LO
            db.execSQL(update_lo);
        }
        catch (Exception e){
            Log.d("[GudangGaram]", "Data Cleansing Exception -> " + e.getMessage().toString());

        }
        finally {
            db.close();
        }
    }

    public void CancelReceiveManifest(String pMDRefSent){
        SQLiteDatabase db   = this.getWritableDatabase();
        try{
            // set flag to null for all DO Sent Attribute in GGGG_DOMAE_DEVICE_D
            db.execSQL("UPDATE GGGG_DOMAE_DEVICE_D " +
                       "SET MD_REASON_CODE    = NULL," +
                       "    MD_SYNC_SENT_DATE = NULL," +
                       "    MD_SENT_DATE      = NULL," +
                       "    MD_CHECK_SENT     = NULL," +
                       "    MD_REF_SENT       = NULL, " +
                       "    MD_QTY_RECEIVED   = NULL " +
                       "WHERE MD_REF_LOADING IS NOT NULL " +
                       "  AND MD_REF_SENT = " + pMDRefSent);

            Log.d("[GudangGaram]:", "CancelManifest : Update Flag GGGG_DOMAE_DEVICE_D WHERE MD_REF_SENT = " + pMDRefSent);
            // set delete record from DO Sent Attribute in GGGG_DOMAE_DEVICE_DO
            db.execSQL("DELETE FROM GGGG_DOMAE_DEVICE_DO WHERE MDO_ID = " + pMDRefSent);
            Log.d("[GudangGaram]:", "CancelManifest : Delete Record From GGGG_DOMAE_DEVICE_DO WHERE MDO_ID = " + pMDRefSent);
        }
        catch (Exception e){
        }
        finally {
            db.close();
        }
    }

    public void delManifest(SQLiteDatabase db, String pheaderID){
        String qDeleteDO;
        try{
            qDeleteDO = "DELETE FROM GGGG_DOMAE_DEVICE_D WHERE MD_REF_SENT = " + pheaderID;
            db.execSQL(qDeleteDO);
            Log.d("[GudangGaram]", "delManifest >> DELETE FROM GGGG_DOMAE_DEVICE_D WHERE MD_REF_SENT = " + pheaderID);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "delManifest Exception " + e.getMessage().toString());
        }
    }

    public void delManifestHeader(String pHeaderID){
        String qDeleteDOH;
        // check if detail already deleted all if so then do delete to the header
            SQLiteDatabase db   = this.getWritableDatabase();
            try{
                delManifestLoading(db,pHeaderID);
                delManifest(db,pHeaderID);
                qDeleteDOH = "DELETE FROM GGGG_DOMAE_DEVICE_DO WHERE MDO_ID = " + pHeaderID;
                db.execSQL(qDeleteDOH);
                Log.d("[GudangGaram]", "delManifestHeader >> DELETE FROM GGGG_DOMAE_DEVICE_DO WHERE MDO_ID = " + pHeaderID.toString());
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "delManifestHeader Exception " + e.getMessage().toString());
            }
            finally {
                db.close();
            }
    }

    public void delManifestLoading(SQLiteDatabase db , String pheaderID){
        Log.d("[GudangGaram]", "delManifestLoading pheaderID -> " + pheaderID);
        String qDeleteLO;
            try{
                qDeleteLO = "DELETE FROM GGGG_DOMAE_DEVICE_LO " +
                            " WHERE MLO_ID IN (" +
                            "  SELECT MD_REF_LOADING " +
                            "  FROM GGGG_DOMAE_DEVICE_D " +
                            "  WHERE MD_REF_SENT = " + pheaderID + ");";
                db.execSQL(qDeleteLO);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "delManifestLoading Exception " + e.getMessage().toString());
            }
    }

    public long UpdateLoadingManifest(
            String pDOLoadingHeaderID,
            String pRakPersiapan,
            String pBinPersiapan,
            String pVehicleNo,
            String pLoadedBy,
            String pLoadedByID)
    {
        // ====================================================================================================
        // this function is purpose to update DO Manifest after user click Submit in Form DO Service
        // ====================================================================================================

        long updatestatus;
        String sysdate = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));
        String Loading_Flag = "Y";

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_MD_REF_LOADING   , pDOLoadingHeaderID);
        args.put(KEY_MD_CHECK_LOADING , Loading_Flag);
        args.put(KEY_MD_LOADING_DATE  , sysdate);
        args.put(KEY_MD_VEHICLE_NO    , pVehicleNo);
        args.put(KEY_MD_LOADED_BY     , pLoadedBy);
        args.put(KEY_MD_LOADED_BY_ID  , pLoadedByID);

        updatestatus = db.update(TABLE_T_MANIFEST_D, args,
                                  KEY_MD_CHECK_LOADING + " IS NULL " +
                                          " AND " + KEY_MD_CHECK_SENT + " IS NULL " +
                                          " AND " + KEY_MD_RAK_PERSIAPAN + "= '" + pRakPersiapan + "' " +
                                          " AND " + KEY_MD_BIN_PERSIAPAN + "= '" + pBinPersiapan + "' ", null);
        db.close();

        return updatestatus;
    }

    public long addManifestDOHeader(
            String pVehicleNo,
            String pRequestor,
            String pLocation,
            String pTrxNumber,
            String pReceivedByID,
            String pReceivedBy,
            byte[] pSignature,
            String pSignatureFN,
            long   pSignatureFS,
            String pRealReceivedBy,
            String pRealReceivedByNIK
    )
    {
        // ====================================================================================================
        // this function is purpose to create DO Header after user click Save in Form DO Service
        // ====================================================================================================

        long savestatus;
        String sysdate = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_MDO_VEHICLE_NO          , pVehicleNo);
        values.put(KEY_MDO_REQUESTOR           , pRequestor);
        values.put(KEY_MDO_LOCATION            , pLocation);
        values.put(KEY_MDO_TRX_NUMBER          , pTrxNumber);
        values.put(KEY_MDO_RECEIVED_BY_ID      , pReceivedByID);
        values.put(KEY_MDO_RECEIVED_BY         , pReceivedBy);
        values.put(KEY_MDO_CREATEDATE          , sysdate);
        values.put(KEY_MDO_SIGNATURE           , pSignature);
        values.put(KEY_MDO_SIGNATURE_FN        , pSignatureFN);
        values.put(KEY_MDO_SIGNATURE_FS        , pSignatureFS);
        values.put(KEY_MDO_REAL_RECEIVED_BY    , pRealReceivedBy);
        values.put(KEY_MDO_REAL_RECEIVED_BY_NIK, pRealReceivedByNIK);

        savestatus = db.insert(TABLE_T_MANIFEST_DO,null,values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public String replace_format_indo(String str){
        String result;
        result = str.replace("Mei","May").replace("Agt","Aug").replace("Okt","Oct").replace("Des","Dec");
        return result;
    }

    public void UpdateDOSFlag(String p_SID, String p_sent_flag)
    {
        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_MD_CHECK_SENT      , p_sent_flag);
        Log.d("[GudangGaram]","UpdateDOSFlag(" + p_SID + "," + p_sent_flag + ")");

        try{
            db.update(TABLE_T_MANIFEST_D, args,
                                      KEY_MD_CHECK_LOADING + " = 'Y' " +
                            " AND " + KEY_MD_REF_SENT + " IS NULL " +
                            " AND " + KEY_MD_ID + "= " + p_SID + " ", null);
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }
    }

    public void UpdateDOSReasonNFlag()
    {
        String p_reason    = "";
        String p_sent_flag = "";
        String p_qty_received = "";

        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_MD_REASON_CODE     , p_reason);
        args.put(KEY_MD_CHECK_SENT      , p_sent_flag);
        args.put(KEY_MD_QTY_RECEIVED    , p_qty_received);

        try{
            db.update(TABLE_T_MANIFEST_D, args,
                                        KEY_MD_CHECK_LOADING + " = 'Y' " +
                                              " AND (" + KEY_MD_REASON_CODE + " IS NOT NULL OR " + KEY_MD_CHECK_SENT + " IS NOT NULL) " +
                                              " AND " + KEY_MD_REF_SENT + " IS NULL ", null);
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }
    }


    public void UpdateDOSReason(String p_SID, String p_reason, String p_sent_flag)
    {
        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_MD_REASON_CODE     , p_reason);
        args.put(KEY_MD_CHECK_SENT      , p_sent_flag);

        try{
            db.update(TABLE_T_MANIFEST_D, args,
                    KEY_MD_CHECK_LOADING + " = 'Y' " +
                            " AND " + KEY_MD_REF_SENT + " IS NULL " +
                            " AND " + KEY_MD_ID + "= " + p_SID + " ", null);
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }
    }

    public void UpdateDOSQtyReceived(String p_SID, String p_qty_received)
    {
        String SQuery;
        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_MD_QTY_RECEIVED      , p_qty_received);

        try{
            Log.d("[GudangGaram]:", "UpdateDOSQtyReceived + ("  + p_SID + ") : " + p_qty_received);
            SQuery =  KEY_MD_CHECK_LOADING + " = 'Y' " +
                    " AND " + KEY_MD_REF_SENT + " IS NULL " +
                    " AND " + KEY_MD_ID + "= " + p_SID + " ";
            Log.d("[GudangGaram]:", "UpdateDOSQtyReceived Query : " + SQuery);

            db.update(TABLE_T_MANIFEST_D, args, SQuery, null);
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }
    }

    public long UpdateDOSManifest(
            String  p_SID,
            Long    p_DOHeaderID,
            String  p_received_by_id,
            String  p_received_by,
            String  p_real_received_by,
            String  p_real_received_by_nik)
    {
        // ====================================================================================================
        // this function is purpose to update flag DO Sent to become Y after user click Save in Form DO Service
        // ====================================================================================================

        long updatestatus;
        String sysdate = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_MD_REF_SENT                , p_DOHeaderID);
        args.put(KEY_MD_SENT_DATE               , sysdate);
        args.put(KEY_MD_RECEIVED_BY_ID          , p_received_by_id);
        args.put(KEY_MD_RECEIVED_BY             , p_received_by);
        args.put(KEY_MD_REAL_RECEIVED_BY        , p_real_received_by);
        args.put(KEY_MD_REAL_RECEIVED_BY_NIK    , p_real_received_by_nik);

        updatestatus = db.update(TABLE_T_MANIFEST_D, args,
                KEY_MD_CHECK_LOADING + " = 'Y' " +
                        " AND " + KEY_MD_REF_SENT + " IS NULL " +
                        " AND " + KEY_MD_ID + "= " + p_SID + " ", null);

        db.close();

        return updatestatus;
    }

    public long addManifestDetail(
            String  pNoKendaraan,
            String  pRakPersiapan,
            String  pBinPersiapan,
            String  pRequestor,
            String  pLocation,
            String  pTrxnumber,
            Integer pOrgID,
            Integer pItemID,
            String  pItemCode,
            String  pItemDesc,
            Float   pQty,
            String  pUOM
    )
    {
        // ====================================================================================================
        // this function is purpose to create Manifest Detail Entries after user click Sync in Form Sync
        // ====================================================================================================

        long savestatus;
        String sysdate = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));

        SQLiteDatabase db = this.getWritableDatabase();

        savestatus = 0;
        try{
            ContentValues values = new ContentValues();
            values.put(KEY_MD_VEHICLE_NO    , pNoKendaraan);
            values.put(KEY_MD_RAK_PERSIAPAN , pRakPersiapan);
            values.put(KEY_MD_BIN_PERSIAPAN , pBinPersiapan);
            values.put(KEY_MD_REQUESTOR     , pRequestor);
            values.put(KEY_MD_LOCATION      , pLocation);
            values.put(KEY_MD_TRX_NUMBER    , pTrxnumber);
            values.put(KEY_MD_ORG_ID        , pOrgID);
            values.put(KEY_MD_ITEM_ID       , pItemID);
            values.put(KEY_MD_ITEM_CODE     , pItemCode);
            values.put(KEY_MD_ITEM_DESC     , pItemDesc);
            values.put(KEY_MD_QTY           , pQty);
            values.put(KEY_MD_UOM           , pUOM);
            values.put(KEY_MD_CREATEDATE    , sysdate);

            savestatus = db.insert(TABLE_T_MANIFEST_D,null,values); // key/value -> keys = column names/ values = column values
            Log.d("[GudangGaram]:", "Save Manifest Detail Status :" + savestatus);
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "addManifestDetail Exception " + e.getMessage().toString());
        }
        finally {
            db.close();
            Log.d("[GudangGaram]:", "addManifestDetail Save To DB Done");
        }

        return savestatus;
    }

    public long addLookup(
            String pcontext,
            String pvalue,
            String pdescription)
    {
        // ====================================================================================================
        // this function is purpose to create Lookup Entries after user click Sync in Form Sync
        // ====================================================================================================

        long savestatus;
        String sysdate = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ML_CONTEXTS      , pcontext);
        values.put(KEY_ML_VALUE         , pvalue);
        values.put(KEY_ML_DESCRIPTION   , pdescription);

        savestatus = db.insert(TABLE_M_LOOKUP,null,values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public long addEmployee(
            String pPersonID,
            String pEmployeeName,
            String pEmployeeNumber,
            String pEmailAddress,
            String pAttribute)
    {
        // ====================================================================================================
        // this function is purpose to create Employee Master after user click Sync in Form Sync
        // ====================================================================================================

        Log.d("[GudangGaram]", "MySQLiteHelper pAttribute        :" + pAttribute);

        long savestatus;
        String sysdate = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ME_PERSON_ID         , pPersonID);
        values.put(KEY_ME_EMPLOYEE_NAME     , pEmployeeName);
        values.put(KEY_ME_EMPLOYEE_NUMBER   , pEmployeeNumber);
        values.put(KEY_ME_EMAIL_ADDRESS     , pEmailAddress);
        values.put(KEY_ME_ATTRIBUTE         , pAttribute);

        savestatus = db.insert(TABLE_M_EMPLOYEE,null,values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public long delEmployee(String pEmployeeID){
        long deletestatus;
        SQLiteDatabase db = this.getWritableDatabase();
        deletestatus = db.delete(TABLE_M_EMPLOYEE, KEY_ME_PERSON_ID  + " = " + pEmployeeID,null);
        db.close();
        return deletestatus;
    }

    public long editEmployee(
                String pEmployeeID,
                String pEmployeeName,
                String pEmployeeNumber,
                String pEmailAddress,
                String pAttribute)
    {
        long editstatus;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_ME_EMPLOYEE_NAME       , pEmployeeName);
        args.put(KEY_ME_EMPLOYEE_NUMBER     , pEmployeeNumber);
        args.put(KEY_ME_EMAIL_ADDRESS       , pEmailAddress);
        args.put(KEY_ME_ATTRIBUTE           , pAttribute);

        editstatus = db.update(TABLE_M_EMPLOYEE, args, KEY_ME_PERSON_ID + "=" + pEmployeeID, null);
        db.close();
        return editstatus;
    }

    public void flushAll(){
        flushTable(TABLE_T_MANIFEST_DO);
        flushTable(TABLE_T_MANIFEST_LO);
        flushTable(TABLE_T_MANIFEST_D);
        flushTable(TABLE_M_LOOKUP);
        flushTable(TABLE_M_EMPLOYEE);
    }
    // ================================= db operation util =========================================
    public void flushTable(String pTableName){
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(pTableName, null, null);
        db.close();
    }

    public int getCount(SQLiteDatabase db, String pTableName, String pWhere) {
        //SQLiteDatabase db =  this.getWritableDatabase();
        try {
            String count = "SELECT count(*) FROM " + pTableName + " " + pWhere;
            Cursor mcursor = db.rawQuery(count, null);
            mcursor.moveToFirst();
            int icount = mcursor.getInt(0);
            if(icount>0){
                return icount;
            }
            else{
                return 0;
            }
        }
        catch(SQLiteException e) {
            e.printStackTrace();
            return 0;
        }
    }


    public int getRecordCount(String pTableName, String pWhere) {
        SQLiteDatabase db =  this.getWritableDatabase();
        try {
            String count = "SELECT count(*) FROM " + pTableName + " " + pWhere;
            Cursor mcursor = db.rawQuery(count, null);
            mcursor.moveToFirst();
            int icount = mcursor.getInt(0);
            if(icount>0){
                return icount;
            }
            else{
                return 0;
            }
        }
        catch(SQLiteException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public List<MasterEmployee> getAllEmployee()
    {
        List<MasterEmployee> items = new ArrayList<MasterEmployee>();
        SQLiteDatabase db = this.getWritableDatabase();
        ML_Cursor = db.query(
                MySQLiteHelper.TABLE_M_EMPLOYEE,
                ME_COLUMNS,
                null,
                null,
                null,
                null ,
                null);

        ML_Cursor.moveToFirst();
        while (!ML_Cursor.isAfterLast())
        {
            MasterEmployee cursorMEmployee = cursorToItem(ML_Cursor);
            items.add(cursorMEmployee);
            ML_Cursor.moveToNext();
        }
        //XCursor.close();
        db.close();
        return items;
    }

    public List<String> LoadLoadingManifest(String p_rak_persiapan) {
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getWritableDatabase();

        String kueri = "SELECT MD_BIN_PERSIAPAN " +
                       "FROM GGGG_DOMAE_DEVICE_D " +
                       "WHERE MD_CHECK_LOADING IS NULL " +
                        " AND MD_REF_SENT IS NULL " +
                        " AND MD_RAK_PERSIAPAN = '" + p_rak_persiapan + "' " +
                        "GROUP BY MD_BIN_PERSIAPAN " +
                        "ORDER BY MD_BIN_PERSIAPAN;";

        ML_Cursor = db.rawQuery(kueri, null);
        if(ML_Cursor!=null){
            ML_Cursor.moveToFirst();
            while (!ML_Cursor.isAfterLast())
            {
                lvi.add(ML_Cursor.getString(0));
                ML_Cursor.moveToNext();
            }
            db.close();
        }

        return lvi;
    }

    public List<String> getLOV_DOS_Location(String p_vehicle_no){
        // ambil dari transaksi yang check loading nya sudah di check --> sudah di load
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getWritableDatabase();
        String kueri = "SELECT COALESCE(MD_LOCATION,' ') MD_LOCATION " +
                       "FROM GGGG_DOMAE_DEVICE_D " +
                       "WHERE MD_CHECK_LOADING = 'Y' " +
                       "  AND MD_REF_SENT IS NULL " +
                       "  AND MD_VEHICLE_NO = '" + p_vehicle_no + "' " +
                       "GROUP BY MD_LOCATION; ";

        ML_Cursor = db.rawQuery(kueri, null);
        try{
            if(ML_Cursor.getCount() > 0){
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    lvi.add(ML_Cursor.getString(0));
                    ML_Cursor.moveToNext();
                }
            }
            else{
                lvi.add("");
            }
        }
        catch(Exception e){
            lvi.add("");
        }
        finally {
            db.close();
        }

        return lvi;
    }

    public List<String> getLOV_DOS_TrxNum(String p_vehicle_no,
                                          String p_location){
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getWritableDatabase();
        String kueri = "SELECT COALESCE(MD_TRX_NUMBER,' ') MD_TRX_NUMBER " +
                       "FROM GGGG_DOMAE_DEVICE_D " +
                       "WHERE MD_CHECK_LOADING = 'Y' " +
                       "  AND MD_REF_SENT IS NULL " +
                       "  AND MD_VEHICLE_NO    = '" + p_vehicle_no + "' " +
                       "  AND MD_LOCATION      = '" + p_location + "' " +
                       "GROUP BY MD_TRX_NUMBER " +
                       "ORDER BY MD_TRX_NUMBER; ";

        ML_Cursor = db.rawQuery(kueri, null);
        try{
            if(ML_Cursor.getCount() > 0){
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    lvi.add(ML_Cursor.getString(0));
                    ML_Cursor.moveToNext();
                }
            }
            else{
                lvi.add("");
            }
        }
        catch(Exception e){
            lvi.add("");
        }
        finally {
            db.close();
        }

        return lvi;
    }


    public List<String> getLOV_LO_RakPersiapan(){
        // ambil dari transaksi yang check loading nya masih null  --> belum di load
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getWritableDatabase();

        String kueri = "SELECT MD_RAK_PERSIAPAN " +
                       "FROM GGGG_DOMAE_DEVICE_D " +
                "WHERE MD_CHECK_LOADING IS NULL " +
                "  AND MD_REF_SENT IS NULL " +
                "GROUP BY MD_RAK_PERSIAPAN " +
                "ORDER BY MD_RAK_PERSIAPAN; ";

        ML_Cursor = db.rawQuery(kueri, null);
        try{
            if(ML_Cursor.getCount() > 0){
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    lvi.add(ML_Cursor.getString(0));
                    ML_Cursor.moveToNext();
                }
            }
            else{
                lvi.add("");
            }
        }
        catch(Exception e){
            lvi.add("");
        }
        finally {
            db.close();
        }

        return lvi;
    }

    public List<String> getList_DOS_BinPersiapan(
            String p_vehicle_no,
            String p_location,
            String p_trx_no)
    {
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getWritableDatabase();

        String kueri = "SELECT MD_BIN_PERSIAPAN " +
                       "FROM GGGG_DOMAE_DEVICE_D " +
                       "WHERE MD_CHECK_LOADING = 'Y' " +
                       "  AND MD_REF_SENT IS NULL " +
                       "  AND MD_VEHICLE_NO    = '" + p_vehicle_no + "' " +
                       "  AND MD_LOCATION      = '" + p_location + "' " +
                       "  AND MD_TRX_NUMBER    = '" + p_trx_no + "' " +
                       "GROUP BY MD_BIN_PERSIAPAN " +
                       "ORDER BY MD_BIN_PERSIAPAN; ";

        ML_Cursor = db.rawQuery(kueri, null);
        try{
            if(ML_Cursor.getCount() > 0){
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    lvi.add(ML_Cursor.getString(0));
                    ML_Cursor.moveToNext();
                }
            }
            else{
                lvi.add("");
            }
        }
        catch(Exception e){
            lvi.add("");
        }
        finally {
            db.close();
        }

        return lvi;
    }

    public List<HolderDOS> getList_DOS_BinPersiapanItem(
            String p_vehicle_no,
            String p_location,
            String p_trx_no,
            String p_bin_persiapan)
    {
        List<HolderDOS> lvi = new ArrayList<HolderDOS>();
        SQLiteDatabase db =  this.getWritableDatabase();

        String kueri = "SELECT MD_ID, " +
                              "MD_ITEM_CODE, " +
                              "MD_ITEM_DESC, " +
                              "MD_QTY, " +
                               "0.0 as MD_QTY_RECEIVED, " +
                              "MD_UOM, " +
                              "MD_REQUESTOR, " +
                              "MD_REASON_CODE " +
                       "FROM GGGG_DOMAE_DEVICE_D " +
                       "WHERE MD_CHECK_LOADING = 'Y' " +
                       "  AND MD_REF_SENT IS NULL " +
                       "  AND MD_VEHICLE_NO    = '" + p_vehicle_no + "' " +
                       "  AND MD_LOCATION      = '" + p_location + "' " +
                       "  AND MD_TRX_NUMBER    = '" + p_trx_no + "' " +
                       "  AND MD_BIN_PERSIAPAN = '" + p_bin_persiapan + "' " +
                       "ORDER BY MD_BIN_PERSIAPAN, MD_ID; ";

        Log.d("[GudangGaram]", kueri);

        ML_Cursor = db.rawQuery(kueri, null);

        try{
            if(ML_Cursor.getCount() > 0){
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    HolderDOS ox  = new HolderDOS();
                    ox.setSID(ML_Cursor.getString(0));
                    ox.setSItemCode(ML_Cursor.getString(1));
                    ox.setSItemDesc(ML_Cursor.getString(2));
                    ox.setSQty(ML_Cursor.getFloat(3));
                    ox.setSQtyR(ML_Cursor.getFloat(4));
                    ox.setSUOM(ML_Cursor.getString(5));
                    ox.setSRequestor(ML_Cursor.getString(6));
                    ox.setSReasonCode(ML_Cursor.getString(7));
                    ox.setEnableReason(true);

                    lvi.add(ox);
                    ML_Cursor.moveToNext();
                }
            }
            else{
                HolderDOS ox  = new HolderDOS();
                lvi.add(ox);
            }
        }
        catch(Exception e){
            HolderDOS ox  = new HolderDOS();
            lvi.add(ox);
        }
        finally {
            db.close();
        }

        return lvi;
    }


    public List<ManifestHeader> get_DOHeader()
    {
        List<ManifestHeader> mhx = new ArrayList<ManifestHeader>();
        SQLiteDatabase db = this.getWritableDatabase();
        XMHCursor = db.query(MySQLiteHelper.TABLE_T_MANIFEST_DO,
                MH_COLUMNS,
                null,
                null,
                null,
                null ,
                null);

        XMHCursor.moveToFirst();
        while (!XMHCursor.isAfterLast())
        {
            ManifestHeader cursorMH = cursorToMH(XMHCursor);
            mhx.add(cursorMH);
            XMHCursor.moveToNext();
        }
        //XMHCursor.close();
        db.close();
        return mhx;
    }

    public List<ManifestDetail> get_DODetail(String p_do_header_id){
        List<ManifestDetail> lvi = new ArrayList<ManifestDetail>();
        SQLiteDatabase db =  this.getWritableDatabase();
        String kueri = "SELECT MD_ID " +
                " ,MD_REF_SENT MH_ID " +
                " ,COALESCE(MD_VEHICLE_NO,'')     MD_VEHICLE_NO " +
                " ,COALESCE(MD_REQUESTOR,'')      MD_REQUESTOR " +
                " ,COALESCE(MD_LOCATION,'')       MD_LOCATION " +
                " ,COALESCE(MD_TRX_NUMBER,'')     MD_TRX_NUMBER " +
                " ,MD_RECEIVED_BY_ID " +
                " ,MD_RECEIVED_BY " +
                " ,MD_RAK_PERSIAPAN " +
                " ,MD_BIN_PERSIAPAN " +
                " ,MD_ORG_ID " +
                " ,MD_ITEM_ID " +
                " ,MD_ITEM_CODE " +
                " ,MD_ITEM_DESC " +
                " ,MD_QTY " +
                " ,COALESCE(MD_QTY_RECEIVED,MD_QTY) MD_QTY_RECEIVED " +
                " ,MD_UOM " +
                " ,MD_LOADING_DATE " +
                " ,COALESCE(MD_CHECK_SENT,'')     MD_CHECK_SENT " +
                " ,MD_SENT_DATE " +
                " ,COALESCE(MD_REASON_CODE,'')    MD_REASON_CODE " +
                " ,MD_CREATED_DATE " +
                " ,MD_REAL_RECEIVED_BY " +
                " ,MD_REAL_RECEIVED_BY_NIK " +
                " ,MD_LOADED_BY_ID " +
                " ,MD_LOADED_BY " +
                " FROM GGGG_DOMAE_DEVICE_D " +
                " WHERE MD_CHECK_LOADING = 'Y' " +
                "  and MD_REF_SENT IS NOT NULL " +
                "  and MD_REF_SENT      = " + p_do_header_id +
                " ORDER BY MD_ID ";

        try{
            ML_Cursor = db.rawQuery(kueri, null);
            if(ML_Cursor.getCount() > 0){
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    ManifestDetail mdox = new ManifestDetail();
                    mdox.setMD_ID(ML_Cursor.getInt(0));
                    mdox.setMH_ID(ML_Cursor.getInt(1));
                    mdox.setMD_VEHICLE_NO(ML_Cursor.getString(2));
                    mdox.setMD_REQUESTOR(ML_Cursor.getString(3));
                    mdox.setMD_LOCATION(ML_Cursor.getString(4));
                    mdox.setMD_TRX_NUMBER(ML_Cursor.getString(5));
                    mdox.setMD_RECEIVED_BY_ID(ML_Cursor.getString(6));
                    mdox.setMD_RECEIVED_BY(ML_Cursor.getString(7));
                    mdox.setMD_RAK_PERSIAPAN(ML_Cursor.getString(8));
                    mdox.setMD_BIN_PERSIAPAN(ML_Cursor.getString(9));
                    mdox.setMD_ORG_ID(ML_Cursor.getInt(10));
                    mdox.setMD_ITEM_ID(ML_Cursor.getInt(11));
                    mdox.setMD_ITEM_CODE(ML_Cursor.getString(12));
                    mdox.setMD_ITEM_DESC(ML_Cursor.getString(13));
                    mdox.setMD_QTY(ML_Cursor.getString(14));
                    mdox.setMD_QTY_RCV(ML_Cursor.getString(15));
                    mdox.setMD_UOM(ML_Cursor.getString(16));
                    mdox.setMD_LOADING_DATE(ML_Cursor.getString(17));
                    mdox.setMD_CHECK_SENT(ML_Cursor.getString(18));
                    mdox.setMD_SENT_DATE(ML_Cursor.getString(19));
                    mdox.setMD_REASON_CODE(ML_Cursor.getString(20));
                    mdox.setMD_CREATED_DATE(ML_Cursor.getString(21));
                    mdox.setMD_REAL_RECEIVED_BY(ML_Cursor.getString(22));
                    mdox.setMD_REAL_RECEIVED_BY_NIK(ML_Cursor.getString(23));
                    mdox.setMD_LOADED_BY_ID(ML_Cursor.getString(24));
                    mdox.setMD_LOADED_BY(ML_Cursor.getString(25));

                    lvi.add(mdox);
                    ML_Cursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }

        return lvi;
    }


    public ManifestHeader cursorToMH(Cursor cursor)
    {
        ManifestHeader mhx = new ManifestHeader();
        mhx.setMDO_ID(cursor.getInt(0));
        mhx.setMDO_VEHICLE_NUMBER(cursor.getString(1));
        mhx.setMDO_REQUESTOR(cursor.getString(2));
        mhx.setMDO_LOCATION(cursor.getString(3));
        mhx.setMDO_TRX_NUMBER(cursor.getString(4));
        mhx.setMDO_RECEIVED_BY_ID(cursor.getString(5));
        mhx.setMDO_RECEIVED_BY(cursor.getString(6));
        mhx.setMDO_SIGNATURE(cursor.getBlob(7));
        mhx.setMDO_SIGNATURE_FN(cursor.getString(8));
        mhx.setMDO_SIGNATURE_FS(cursor.getInt(9));
        // cursor.getString(10) --> creation_date
        mhx.setMDO_REAL_RECEIVED_BY(cursor.getString(11));
        mhx.setMDO_REAL_RECEIVED_BY_NIK(cursor.getString(12));

        return mhx;
    }


    public List<String> getLOV(String ML_Context){
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getWritableDatabase();

        String kueri = "SELECT " + KEY_ML_CONTEXTS + "," +
                                   KEY_ML_VALUE  + "," +
                                   KEY_ML_DESCRIPTION +
                " FROM "     + TABLE_M_LOOKUP +
                " WHERE "    + KEY_ML_CONTEXTS + " = '" + ML_Context + "' " +
                " ORDER BY " + KEY_ML_VALUE;

        try{
            ML_Cursor = db.rawQuery(kueri, null);
            if(ML_Cursor.getCount() > 0){
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    lvi.add(ML_Cursor.getString(1));
                    ML_Cursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }

        return lvi;
    }

    public List<StringWithTag> getEmployeeListLoad(){
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();

        SQLiteDatabase db =  this.getWritableDatabase();
        // ambil semua employee kecuali yang Unregistered
        String kueri = "SELECT " + KEY_ME_PERSON_ID  + "," + KEY_ME_EMPLOYEE_NAME +
                " FROM " + TABLE_M_EMPLOYEE +
                " WHERE " + KEY_ME_EMPLOYEE_NUMBER  + " <> 'OX811700000' AND " + KEY_ME_ATTRIBUTE + " = 'GUDANG' " +
                " ORDER BY " + KEY_ME_EMPLOYEE_NAME;

        try{
            ML_Cursor = db.rawQuery(kueri, null);
            if(ML_Cursor.getCount() > 0) {
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    Integer key  = ML_Cursor.getInt(0);
                    String value = ML_Cursor.getString(1);
                    lvi.add(new StringWithTag(value, key));
                    ML_Cursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }

        return lvi;
    }


    public List<StringWithTag> getEmployeeList(){
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();

        SQLiteDatabase db =  this.getWritableDatabase();
        String kueri = "SELECT " + KEY_ME_PERSON_ID  + "," + KEY_ME_EMPLOYEE_NAME +
                       " FROM " + TABLE_M_EMPLOYEE +
                       " ORDER BY " + KEY_ME_EMPLOYEE_NAME;

        try{
            ML_Cursor = db.rawQuery(kueri, null);
            if(ML_Cursor.getCount() > 0) {
                ML_Cursor.moveToFirst();
                while (!ML_Cursor.isAfterLast())
                {
                    Integer key  = ML_Cursor.getInt(0);
                    String value = ML_Cursor.getString(1);
                    lvi.add(new StringWithTag(value, key));
                    ML_Cursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }

        return lvi;
    }

    public MasterEmployee cursorToItem(Cursor cursor)
    {
        MasterEmployee ix = new MasterEmployee();
        //ix.setID(cursor.getInt(0));
        //{KEY_ID,KEY_RoomCode,KEY_EmployeeCode,KEY_AssetCode,KEY_DeviceID,KEY_CreationDate,KEY_Qty};

        ix.setCEmployeName(cursor.getString(0));
        ix.setCEmployeeNumber(cursor.getString(1));
        //ix.getCEmailAddress(cursor.getString(2));
        return ix;
    }

    public List<HolderManifestListLO> getAllItemListLO() {
        List<HolderManifestListLO> items = new ArrayList<HolderManifestListLO>();
        SQLiteDatabase db = this.getWritableDatabase();

        String kueri = "select MD_VEHICLE_NO, " +
                              "MD_LOCATION, " +
                              "MD_RAK_PERSIAPAN, " +
                              "MD_BIN_PERSIAPAN, " +
                              "MD_TRX_NUMBER " +
                        "from GGGG_DOMAE_DEVICE_D " +
                        "where MD_CHECK_LOADING = 'Y' " +
                        "  and MD_REF_SENT IS NULL " +
                        "group by MD_VEHICLE_NO, MD_LOCATION, MD_RAK_PERSIAPAN, MD_BIN_PERSIAPAN, MD_TRX_NUMBER " +
                        "order by MD_VEHICLE_NO, MD_LOCATION, MD_RAK_PERSIAPAN, MD_BIN_PERSIAPAN, MD_TRX_NUMBER ";

        try{
            ML_Cursor = db.rawQuery(kueri, null);
            ML_Cursor.moveToFirst();
            while (!ML_Cursor.isAfterLast())
            {
                HolderManifestListLO ix = new HolderManifestListLO();
                ix.setVehicle_no(ML_Cursor.getString(0));
                ix.setLocation(ML_Cursor.getString(1));
                ix.setRakPersiapan(ML_Cursor.getString(2));
                ix.setBinPersiapan(ML_Cursor.getString(3));
                ix.setTrx_num(ML_Cursor.getString(4));

                items.add(ix);
                ML_Cursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }

        return items;
    }

    public List<HolderManifestList> getAllItemListDO()
    {
        List<HolderManifestList> items = new ArrayList<HolderManifestList>();
        SQLiteDatabase db = this.getWritableDatabase();

        String kueri = "select H.MDO_ID, " +
                             " H.MDO_VEHICLE_NO, " +
                             " H.MDO_LOCATION, " +
                             " H.MDO_TRX_NUMBER, " +
                             " H.MDO_CREATED_DATE  RECEIVE_DATE, " +
                             " H.MDO_RECEIVED_BY " +
                       "from GGGG_DOMAE_DEVICE_D  X," +
                           " GGGG_DOMAE_DEVICE_DO H," +
                           " GGGG_DOMAE_DEVICE_LO L " +
                       "where H.MDO_ID = X.MD_REF_SENT " +
                       "  and L.MLO_ID = X.MD_REF_LOADING " +
                       "group by H.MDO_ID, " +
                                "H.MDO_VEHICLE_NO," +
                                "H.MDO_LOCATION," +
                                "H.MDO_TRX_NUMBER," +
                                "H.MDO_REQUESTOR," +
                                "H.MDO_CREATED_DATE," +
                                "H.MDO_RECEIVED_BY " +
                       "order by H.MDO_ID DESC ";

        try{
            ML_Cursor = db.rawQuery(kueri, null);
            ML_Cursor.moveToFirst();
            while (!ML_Cursor.isAfterLast())
            {
                HolderManifestList ix = new HolderManifestList();
                ix.setID(ML_Cursor.getString(0));
                ix.setVehicle_no(ML_Cursor.getString(1));
                ix.setLocation(ML_Cursor.getString(2));
                ix.setTrx_num(ML_Cursor.getString(3));
                ix.setReceivedDate(ML_Cursor.getString(4));
                ix.setReceivedBy(ML_Cursor.getString(5));

                items.add(ix);
                ML_Cursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            db.close();
        }

        return items;
    }


    public List<CEmployee> LoadCboEmployee() {
        List<CEmployee> lvi = new ArrayList<CEmployee>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " select *  " +
                        "from (  select ME_PersonID, ME_EmployeeNumber, ME_EmployeeName from GGGG_DOMAE_EMPLOYEE union select -1 as ME_PersonID, -1 as ME_EmployeeNumber, 'Unregistered Employee,' as ME_EmployeeName ) dtx " +
                        "where trim(dtx.ME_EmployeeName) <> '' " +
                        "order by dtx.ME_EmployeeName, dtx.ME_EmployeeNumber ";

        //'OX811700000

        XMICursor = db.rawQuery(kueri, null);
        try{
            if(XMICursor!=null){
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    CEmployee oc = new CEmployee();
                    oc.setPersonID(XMICursor.getInt(0));
                    oc.setPersonNIK(XMICursor.getString(1));
                    oc.setPersonName(XMICursor.getString(2));

                    lvi.add(oc);
                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XMICursor.close();
            //db.close();
        }

        return lvi;
    }

    /*
    public ViewHolderDOS cursorViewHolderDOS(Cursor cursor){
        ViewHolderDOS vx = new ViewHolderDOS();
        vx.setXITEM_CODE(cursor.getString(1));
        vx.setXITEM_DESC(cursor.getString(2));

        //vx.set

        return vx;
    }
    */

}
