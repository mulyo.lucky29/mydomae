package com.gudanggaramtbk.mydomae.activity;

import android.app.AlertDialog;
import android.app.FragmentManager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;


import com.github.gcacace.signaturepad.views.SignaturePad;
import com.gudanggaramtbk.mydomae.adapter.CustomListAdapterDOS;
import com.gudanggaramtbk.mydomae.holder.HolderDOS;
import com.gudanggaramtbk.mydomae.util.LovEmpFragment;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.util.StringWithTag;
import com.gudanggaramtbk.mydomae.model.CEmployee;

public class TProcessDOReceive extends AppCompatActivity implements  View.OnClickListener {
    public static final int REQUEST_EXTERNAL_STORAGE = 1;
    private MySQLiteHelper dbHelper;
    private SignaturePad signaturePad;
    private SharedPreferences   config;

    private Button          Cmd_populate;
    private Button          Cmd_Cancel;
    private Button          Cmd_Save;
    private Button          Cmd_Clear;
    private ImageButton     Cmd_DOS_ReceivedByLOV;



    private Spinner   Cbo_DOS_NomorKendaraan;
    private Spinner   Cbo_DOS_Location;
    private Spinner   Cbo_DOS_TransactionNumber;
    private Spinner   Cbo_DOS_ReceivedBy;

    private TextView  Txt_DOS_ReceivedBy;
    private TextView  Lbl_DOS_ReceivedByID;

    private EditText  Txt_DOS_Real_Received_By;
    private EditText  Txt_DOS_Real_Received_By_NIK;
    private TextView Lbl_DOS_Real_Received_By_NIK;
    private TextView Lbl_DOS_Real_Received_BY;

    private String    Scbo_DOS_NomorKendaraan;
    private String    Scbo_DOS_Location;
    private String    Scbo_DOS_TransactionNumber;
    private String    Scbo_DOS_Requestor;
    private String    Scbo_DOS_ReceivedBy;
    private String    Scbo_DOS_ReceivedByID;
    private String    ReceivedReal;
    private String    ReceivedRealNIK;

    private boolean   State_TTD;

    private static ExpandableListView    expandableListView;
    private static ExpandableListAdapter adapter;

    Context            context;

    ArrayList<String>   WG_GroupHeader    = new ArrayList<String>();
    List<HolderDOS>     WG_ItemList       = new ArrayList<HolderDOS>();

    ArrayList<String>   GroupHeader;
    List<HolderDOS>     AllItemList;
    List<HolderDOS>     ItemList;
    List<CEmployee>     lst_ReceivedBy;


    CustomListAdapterDOS CU;
    TabHost host;

    // --------------- setter activity -----------------------
    public void setLbl_DOS_ReceivedByID(String lbl_DOS_ReceivedByID) {
        Lbl_DOS_ReceivedByID.setText(lbl_DOS_ReceivedByID);
    }
    public void setTxt_DOS_ReceivedBy(String txt_DOS_ReceivedBy) {
        Txt_DOS_ReceivedBy.setText(txt_DOS_ReceivedBy);
        if(txt_DOS_ReceivedBy.equals("Unregistered Employee,")){
            //Log.d("[GudangGaram]", " VISIBLE " + key.toString());
            Txt_DOS_Real_Received_By.setVisibility(View.VISIBLE);
            Lbl_DOS_Real_Received_BY.setVisibility(View.VISIBLE);
            Txt_DOS_Real_Received_By_NIK.setVisibility(View.VISIBLE);
            Lbl_DOS_Real_Received_By_NIK.setVisibility(View.VISIBLE);
        }
        else{
            //Log.d("[GudangGaram]", " INVISIBLE " + key.toString());
            Txt_DOS_Real_Received_By.setVisibility(View.INVISIBLE);
            Lbl_DOS_Real_Received_BY.setVisibility(View.INVISIBLE);
            Txt_DOS_Real_Received_By_NIK.setVisibility(View.INVISIBLE);
            Lbl_DOS_Real_Received_By_NIK.setVisibility(View.INVISIBLE);
            Txt_DOS_Real_Received_By.setText("");
            Txt_DOS_Real_Received_By_NIK.setText("");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tprocess_doreceive);

        context = this;

        config = getSharedPreferences("MyDOMAESetting", MODE_PRIVATE);

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();
        State_TTD = false;

        signaturePad                 = (SignaturePad) findViewById(R.id.signature_pad);
        Cbo_DOS_Location             = (Spinner)findViewById(R.id.cbo_DOS_Location);
        Cbo_DOS_NomorKendaraan       = (Spinner)findViewById(R.id.cbo_DOS_NomorKendaraan);
        Cbo_DOS_TransactionNumber    = (Spinner)findViewById(R.id.cbo_DOS_TransactionNumber);
        Cbo_DOS_ReceivedBy           = (Spinner)findViewById(R.id.cbo_DOS_ReceivedBy);

        Cmd_populate           = (Button)findViewById(R.id.cmd_populate);
        Cmd_Cancel             = (Button)findViewById(R.id.cmd_cancel);
        Cmd_Clear              = (Button)findViewById(R.id.cmd_clear);
        Cmd_Save               = (Button)findViewById(R.id.cmd_save);
        Cmd_DOS_ReceivedByLOV  = (ImageButton)findViewById(R.id.cmd_DOS_ReceivedByLOV);

        Lbl_DOS_ReceivedByID         = (TextView) findViewById(R.id.lbl_DOS_ReceivedByID);
        Txt_DOS_Real_Received_By     = (EditText)findViewById(R.id.txt_DOS_Real_Received_By);
        Txt_DOS_Real_Received_By_NIK = (EditText)findViewById(R.id.txt_DOS_Real_Received_By_NIK);
        Lbl_DOS_Real_Received_By_NIK = (TextView)findViewById(R.id.lbl_DOS_Real_Received_BY);
        Lbl_DOS_Real_Received_BY     = (TextView)findViewById(R.id.lbl_DOS_Real_Received_By_NIK);
        Txt_DOS_ReceivedBy           = (TextView)findViewById(R.id.txt_DOS_ReceivedBy);

        Cmd_Save.setOnClickListener(this);
        Cmd_Clear.setOnClickListener(this);
        Cmd_Cancel.setOnClickListener(this);
        Cmd_populate.setOnClickListener(this);
        Cmd_DOS_ReceivedByLOV.setOnClickListener(this);

        try{
            host = (TabHost)findViewById(R.id.tab_dos);
            host.setup();

            //Tab 1
            TabHost.TabSpec spec = host.newTabSpec("BinPersiapan");
            spec.setContent(R.id.tab1);
            spec.setIndicator("Bin Persiapan");
            host.addTab(spec);

            //Tab 2
            spec = host.newTabSpec("Signature");
            spec.setContent(R.id.tab2);
            spec.setIndicator("Signature");
            host.addTab(spec);

            // focus on tab 1
            host.setCurrentTab(0);
            // init host background
            host.getTabWidget().getChildAt(0).setBackgroundColor(Color.RED);
            host.getTabWidget().getChildAt(1).setBackgroundColor(Color.CYAN);

            // host listener
            host.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
                @Override
                public void onTabChanged(String tabId) {
                    int tab = host.getCurrentTab();
                    if(tabId == "Signature"){
                        host.getTabWidget().getChildAt(1).setBackgroundColor(Color.RED);
                        host.getTabWidget().getChildAt(0).setBackgroundColor(Color.CYAN);
                    }
                    else{
                        host.getTabWidget().getChildAt(1).setBackgroundColor(Color.CYAN);
                        host.getTabWidget().getChildAt(0).setBackgroundColor(Color.RED);
                    }
                }
            });
        }
        catch(Exception e){
        }

        // init command
        Cmd_Clear.setEnabled(false);
        Cmd_Save.setEnabled(false);
        Txt_DOS_Real_Received_By.setVisibility(View.INVISIBLE);
        Txt_DOS_Real_Received_By_NIK.setVisibility(View.INVISIBLE);
        Lbl_DOS_Real_Received_By_NIK.setVisibility(View.INVISIBLE);
        Lbl_DOS_Real_Received_BY.setVisibility(View.INVISIBLE);

        expandableListView = (ExpandableListView) findViewById(R.id.lst_doservice);
        expandableListView.setGroupIndicator(null);

        // signaturepad init
        try{
            signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                //@Override
                public void onStartSigning() {
                }
                @Override
                public void onSigned() {
                    State_TTD = true;
                    // check if detail is already populated
                    try{
                        if(expandableListView.getCount() > 0){
                            Cmd_Save.setEnabled(true);
                            Cmd_Clear.setEnabled(true);
                        }
                        else{
                            Cmd_Save.setEnabled(false);
                            Cmd_Clear.setEnabled(true);
                        }
                    }
                    catch(Exception e){
                    }
                }
                @Override
                public void onClear() {
                    State_TTD = false;
                    Cmd_Clear.setEnabled(false);
                    Cmd_Save.setEnabled(false);
                }
            });
        }
        catch(Exception e){
        }

        try{
            // clear temporary flag and reason code if not already assigned REF_SENT_ID
            dbHelper.UpdateDOSReasonNFlag();
        }
        catch(Exception e){
        }

        initComboBox(R.id.cbo_DOS_NomorKendaraan,initListCBNoKendaraan());
        initComboBox(R.id.cbo_DOS_TransactionNumber,initListCBTransactionNumber());
        initComboBox(R.id.cbo_DOS_Location, initListCBLocation());
        initKVPComboBox(R.id.cbo_DOS_ReceivedBy);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            // clear temporary flag and reason code and qty received if not already assigned REF_SENT_ID
            dbHelper.UpdateDOSReasonNFlag();
        }
        catch(Exception e){
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.cmd_clear) {
            EH_CMD_CLEAR();
        }
        else if (id == R.id.cmd_save) {
            EH_CMD_SAVE();
        }
        else if(id == R.id.cmd_cancel){
            EH_CMD_CANCEL();
        }
        else if(id == R.id.cmd_populate){
            EH_CMD_POPULATE();
        }
        else if(id == R.id.cmd_DOS_ReceivedByLOV){
            EH_CMD_LOV_RECEIVEDBY();
        }
    }

    // ============== EVENT HANDLER ==================================================
    private void EH_CMD_LOV_RECEIVEDBY(){
        Log.d("[GudangGaram]", "TProcessDOReceive :: EH_CMD_LOV_RECEIVEDBY");
        // --- move focus cursor
        Txt_DOS_ReceivedBy.requestFocus();
        // --- summon lov
        lst_ReceivedBy = dbHelper.LoadCboEmployee();
        FragmentManager ListOfValue = getFragmentManager();
        LovEmpFragment lov_ReceivedBy = new LovEmpFragment();
        lov_ReceivedBy.setContext(context);
        lov_ReceivedBy.SetListOfValue(lst_ReceivedBy);
        lov_ReceivedBy.show(ListOfValue,"LOV Received By");
    }

    private void EH_REFRESH(){
        initComboBox(R.id.cbo_DOS_NomorKendaraan,initListCBNoKendaraan());
        initComboBox(R.id.cbo_DOS_Location, initListCBLocation());
        initComboBox(R.id.cbo_DOS_TransactionNumber,initListCBTransactionNumber());
        initKVPComboBox(R.id.cbo_DOS_ReceivedBy);
    }

    public void EH_CMD_POPULATE(){
        Integer ErrCount;
        ErrCount = 0;
        // focus on tab 1
        host.setCurrentTab(0);
        try {
            if(Cbo_DOS_NomorKendaraan.getCount() > 0){
                Scbo_DOS_NomorKendaraan     = Cbo_DOS_NomorKendaraan.getSelectedItem().toString();
                if (Scbo_DOS_NomorKendaraan.trim().length() == 0) {
                    Toast.makeText(this, "Parameter Nomor Kendaraan Should Be Entered", Toast.LENGTH_LONG).show();
                    ErrCount += 1;
                }
            }
            else {
                ErrCount += 1;
            }

            if(Cbo_DOS_Location.getCount() > 0) {
                Scbo_DOS_Location           = Cbo_DOS_Location.getSelectedItem().toString();
                if (Scbo_DOS_Location.trim().length() == 0) {
                    Toast.makeText(this, "Parameter Location Should Be Entered", Toast.LENGTH_LONG).show();
                    ErrCount += 1;
                }
            }
            else{
                ErrCount += 1;
            }

            if(Cbo_DOS_TransactionNumber.getCount() > 0){
                Scbo_DOS_TransactionNumber  = Cbo_DOS_TransactionNumber.getSelectedItem().toString();
                if (Scbo_DOS_TransactionNumber.trim().length() == 0) {
                    Toast.makeText(this, "Parameter Transaction Number Should Be Entered", Toast.LENGTH_LONG).show();
                    ErrCount += 1;
                }
            }
            else{
                ErrCount += 1;
            }

            if(ErrCount == 0){
                PopulateItems(Scbo_DOS_NomorKendaraan,Scbo_DOS_Location,Scbo_DOS_TransactionNumber);
            }
            else{
                Toast.makeText(this, "No Data Found", Toast.LENGTH_LONG).show();
            }
        }
        catch(Exception e){
        }
    }

    private void EH_CMD_CLEAR(){
        try{
            State_TTD = false;
            // clear signature pad
            signaturePad.clear();

            // reseting combobox
            initComboBox(R.id.cbo_DOS_NomorKendaraan,initListCBNoKendaraan());
            initComboBox(R.id.cbo_DOS_Location, initListCBLocation());
            initComboBox(R.id.cbo_DOS_TransactionNumber,initListCBTransactionNumber());
            // resetting populate list
            adapter = null;
            expandableListView.setAdapter(adapter);
            // clear received ID
            Lbl_DOS_ReceivedByID.setText("");
            // clear received by name
            Txt_DOS_ReceivedBy.setText("");

            // clear Real Received
            Txt_DOS_Real_Received_By.setText("");
            Txt_DOS_Real_Received_By_NIK.setText("");

            initKVPComboBox(R.id.cbo_DOS_ReceivedBy);
        }
        catch(Exception e){
            Log.d("[GudangGaram]","EH_CMD_CLEAR Exception " + e.toString());
        }
    }

    private void EH_CMD_CANCEL(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void EH_CMD_SAVE(){
        long DO_Header_ID;
        Scbo_DOS_NomorKendaraan     = Cbo_DOS_NomorKendaraan.getSelectedItem().toString();
        Scbo_DOS_Location           = Cbo_DOS_Location.getSelectedItem().toString();
        Scbo_DOS_TransactionNumber  = Cbo_DOS_TransactionNumber.getSelectedItem().toString();
        Scbo_DOS_ReceivedBy         = Txt_DOS_ReceivedBy.getText().toString();
        // Cbo_DOS_ReceivedBy diganti dengan LOV Fragment Popup Dialog
        //Scbo_DOS_ReceivedBy         = Cbo_DOS_ReceivedBy.getSelectedItem().toString();

        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder
                    .setTitle("Save DO Confirm ")
                    .setMessage("Confirm Save DO ?")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    EH_CMD_SAVE_ACTION();
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
        }
    }

    public Integer check_validate_input(String p_vehicle_no,String p_location, String p_trx_no,String p_rcv_by, String p_real_rcv_by, String p_real_rcv_by_nik){
        Integer err;
        Integer Result1;
        Integer Result2;
        err = 0;

        String wherekueri1 = "WHERE MD_CHECK_LOADING = 'Y' " +
                "  AND MD_REF_SENT IS NULL " +
                "  AND MD_VEHICLE_NO    = '" + p_vehicle_no + "' " +
                "  AND MD_LOCATION      = '" + p_location + "' " +
                "  AND MD_TRX_NUMBER    = '" + p_trx_no + "' " +
                "  AND MD_CHECK_SENT    = 'Y' ";

        String wherekueri2 = "WHERE MD_CHECK_LOADING = 'Y' " +
                "  AND MD_REF_SENT IS NULL " +
                "  AND MD_VEHICLE_NO    = '" + p_vehicle_no + "' " +
                "  AND MD_LOCATION      = '" + p_location + "' " +
                "  AND MD_TRX_NUMBER    = '" + p_trx_no + "' " +
                "  AND TRIM(COALESCE(MD_CHECK_SENT,'N')) <> 'Y' " +
                "  AND length(TRIM(COALESCE(MD_REASON_CODE,''))) = 0 ";


        // validate if received by un register employee
        if(p_rcv_by.trim().length() >0){
            if(p_rcv_by.equals("Unregistered Employee,")){
                if(p_real_rcv_by.trim().length() == 0){
                    Toast.makeText(this, "Real Received By Should Be Mentioned", Toast.LENGTH_LONG).show();
                    err +=1;
                }
                if(p_real_rcv_by_nik.trim().length() == 0){
                    Toast.makeText(this, "NIK Should Be Mentioned", Toast.LENGTH_LONG).show();
                    err +=1;
                }
            }
        }
        else{
            Toast.makeText(this, "Received By Should Be Selected", Toast.LENGTH_LONG).show();
            err +=1;
        }

        // validate record should be at least 1 select
        /*
        Result1 = dbHelper.getRecordCount("GGGG_DOMAE_DEVICE_D", wherekueri1);
        if(Result1 == 0){
            err +=1;
            Toast.makeText(this, "None Item Selected", Toast.LENGTH_LONG).show();
        }
        */

        Result2 = dbHelper.getRecordCount("GGGG_DOMAE_DEVICE_D",wherekueri2);
        if(Result2 > 0){
            err +=1;
            Toast.makeText(this, "Item Not Selected Without Any Reason Mentioned ", Toast.LENGTH_LONG).show();
        }


        return err;
    }


    private void EH_CMD_SAVE_ACTION(){
        String SID;
        String Filename;
        String DeviceID;
        String ReasonCode;
        byte[] SignatureImage;
        long SignatureSize;

        Log.d("[GudangGaram]","EH_CMD_SAVE");

        Scbo_DOS_ReceivedByID   = Lbl_DOS_ReceivedByID.getText().toString();
        Scbo_DOS_ReceivedBy     = Txt_DOS_ReceivedBy.getText().toString();
        // Cbo_DOS_ReceivedBy diganti dengan LOV Fragment Popup Dialog
        //Scbo_DOS_ReceivedBy     = Cbo_DOS_ReceivedBy.getSelectedItem().toString();

        ReceivedReal            = Txt_DOS_Real_Received_By.getText().toString();
        ReceivedRealNIK         = Txt_DOS_Real_Received_By_NIK.getText().toString();

        DeviceID                = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Filename                = String.format(DeviceID + "_%d.jpg", System.currentTimeMillis());

        if(Scbo_DOS_ReceivedBy.trim().length() > 0) {
            // validate records
            if (check_validate_input(Scbo_DOS_NomorKendaraan, Scbo_DOS_Location, Scbo_DOS_TransactionNumber,Scbo_DOS_ReceivedBy, ReceivedReal, ReceivedRealNIK) == 0) {
                SignatureImage  = createSignature(Filename);
                SignatureSize   = SignatureImage.length;

                try {
                    // insert manifest DO Header
                    Long DO_Header_ID = dbHelper.addManifestDOHeader(Scbo_DOS_NomorKendaraan, Scbo_DOS_Requestor, Scbo_DOS_Location, Scbo_DOS_TransactionNumber, Scbo_DOS_ReceivedByID, Scbo_DOS_ReceivedBy, SignatureImage, Filename, SignatureSize,ReceivedReal,ReceivedRealNIK);
                    if (DO_Header_ID > 0) {
                        Log.d("[GudangGaram]", "DO Saved With DO Header ID = " + DO_Header_ID.toString());
                        if (WG_GroupHeader.size() > 0) {
                            //for (int ax = 0; ax < WG_GroupHeader.size(); ax++) {
                            for (int bx = 0; bx < WG_ItemList.size(); bx++) {
                                //Log.d("[GudangGaram]","DO Saved With DO Header ID = " + WG_ItemList.get(bx).getSCheck());
                                SID = WG_ItemList.get(bx).getSID().toString();
                                // update ID di manifest DO
                                dbHelper.UpdateDOSManifest(SID, DO_Header_ID, Scbo_DOS_ReceivedByID, Scbo_DOS_ReceivedBy,ReceivedReal,ReceivedRealNIK);
                            }
                            //}
                        }
                    }
                } catch (Exception e) {
                    Log.d("[GudangGaram]", " EH_CMD_SAVE Exception " + e.getMessage().toString());
                    // rollback if its fail
                } finally {
                    EH_CMD_CLEAR();
                    Toast.makeText(this, "Manifest Saved And Flagged", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, "Cannot Save, Please Check Again Your Input", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(this, "Received By Should Be Entered", Toast.LENGTH_LONG).show();
        }
    }

    void PopulateItems(String p_DOS_NomorKendaraan, String p_DOS_Location, String p_DOS_TransactionNumber) {
        String xGroupName;
        Integer GroupCount;
        Integer ItemCount;
        Integer RecNum;
        ArrayList<HolderDOS>            ARLItemList;
        ArrayList<ArrayList<HolderDOS>> Matrix;


        List<String> MListBinPersiapan            = new ArrayList<String>();
        List<HolderDOS> MListBinPersiapanItem = new ArrayList<HolderDOS>();

        GroupHeader    = new ArrayList<String>();
        AllItemList    = new ArrayList<HolderDOS>();

        Matrix         = new ArrayList<ArrayList<HolderDOS>>();

        HashMap<String, List<HolderDOS>> hashMapDOS = new HashMap<String, List<HolderDOS>>();

        // populate query to get list bin persiapan as group header
        MListBinPersiapan = dbHelper.getList_DOS_BinPersiapan(p_DOS_NomorKendaraan,p_DOS_Location,p_DOS_TransactionNumber);

        GroupHeader.clear();
        AllItemList.clear();

        GroupCount = MListBinPersiapan.size();
        Log.d("[GudangGaram]"," Group Count : " + GroupCount);
        RecNum = 0;

        if(GroupCount > 0){
            // Adding headers to list
            for (int gi = 0; gi < GroupCount; gi++) {
                // get and add header group name
                xGroupName = MListBinPersiapan.get(gi).toString();
                GroupHeader.add(xGroupName);
                Log.d("[GudangGaram]","Load Header Group : " + xGroupName);
                ItemList       = new ArrayList<HolderDOS>();
                // populate query to get list item in each bin persiapan as item list detail
                MListBinPersiapanItem = dbHelper.getList_DOS_BinPersiapanItem(p_DOS_NomorKendaraan, p_DOS_Location,p_DOS_TransactionNumber, xGroupName);
                ItemCount = MListBinPersiapanItem.size();
                ARLItemList    = new ArrayList<HolderDOS>();

                Log.d("[GudangGaram]","Group [" + xGroupName + "] Item Count : " + Integer.toString(ItemCount));
                if(ItemCount > 0){
                    for(int ci = 0; ci < ItemCount; ci++){
                        // assign view holderdos with data from database
                        HolderDOS o = new HolderDOS();
                        o.setRecordNumber(RecNum);
                        o.setSID(MListBinPersiapanItem.get(ci).getSID());
                        o.setSItemCode(MListBinPersiapanItem.get(ci).getSItemCode());
                        o.setSItemDesc(MListBinPersiapanItem.get(ci).getSItemDesc());
                        o.setSQty(MListBinPersiapanItem.get(ci).getSQty());
                        o.setSUOM(MListBinPersiapanItem.get(ci).getSUOM());
                        o.setSRequestor(MListBinPersiapanItem.get(ci).getSRequestor());
                        o.setSQtyR(MListBinPersiapanItem.get(ci).getSQtyR());
                        o.setSReasonCode(MListBinPersiapanItem.get(ci).getSReasonCode());
                        o.setSCheck(false);
                        o.setEnableReason(true);

                        // check & reason code depends on user choose
                        Scbo_DOS_Requestor = MListBinPersiapanItem.get(ci).getSRequestor();

                        ItemList.add(o);
                        AllItemList.add(o);

                        ARLItemList.add(ci, o);

                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: ItemList (" + ItemList.size() + ")");
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Record Number   : " + RecNum);
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: index           : " + Integer.toString(ci));
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Trx ID          : " + MListBinPersiapanItem.get(ci).getSID());
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Item Code       : " + MListBinPersiapanItem.get(ci).getSItemCode());
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Item Desc       : " + MListBinPersiapanItem.get(ci).getSItemDesc());
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Item Qty        : " + MListBinPersiapanItem.get(ci).getSQty());
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Item Qty Rcv    : " + MListBinPersiapanItem.get(ci).getSQtyR());
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Item UOM        : " + MListBinPersiapanItem.get(ci).getSUOM());
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Requestor       : " + MListBinPersiapanItem.get(ci).getSRequestor());
                        Log.d("[GudangGaram]","Group [" + xGroupName + "] :: Reason Code     : " + MListBinPersiapanItem.get(ci).getSCheck());
                        Log.d("[GudangGaram]","---------------------------------------------------------------------------------------------------------------------");

                        RecNum = RecNum + 1;
                    }
                    Log.d("[GudangGaram]","ARLItemList Count ->" + ARLItemList.size());

                    hashMapDOS.put(GroupHeader.get(gi), ItemList);
                    Matrix.add(gi,ARLItemList);
                }
            } // end for

            adapter = new CustomListAdapterDOS(TProcessDOReceive.this, GroupHeader, hashMapDOS, Matrix);
            // fill data to adpater over expandablelistview
            expandableListView.setAdapter(adapter);
            WG_GroupHeader = GroupHeader;
            WG_ItemList    = AllItemList;
        }
        else{
            WG_GroupHeader = null;
            WG_ItemList    = null;
            Scbo_DOS_Requestor = " ";
        }

        // expand all group
        for (int xi = 0; xi < GroupCount; xi++) {
            expandableListView.expandGroup(xi);
        }
    }

    private void initKVPComboBox(final int p_layout){
        Spinner spinner = (Spinner) findViewById(p_layout);
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();
        lvi = dbHelper.getEmployeeList();
        // Creating adapter for spinner
        ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                Integer       key = (Integer) swt.tag;

                Log.d("[GudangGaram]", " KVP Value Selected >> " + swt);
                Log.d("[GudangGaram]", " KVP Key   Selected >> " + key.toString());

                Lbl_DOS_ReceivedByID.setText(key.toString());

                // kalo penerima tidak ada dalam list maka harus isi nama dan nik
                if(swt.toString().equals("Unregistered Employee,")){
                    Log.d("[GudangGaram]", " VISIBLE " + key.toString());
                    Txt_DOS_Real_Received_By.setVisibility(View.VISIBLE);
                    Lbl_DOS_Real_Received_BY.setVisibility(View.VISIBLE);
                    Txt_DOS_Real_Received_By_NIK.setVisibility(View.VISIBLE);
                    Lbl_DOS_Real_Received_By_NIK.setVisibility(View.VISIBLE);
                }
                else{

                    Log.d("[GudangGaram]", " INVISIBLE " + key.toString());
                    Txt_DOS_Real_Received_By.setVisibility(View.INVISIBLE);
                    Lbl_DOS_Real_Received_BY.setVisibility(View.INVISIBLE);
                    Txt_DOS_Real_Received_By_NIK.setVisibility(View.INVISIBLE);
                    Lbl_DOS_Real_Received_By_NIK.setVisibility(View.INVISIBLE);
                    Txt_DOS_Real_Received_By.setText("");
                    Txt_DOS_Real_Received_By_NIK.setText("");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initComboBox(final int p_layout, List<String> lvi)
    {
        Spinner spinner = (Spinner) findViewById(p_layout);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                try{
                    if(p_layout == R.id.cbo_DOS_NomorKendaraan){
                        initComboBox(R.id.cbo_DOS_Location, initListCBLocation());
                    }
                    else if(p_layout == R.id.cbo_DOS_Location) {
                        initComboBox(R.id.cbo_DOS_TransactionNumber,initListCBTransactionNumber());
                    }
                    // resetting populate list to clear previous
                    adapter = null;
                    expandableListView.setAdapter(adapter);
                }
                catch(Exception e){
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
    }

    // ========================== LOV ==============================================================
    private List<String> initListCBNoKendaraan(){
        List<String> olist = new ArrayList<String>();
        olist = dbHelper.getLOV("GGGG_INV_VEHICLE_DO");
        return olist;
    }
    private List<String> initListCBTransactionNumber(){
        List<String> olist = new ArrayList<String>();
        try{
            if(Cbo_DOS_NomorKendaraan.getCount() > 0 && Cbo_DOS_Location.getCount() > 0){
                Scbo_DOS_NomorKendaraan = Cbo_DOS_NomorKendaraan.getSelectedItem().toString();
                Scbo_DOS_Location       = Cbo_DOS_Location.getSelectedItem().toString();
                olist = dbHelper.getLOV_DOS_TrxNum(Scbo_DOS_NomorKendaraan,Scbo_DOS_Location);
            }
        }
        catch(Exception e){
        }
        return olist;
    }
    private List<String> initListCBLocation(){
        List<String> olist = new ArrayList<String>();
        try{
            if(Cbo_DOS_NomorKendaraan.getCount() >0){
                Scbo_DOS_NomorKendaraan     = Cbo_DOS_NomorKendaraan.getSelectedItem().toString();
                olist = dbHelper.getLOV_DOS_Location(Scbo_DOS_NomorKendaraan.toString());
            }
        }
        catch(Exception e){
        }
        return olist;
    }

    private List<String> InitListCBReason(){
        List<String> olist = new ArrayList<String>();
        olist = dbHelper.getLOV("GGGG_INV_REJECT_REASON_DO");
        return olist;
    }


    private byte[] createSignature(String FileName) {
        Bitmap signatureBitmap             = signaturePad.getSignatureBitmap();
        ByteArrayOutputStream OutputStream = new ByteArrayOutputStream();
        //signatureBitmap.compress(Bitmap.CompressFormat.JPEG,100, OutputStream); // Could be Bitmap.CompressFormat.PNG or Bitmap.CompressFormat.WEBP
        signatureBitmap.compress(Bitmap.CompressFormat.JPEG,80, OutputStream); // Could be Bitmap.CompressFormat.PNG or Bitmap.CompressFormat.WEBP

        byte[] SignatureFileResult = OutputStream.toByteArray();

        if (addJpgSignatureToGallery(signatureBitmap, FileName)) {
            Toast.makeText(this, "JPG format saved into Gallery", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Failed to save jpg format to Gallery", Toast.LENGTH_SHORT).show();
        }
        return SignatureFileResult;
    }

    public File getAlbumStorageDir(String albumName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.d("TAG", "Directory not created");
        }
        return file;
    }

    private void saveBitmapToJPG(Bitmap bitmap, File photo) {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        try {
            OutputStream stream = new FileOutputStream(photo);
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            stream.close();
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean addJpgSignatureToGallery(Bitmap signature, String filename) {
        boolean result = false;

        File photo = new File(getAlbumStorageDir("MYDOMAE_SIGNATURE"), filename);
        saveBitmapToJPG(signature, photo);
        scanMediaFile(photo);
        result = true;
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private boolean saveSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
        try {
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  result;
    }

}
