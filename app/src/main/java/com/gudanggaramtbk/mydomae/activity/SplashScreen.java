package com.gudanggaramtbk.mydomae.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gudanggaramtbk.mydomae.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }
}
