package com.gudanggaramtbk.mydomae.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import android.view.View.OnClickListener;
import android.widget.ProgressBar;

import com.gudanggaramtbk.mydomae.R;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class SettingActivity extends AppCompatActivity {
    private Button CmdSaveSetting,CmdTestConn;
    private TextView TxtSoapAddress, TxtSoapNamespace,TxtUrlUpdater;

    String SSoapAddress,SSoapNamespace, SUrlUpdater;
    Boolean ScannerModeState;

    SharedPreferences config;
    ProgressDialog pd;

    String SSoapMethodTest = "Get_Test_Result";

    /*
            String SOAP_ADDRESS = "http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String SOAP_ACTION = "http://gudanggaramtbk.com/InsertToOracle";
            String OPERATION_NAME = "InsertToOracle";
            String NAMESPACE = "http://gudanggaramtbk.com/";
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        CmdSaveSetting = (Button)findViewById(R.id.cmd_saveSetting);
        CmdTestConn = (Button)findViewById(R.id.cmd_testConn);

        TxtSoapAddress = (TextView) findViewById(R.id.txt_SettingSOAPAddress);
        TxtSoapNamespace = (TextView) findViewById(R.id.txt_SettingSOAPNamespace);
        TxtUrlUpdater = (TextView) findViewById(R.id.txt_SettingURLUpdaters);

        config = getSharedPreferences("MyDOMAESetting", MODE_PRIVATE);

        OnClickListener EHSaveListener = new OnClickListener() {
            public void onClick(View v) {
                // go to next activity
                save_setting(config);
                Toast.makeText(getApplicationContext(),"Setting Saved", Toast.LENGTH_LONG).show();
                reload_setting(config);
            }
        };
        OnClickListener EHTestConnListener = new OnClickListener() {
            public void onClick(View v) {
                test_connection();
            }
        };

        CmdSaveSetting.setOnClickListener(EHSaveListener);
        reload_setting(config);
        CmdTestConn.setOnClickListener(EHTestConnListener);
    }

    public String test_connection(){
        String xfactor;
        String Message;
        Message = "Checking Connection ... Please Wait ...";

        xfactor = "S";
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SSoapAddress);
            httpTransport.debug = true;
            httpTransport.call( SSoapNamespace + SSoapMethodTest, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        catch(Exception ex){
            xfactor = ex.getMessage().toString();
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
        }
        if(xfactor == "S"){
            Toast.makeText(getApplicationContext(), "Test Connection Succeed ", Toast.LENGTH_LONG).show();
        }
        return xfactor;
    }

    private void save_setting(SharedPreferences xconfig){
        SharedPreferences.Editor editor = xconfig.edit();
        editor.putString("SoapAddress",TxtSoapAddress.getText().toString());
        editor.putString("SoapNamespace",TxtSoapNamespace.getText().toString());
        editor.putString("URLUpdater",TxtUrlUpdater.getText().toString());
        editor.commit();
    }

    private void reload_setting(SharedPreferences xconfig){
        SSoapAddress = xconfig.getString("SoapAddress", "");
        SSoapNamespace = xconfig.getString("SoapNamespace", "");
        SUrlUpdater = xconfig.getString("URLUpdater","");

        TxtSoapNamespace.setText(SSoapNamespace);
        TxtSoapAddress.setText(SSoapAddress);
        TxtUrlUpdater.setText(SUrlUpdater);
    }
}
