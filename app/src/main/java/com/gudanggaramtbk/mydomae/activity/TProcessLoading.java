package com.gudanggaramtbk.mydomae.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.holder.HolderNoTrx;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.util.StringWithTag;
import com.gudanggaramtbk.mydomae.viewholder.ViewHolderNoTrx;
import com.gudanggaramtbk.mydomae.service.SoapRetrieveBinPersiapan;
import com.gudanggaramtbk.mydomae.service.SoapRetrieveManifest;
import com.gudanggaramtbk.mydomae.service.SoapRetrieveRakPersiapan;

import java.util.ArrayList;
import java.util.List;

public class TProcessLoading extends AppCompatActivity {
    private SharedPreferences           config;
    private MySQLiteHelper dbHelper;
    private String                      Scbo_PL_NoKendaraan;
    private String                      Scbo_PL_RakPersiapan;
    private String                      Scbo_PL_LoadBy;
    private String                      Scbo_PL_LoadByID;

    private Button                      Cmd_PL_Populate;
    private Button                      Cmd_PL_Submit;
    private Button                      Cmd_PL_Cancel;
    private Spinner                     Cbo_PL_NoKendaraan;
    private Spinner                     Cbo_PL_RakPersiapan;
    private Spinner                     Cbo_PL_LoadBy;
    private TextView                    Lbl_PL_LoadByID;

    private ListView                    listBinPersiapan;

    ArrayAdapter<String>                adapter;
    //private ArrayAdapter<HolderNoTrx>           trxadapter;
    private List<HolderNoTrx>                   lvitrx;

    private SoapRetrieveManifest downloadManifest;
    private SoapRetrieveBinPersiapan populateBinPersiapan;
    private SoapRetrieveRakPersiapan populateRakPersiapan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tprocess_loading);

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        config = getSharedPreferences("MyDOMAESetting", MODE_PRIVATE);

        Cbo_PL_NoKendaraan  = (Spinner)findViewById(R.id.cbo_PL_NoKendaraan);
        Cbo_PL_RakPersiapan = (Spinner)findViewById(R.id.cbo_PL_RakPersiapan);
        Cbo_PL_LoadBy       = (Spinner)findViewById(R.id.cbo_PL_LoadBy);
        Lbl_PL_LoadByID     = (TextView)findViewById(R.id.lbl_PL_LoadByID);

        Cmd_PL_Populate     = (Button)findViewById(R.id.cmd_PL_Populate);
        Cmd_PL_Submit       = (Button)findViewById(R.id.cmd_PL_Submit);
        Cmd_PL_Cancel       = (Button)findViewById(R.id.cmd_PL_Cancel);

        Cmd_PL_Populate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_POPULATE_NO_TRX();
                // replaced by api v 1.4
                //EH_CMD_POPULATE_BIN_PERSIAPAN();
            }
        });
        Cmd_PL_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    EH_CMD_SUBMIT();
            }
        });
        Cmd_PL_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_CANCEL();
            }
        });

        listBinPersiapan = (ListView) findViewById(R.id.lst_ProcessLoading);
        listBinPersiapan.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        // initialized soap service
        populateBinPersiapan = new SoapRetrieveBinPersiapan(config);
        populateBinPersiapan.setContext(this);
        populateBinPersiapan.setDBHelper(dbHelper);

        populateRakPersiapan = new SoapRetrieveRakPersiapan(config);
        populateRakPersiapan.setContext(this);
        populateRakPersiapan.setDBHelper(dbHelper);

        downloadManifest = new SoapRetrieveManifest(config,listBinPersiapan);
        downloadManifest.setContext(this);
        downloadManifest.setDBHelper(dbHelper);

        // initialized combobox
        initComboBox(R.id.cbo_PL_NoKendaraan,initListCBNoKendaraan());
        initKVPComboBox(R.id.cbo_PL_LoadBy);

        // call WS to get update Rak Persiapan from Oracle
        initComboBoxRakPersiapan();
    }

    private Integer do_validation_input(){
        Integer err;
        err = 0;

        try{
            if(Cbo_PL_NoKendaraan.getCount() > 0) {
                Scbo_PL_NoKendaraan  = Cbo_PL_NoKendaraan.getSelectedItem().toString();
                if(Scbo_PL_NoKendaraan.trim().length() == 0){
                    Toast.makeText(this,"Please Select No Kendaraan", Toast.LENGTH_LONG).show();
                }
            }
            else{
                err +=1;
            }

            if(Cbo_PL_RakPersiapan.getCount() > 0) {
                Scbo_PL_RakPersiapan = Cbo_PL_RakPersiapan.getSelectedItem().toString();
                if(Scbo_PL_RakPersiapan.trim().length() == 0){
                    Toast.makeText(this,"Please Select Rak Persiapan", Toast.LENGTH_LONG).show();
                }
            }
            else {
                err +=1;
            }
        }
        catch (Exception e){
        }

        return err;
    }

    private void initKVPComboBox(final int p_layout){
        Spinner spinner = (Spinner) findViewById(p_layout);
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();
        lvi = dbHelper.getEmployeeListLoad();
        // Creating adapter for spinner
        ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                Integer       key = (Integer) swt.tag;

                Log.d("[GudangGaram]", " Load By KVP Value Selected >> " + swt);
                Log.d("[GudangGaram]", " Load By KVP Key   Selected >> " + key.toString());

                Lbl_PL_LoadByID.setText(key.toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initComboBoxRakPersiapan(){
        /*
        if(dbHelper.isNetworkConnected(config, TProcessLoading.this)){
            try{
                populateRakPersiapan.Retrieve(Cbo_PL_RakPersiapan);
            }
            catch(Exception e){
            }
        }
        else{
            Toast.makeText(this,"Connection Problem ", Toast.LENGTH_LONG).show();
            // close and go to main activity
            EH_CMD_CANCEL();
        }
        */

        try{
            populateRakPersiapan.Retrieve(listBinPersiapan, Cbo_PL_RakPersiapan);
        }
        catch(Exception e){
        }
    }

    private List<String> initListCBNoKendaraan(){
        List<String> olist = new ArrayList<String>();
        olist = dbHelper.getLOV("GGGG_INV_VEHICLE_DO");
        return olist;
    }

    private List<String> initListLoaderBy(){
        List<String> olist = new ArrayList<String>();
        olist = dbHelper.getLOV("GGGG_INV_VEHICLE_DO");
        return olist;
    }

    private void initComboBox(int p_layout, List<String> lvi)
    {
        Spinner spinner = (Spinner) findViewById(p_layout);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lvi);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                // refresh list
                adapter = null;
                listBinPersiapan.setAdapter(adapter);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void EH_CMD_CANCEL(){
        Log.d("[GudangGaram]: ","EH_CMD_CANCEL");
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void EH_CMD_POPULATE_NO_TRX(){
        Integer err;

        try{
            err = do_validation_input();
            if(err == 0){
                Log.d("[GudangGaram]:", "populateBinPersiapan.Retrieve >> ");
                Log.d("[GudangGaram]:", "Bin Persiapan  : " + listBinPersiapan);
                Log.d("[GudangGaram]:", "Rak Persiapan  : " + Scbo_PL_RakPersiapan);
                // call WS to retrieve Bin Persiapan from oracle
                lvitrx = populateBinPersiapan.Retrieve(listBinPersiapan, Scbo_PL_RakPersiapan);

                Log.d("[GudangGaram]:", "Post Result Count  : " + lvitrx.size());
            }
            else{
                Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
            }
        }
        catch(Exception e){
        }
    }

    public void EH_CMD_POPULATE_BIN_PERSIAPAN(){
        /* no longer to be used since v.1.4  */
        Integer err;
        err = 0;

        try{
            if(Cbo_PL_NoKendaraan.getCount() > 0) {
                Scbo_PL_NoKendaraan  = Cbo_PL_NoKendaraan.getSelectedItem().toString();
                if(Scbo_PL_NoKendaraan.trim().length() == 0){
                    Toast.makeText(this,"Please Select No Kendaraan", Toast.LENGTH_LONG).show();
                }
            }
            else{
                err +=1;
            }

            if(Cbo_PL_RakPersiapan.getCount() > 0) {
                 Scbo_PL_RakPersiapan = Cbo_PL_RakPersiapan.getSelectedItem().toString();
                 if(Scbo_PL_RakPersiapan.trim().length() == 0){
                    Toast.makeText(this,"Please Select Rak Persiapan", Toast.LENGTH_LONG).show();
                 }
            }
            else {
                err +=1;
            }

            if(err == 0){
                Log.d("[GudangGaram]:", "populateBinPersiapan.Retrieve >> ");
                Log.d("[GudangGaram]:", "Bin Persiapan  : " + listBinPersiapan);
                Log.d("[GudangGaram]:", "Rak Persiapan  : " + Scbo_PL_RakPersiapan);
                // call WS to retrieve Bin Persiapan from oracle
                populateBinPersiapan.Retrieve(listBinPersiapan, Scbo_PL_RakPersiapan);
                    /*
                    if (dbHelper.isNetworkConnected(config, TProcessLoading.this)) {
                        try {
                            populateBinPersiapan.Retrieve(listBinPersiapan, Scbo_PL_RakPersiapan);
                        }
                        catch (Exception e) {
                            Log.d("[GudangGaram]:", "Exception populateBinPersiapan.Retrieve  : " + e.getMessage().toString());
                        }
                    }
                    else {
                            Toast.makeText(this,"Connection Problem ", Toast.LENGTH_LONG).show();
                    }
                    */
            }
            else{
                Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
            }
        }
        catch(Exception e){
        }
    }

    private void EH_CMD_SUBMIT_ACTION(){
        long DOLoadingHeader;
        Integer err;
        String SBin_Persiapan;
        String STrx_No;

        err = 0;

        try{
            if(Cbo_PL_NoKendaraan.getCount() > 0){
                Scbo_PL_NoKendaraan  = Cbo_PL_NoKendaraan.getSelectedItem().toString();
            }
            else{
                Toast.makeText(this,"No Kendaraan Should Be Selected ", Toast.LENGTH_LONG).show();
                err +=1;
            }
            if(Cbo_PL_RakPersiapan.getCount() > 0){
                Scbo_PL_RakPersiapan = Cbo_PL_RakPersiapan.getSelectedItem().toString();
            }
            else{
                Toast.makeText(this,"Rak Persiapan Should Be Selected ", Toast.LENGTH_LONG).show();
                err +=1;
            }
            if(Cbo_PL_LoadBy.getCount() > 0){
                Scbo_PL_LoadBy   = Cbo_PL_LoadBy.getSelectedItem().toString();
                Scbo_PL_LoadByID = Lbl_PL_LoadByID.getText().toString();

                if(Scbo_PL_LoadBy.trim().length() > 0){
                    if(Scbo_PL_LoadBy.trim().equals("")){
                        Toast.makeText(this,"Petugas Delivery Should Be Selected ", Toast.LENGTH_LONG).show();
                        err +=1;
                    }
                }
                else{
                    Toast.makeText(this,"Petugas Delivery Should Be Selected ", Toast.LENGTH_LONG).show();
                    err +=1;
                }
            }
            else{
                Toast.makeText(this,"None Of Petugas Delivery Can Be Selected ", Toast.LENGTH_LONG).show();
                err +=1;
            }


            if(err == 0){
                SparseBooleanArray checkedItems = listBinPersiapan.getCheckedItemPositions();
                Log.d("[GudangGaram]:", "checkedItems >> " +  checkedItems.size());


                if (checkedItems != null) {
                    // save DO Header for Local Database
                    DOLoadingHeader = dbHelper.addManifestDOLoading(Scbo_PL_NoKendaraan,Scbo_PL_RakPersiapan, Scbo_PL_LoadBy,Scbo_PL_LoadByID);
                    if(DOLoadingHeader > 0){
                        for (int i=0; i<checkedItems.size(); i++) {
                            if (checkedItems.valueAt(i)) {
                                View HoldView = listBinPersiapan.getAdapter().getView(checkedItems.keyAt(i),null,null);
                                ViewHolderNoTrx holder = (ViewHolderNoTrx) HoldView.getTag();

                                STrx_No         = holder.Xtxt_no_trx.getText().toString();
                                SBin_Persiapan  = holder.Xtxt_bin_persiapan.getText().toString();

                                // call WS to retrieve manifest to local database based on Rak Persiapan & Bin Persiapan
                                downloadManifest.Retrieve(Cbo_PL_RakPersiapan,
                                                          Long.toString(DOLoadingHeader),
                                                          Scbo_PL_NoKendaraan,
                                                          Scbo_PL_RakPersiapan,
                                                          SBin_Persiapan,
                                                          STrx_No,
                                                          Scbo_PL_LoadBy,
                                                          Scbo_PL_LoadByID);
                            }
                        }
                        // init combobox
                        Toast.makeText(this,"Init Combobox NoKendaraan ", Toast.LENGTH_LONG).show();
                        initComboBox(R.id.cbo_PL_NoKendaraan,initListCBNoKendaraan());
                    }
                }
            }
            else{
                Toast.makeText(getApplicationContext(), "Cannot Submit, Please Check Again Your Input", Toast.LENGTH_LONG).show();
            }

        }
        catch(Exception e){
        }
    }

    private void EH_CMD_SUBMIT(){
        if(listBinPersiapan.getCount() > 0){

            if(listBinPersiapan.getCheckedItemCount() > 0){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder
                        .setTitle("Submit Confirm")
                        .setMessage("Confirm Submit ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_SUBMIT_ACTION();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            else{
                Toast.makeText(getApplicationContext(),"No Data Selected", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getApplicationContext(),"No Data Can Be Submitted", Toast.LENGTH_LONG).show();
        }
    }



}
