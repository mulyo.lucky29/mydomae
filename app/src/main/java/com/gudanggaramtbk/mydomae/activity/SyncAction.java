package com.gudanggaramtbk.mydomae.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.service.SoapRetrieveEmployee;
import com.gudanggaramtbk.mydomae.service.SoapRetrieveLookup;
import com.gudanggaramtbk.mydomae.service.SoapSendDataBase;
import com.gudanggaramtbk.mydomae.service.SoapSendDoHeader;

public class SyncAction extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private SoapSendDataBase SendDB;
    private SoapRetrieveLookup downloadMLookupSoap;
    private SoapRetrieveEmployee downloadMEmployeeSoap;
    private SoapSendDoHeader SendDOH;

    private Button Cmd_DLookup;
    private Button Cmd_DEmployee;
    private Button Cmd_SDOService;
    private Button Cmd_backupDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_action);
        config = getSharedPreferences("MyDOMAESetting", MODE_PRIVATE);
        context = this;
        dbHelper = new MySQLiteHelper(context);
        dbHelper.getWritableDatabase();

        // initialize soap downloader master
        downloadMLookupSoap = new SoapRetrieveLookup(config, context, dbHelper);
        // initialize soap downloader emloyee
        downloadMEmployeeSoap = new SoapRetrieveEmployee(config, context, dbHelper);
        // initialize SoapSendDoHeader
        SendDOH = new SoapSendDoHeader(config, context, dbHelper);
        // initialize SoapSendDB
        SendDB = new SoapSendDataBase(config, context, dbHelper);

        Cmd_DLookup     = (Button)findViewById(R.id.cmd_DLookup);
        Cmd_DEmployee   = (Button)findViewById(R.id.cmd_DEmployee);
        Cmd_SDOService  = (Button)findViewById(R.id.cmd_SDOService);
        Cmd_backupDB    = (Button) findViewById(R.id.cmd_backupDB);

        Cmd_DLookup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SYNC_M_LOOKUP();
            }
        });
        Cmd_DEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SYNC_M_EMPLOYEE();
            }
        });
        Cmd_SDOService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SYNC_DO();
            }
        });
        Cmd_backupDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_BACKUP_DB();
            }
        });
    }

    public void EH_CMD_BACKUP_DB(){
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                String device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                SendDB.Send(device_id);
            }
            catch(Exception e){
                Toast.makeText(context,"Backup DB Exception > " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
            finally {
            }
        }
        else{
            Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }
    public void EH_CMD_SYNC_M_LOOKUP(){
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                downloadMLookupSoap.Retrieve();
            }
            catch(Exception e){
                Toast.makeText(context,"Download Master Lookup Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
            finally {
                Toast.makeText(context,"Download Master Lookup Done ", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }
    public void EH_CMD_SYNC_M_EMPLOYEE(){
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                downloadMEmployeeSoap.Retrieve();
            }
            catch(Exception e){
                Toast.makeText(context,"Download Master Employee Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
            finally {
                Toast.makeText(context,"Download Master Employee Done ", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }
    public void EH_CMD_SYNC_DO(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Sync To Oracle Confirm")
                .setMessage("Confirm Sync To Oracle ?")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        EH_Action_Sync();
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_Action_Sync(){
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                SendDOH.Send();
                Toast.makeText(context,"Sending Manifest Done ", Toast.LENGTH_LONG).show();
            }
            catch(Exception e){
                Toast.makeText(context,"Sending Manifest Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }
}
