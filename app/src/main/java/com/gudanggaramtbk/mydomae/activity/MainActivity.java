package com.gudanggaramtbk.mydomae.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.adapter.CustomListAdapter;
import com.gudanggaramtbk.mydomae.adapter.CustomListAdapterLO;
import com.gudanggaramtbk.mydomae.holder.HolderManifestList;
import com.gudanggaramtbk.mydomae.holder.HolderManifestListLO;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    private MySQLiteHelper dbHelper;
    private SharedPreferences        config;
    private TextView                 SDeviceID;
    private List<HolderManifestList>   lviDO;
    private ListView                   listDO;

    private List<HolderManifestListLO> lviLO;
    private ListView                   listLO;

    private CustomListAdapter adapterDO;
    private CustomListAdapterLO adapterLO;

    private Integer                  LPosition;
    //private SoapSendDoHeader         SendDOH;
    boolean doubleBackToExitPressedOnce = false;
    TabHost hostList;

    public ListView getListMain() {
        return listDO;
    }

    private static final int REQUEST_PERMISSION = 1;

    private static String[] PERMISSIONS_APPS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    public void check_date(){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try{
            Date date1 = format.parse("2020-01-01");
            Date date2 = format.parse("2020-02-01");
            Date date3 = format.parse("2020-03-01");
            Date date4 = format.parse("2020-04-01");
            Date date5 = format.parse("2020-05-01");
            Date date6 = format.parse("2020-06-01");
            Date date7 = format.parse("2020-07-01");
            Date date8 = format.parse("2020-08-01");
            Date date9 = format.parse("2020-09-01");
            Date date10 = format.parse("2020-10-01");
            Date date11 = format.parse("2020-11-01");
            Date date12 = format.parse("2020-12-01");
            String tgl1 = new SimpleDateFormat("dd-MMM-yy").format(date1);
            String tgl2 = new SimpleDateFormat("dd-MMM-yy").format(date2);
            String tgl3 = new SimpleDateFormat("dd-MMM-yy").format(date3);
            String tgl4 = new SimpleDateFormat("dd-MMM-yy").format(date4);
            String tgl5 = new SimpleDateFormat("dd-MMM-yy").format(date5);
            String tgl6 = new SimpleDateFormat("dd-MMM-yy").format(date6);
            String tgl7 = new SimpleDateFormat("dd-MMM-yy").format(date7);
            String tgl8 = new SimpleDateFormat("dd-MMM-yy").format(date8);
            String tgl9 = new SimpleDateFormat("dd-MMM-yy").format(date9);
            String tgl10 = new SimpleDateFormat("dd-MMM-yy").format(date10);
            String tgl11 = new SimpleDateFormat("dd-MMM-yy").format(date11);
            String tgl12 = new SimpleDateFormat("dd-MMM-yy").format(date12);
            Log.d("[GudangGaram]", "Tgl 1" + tgl1);
            Log.d("[GudangGaram]", "Tgl 2" + tgl2);
            Log.d("[GudangGaram]", "Tgl 3" + tgl3);
            Log.d("[GudangGaram]", "Tgl 4" + tgl4);
            Log.d("[GudangGaram]", "Tgl 5" + tgl5);
            Log.d("[GudangGaram]", "Tgl 6" + tgl6);
            Log.d("[GudangGaram]", "Tgl 7" + tgl7);
            Log.d("[GudangGaram]", "Tgl 8" + tgl8);
            Log.d("[GudangGaram]", "Tgl 9" + tgl9);
            Log.d("[GudangGaram]", "Tgl 10" + tgl10);
            Log.d("[GudangGaram]", "Tgl 11" + tgl11);
            Log.d("[GudangGaram]", "Tgl 12" + tgl12);

        }
        catch(ParseException ex){
        }


    }

    void check_apps_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    PERMISSIONS_APPS,
                    REQUEST_PERMISSION
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //}

        config = getSharedPreferences("MyDOMAESetting", MODE_PRIVATE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_refresh();
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();
        dbHelper.dataCleansing();

        // -------- navigation bat ------------
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        TextView nav_device_id = (TextView)hView.findViewById(R.id.txt_xdevice_id);
        nav_device_id.setText(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));


        // -------- reload list manifest yang sudah di received untuk di lakukan sync --------------
         listDO = (ListView) findViewById(R.id.lst_manifestDO);
         listLO = (ListView) findViewById(R.id.lst_manifestLO);

         EH_cmd_ListRefreshDO(this);
         EH_cmd_ListRefreshLO(this);


        try{
            hostList = (TabHost)findViewById(R.id.TabMain);
            hostList.setup();
            //Tab 1
            TabHost.TabSpec spec = hostList.newTabSpec("Loading List");
            spec.setContent(R.id.MTab_Loading);
            spec.setIndicator("Loading List");
            hostList.addTab(spec);

            //Tab 2
            spec = hostList.newTabSpec("Receiving List");
            spec.setContent(R.id.MTab_Receiving);
            spec.setIndicator("Receiving List");
            hostList.addTab(spec);

            // focus on tab 1
            hostList.setCurrentTab(0);
        }
        catch(Exception e){
        }

        // check application permission
        check_apps_permission();

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void EH_CMD_EraseAll(){
        dbHelper.flushAll();
        EH_cmd_refresh();
        //EH_cmd_ListRefreshDO(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.mn_flushData) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Warning, This Action Will Erase All Data and its Irreversible, Continue ?");

            // set dialog message
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Proceed",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                EH_CMD_EraseAll();
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.nav_process_loading){
            EH_cmd_process_loading();
        }
        else if(id == R.id.nav_delivery_service){
            EH_cmd_delivery_service();
        }
        else if(id == R.id.nav_sync){
            EH_cmd_sync();
        }
        else if(id == R.id.nav_setting){
            EH_cmd_setting();
        }
        else if(id == R.id.nav_about){
            EH_cmd_about();
        }
        else if(id == R.id.nav_exit){
            EH_cmd_exit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void EH_cmd_edit(HolderManifestList o){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.rowlist, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Do Manifest Receive Cancel");
        alertDialogBuilder.setView(promptsView);

        final TextView Etxt_ID           = (TextView) promptsView.findViewById(R.id.txt_ID);
        final TextView Etxt_trx_num      = (TextView) promptsView.findViewById(R.id.txt_trx_num);
        final TextView Etxt_vehicle_no   = (TextView) promptsView.findViewById(R.id.txt_vehicle_no);
        final TextView Etxt_location     = (TextView) promptsView.findViewById(R.id.txt_location);
        final TextView Etxt_ReceivedBy   = (TextView) promptsView.findViewById(R.id.txt_ReceivedBy);
        final TextView Etxt_ReceivedDate = (TextView) promptsView.findViewById(R.id.txt_ReceivedDate);

        Etxt_ID.setText(o.getID().toString());
        Etxt_trx_num.setText(o.getTrx_num().toString());
        Etxt_ReceivedBy.setText(o.getReceivedBy().toString());
        Etxt_ReceivedDate.setText(o.getReceivedDate().toString());
        Etxt_vehicle_no.setText(o.getVehicle_no().toString());
        Etxt_location.setText(o.getLocation().toString());

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                EH_cmd_cancel(Etxt_ID.getText().toString());
                            }
                        })
                .setNegativeButton("Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
        alertDialog.getWindow().setLayout(700, 800);
    }

    // ============================= EH CMD =========================================
    public void EH_cmd_cancel(String ID){
        try{
            dbHelper.CancelReceiveManifest(ID);
        }
        catch(Exception e){
        }
        finally {
            EH_cmd_ListRefreshDO(this);
        }
    }

    public void EH_cmd_ListRefreshLO(Context ctx){
        try {
            lviLO = dbHelper.getAllItemListLO();
            adapterLO = new CustomListAdapterLO(ctx, lviLO);

            adapterLO.notifyDataSetChanged();

            listLO.setAdapter(adapterLO);
        }
        catch (Exception e) {
            Log.d("[GudangGaram]:", e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void EH_cmd_ListRefreshDO(Context ctx) {
        try {
            lviDO = dbHelper.getAllItemListDO();
            adapterDO = new CustomListAdapter(ctx, lviDO);
            adapterDO.notifyDataSetChanged();
            listDO.setAdapter(adapterDO);
            listDO.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    LPosition = position;
                    listDO.setSelection(position);
                }
            });

            listDO.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    HolderManifestList vx = new HolderManifestList();
                    vx.setID(lviDO.get(position).getID().toString());
                    vx.setTrx_num(lviDO.get(position).getTrx_num().toString());
                    vx.setReceivedBy(lviDO.get(position).getReceivedBy().toString());
                    vx.setReceivedDate(lviDO.get(position).getReceivedDate().toString());
                    vx.setVehicle_no(lviDO.get(position).getVehicle_no().toString());
                    vx.setLocation(lviDO.get(position).getLocation().toString());

                    EH_cmd_edit(vx);

                    return true;
                }
            });
        }
        catch (Exception e) {
            Log.d("[GudangGaram]:", e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void EH_cmd_process_loading(){
        Intent I_ProcessLoading;
        Toast.makeText(getApplicationContext(),"Process Loading", Toast.LENGTH_LONG).show();
        I_ProcessLoading = new Intent(this, TProcessLoading.class);
        startActivity(I_ProcessLoading);
    }
    public void EH_cmd_delivery_service(){
        Intent I_DOService;
        Toast.makeText(getApplicationContext(),"DO Service", Toast.LENGTH_LONG).show();
        I_DOService = new Intent(this, TProcessDOReceive.class);
        startActivity(I_DOService);
    }
    public void EH_cmd_setting(){
        Intent I_Setting;
        Toast.makeText(getApplicationContext(),"Setting", Toast.LENGTH_LONG).show();
        I_Setting = new Intent(this, SettingActivity.class);
        startActivity(I_Setting);
    }
    public void EH_cmd_about(){
        Intent I_About;
        Toast.makeText(getApplicationContext(),"About", Toast.LENGTH_LONG).show();
        I_About = new Intent(this, AboutActivity.class);
        startActivity(I_About);
    }
    public void EH_cmd_sync(){
        Intent I_Sync;
        Toast.makeText(getApplicationContext(),"Sync", Toast.LENGTH_LONG).show();
        I_Sync = new Intent(this, SyncAction.class);
        startActivity(I_Sync);
    }
    public void EH_cmd_refresh(){
        EH_cmd_ListRefreshDO(this);
        EH_cmd_ListRefreshLO(this);
    }
    public void EH_cmd_exit(){
        int pid;
        finish();
        pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
    }
}
