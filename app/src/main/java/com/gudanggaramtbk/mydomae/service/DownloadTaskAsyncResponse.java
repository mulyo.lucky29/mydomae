package com.gudanggaramtbk.mydomae.service;

/**
 * Created by LuckyM on 9/28/2017.
 */

public interface DownloadTaskAsyncResponse {
    void PostDownloadAction(String output);
}
