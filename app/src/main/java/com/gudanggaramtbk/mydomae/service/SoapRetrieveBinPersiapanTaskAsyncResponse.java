package com.gudanggaramtbk.mydomae.service;

import java.util.List;

/**
 * Created by LuckyM on 11/8/2017.
 */

public interface SoapRetrieveBinPersiapanTaskAsyncResponse {
    void PostSentAction(List<String> listoutput);
}
