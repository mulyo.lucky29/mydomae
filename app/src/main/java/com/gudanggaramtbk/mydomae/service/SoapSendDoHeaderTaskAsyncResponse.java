package com.gudanggaramtbk.mydomae.service;

/**
 * Created by LuckyM on 10/9/2017.
 */

public interface SoapSendDoHeaderTaskAsyncResponse {
    void PostSentAction(String output);
}
