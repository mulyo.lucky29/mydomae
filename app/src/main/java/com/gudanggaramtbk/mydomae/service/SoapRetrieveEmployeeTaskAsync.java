package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 10/6/2017.
 */

public class SoapRetrieveEmployeeTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;

    public SoapRetrieveEmployeeTaskAsync(){
    }

    public interface SoapRetrieveEmployeeTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveEmployeeTaskAsync.SoapRetrieveEmployeeTaskAsyncResponse delegate = null;
    public SoapRetrieveEmployeeTaskAsync(SoapRetrieveEmployeeTaskAsync.SoapRetrieveEmployeeTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config){

        Log.d("[GudangGaram]", "SoapRetrieveEmployeeTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveEmployeeTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve Employee Data From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    @Override
    protected void onPostExecute(String file_url) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveEmployeeTaskAsync :: onPostExecute >> " + SentResponse);
        delegate.PostSentAction(file_url);
        Toast.makeText(context,"Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;
        //String response = null;
        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Employee";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,3000);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            // flush table employee
            dbHelper.flushTable("GGGG_DOMAE_EMPLOYEE");

            SoapObject obj, obj1, obj2, obj3;
            obj     = (SoapObject) envelope.getResponse();
            obj1    = (SoapObject) obj.getProperty("diffgram");
            obj2    = (SoapObject) obj1.getProperty("NewDataSet");

            for (int i = 0; i < obj2.getPropertyCount();i++){
                obj3 = (SoapObject) obj2.getProperty(i);

                dbHelper.addEmployee(
                        obj3.getProperty(0).toString(),
                        obj3.getProperty(1).toString(),
                        obj3.getProperty(2).toString(),
                        obj3.getProperty(3).toString(),
                        obj3.getProperty(4).toString());

                Log.d("[GudangGaram]", "PERSON_ID        :" + obj3.getProperty(0).toString());
                Log.d("[GudangGaram]", "FULL_NAME        :" + obj3.getProperty(1).toString());
                Log.d("[GudangGaram]", "EMPLOYEE_NUMBER  :" + obj3.getProperty(2).toString());
                Log.d("[GudangGaram]", "EMAIL_ADDRESS    :" + obj3.getProperty(3).toString());
                Log.d("[GudangGaram]", "ATTRIBUTE        :" + obj3.getProperty(4).toString());

            }
        }
        catch (Exception ex) {
            SentResponse = ex.getMessage().toString();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
            //response = "<?xml version='1.0' encoding='utf-8'?><Message response='FAIL' ErrorMessage='The System is under maintenance or having some techical issue.'></Message>";
        }
        Log.d("[GudangGaram]", "SoapRetrieveLookupTaskAsync  :: end doInBackground");

        return "";
    }
}
