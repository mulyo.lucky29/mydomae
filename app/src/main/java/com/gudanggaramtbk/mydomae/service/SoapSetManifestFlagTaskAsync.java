package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 11/10/2017.
 */

public class SoapSetManifestFlagTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private ListView listBinPersiapan;

    private String xp_no_kendaraan;
    private String xp_rak_persiapan;
    private String xp_bin_persiapan;
    private String xp_no_trx;

    private Spinner OXRakPersiapan;
    private SoapRetrieveRakPersiapan    populateRakPersiapan;

    public interface SoapSetManifestFlagTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSetManifestFlagTaskAsync.SoapSetManifestFlagTaskAsyncResponse delegate = null;
    public SoapSetManifestFlagTaskAsync(SoapSetManifestFlagTaskAsync.SoapSetManifestFlagTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context            context,
                             MySQLiteHelper     dbHelper,
                             SharedPreferences  config,
                             String             p_no_kendaraan,
                             String             p_rak_persiapan,
                             String             p_bin_persiapan,
                             String             p_no_trx,
                             Spinner            ORakPersiapan,
                             ListView           lst){
        Log.d("[GudangGaram]", "SoapSetManifestFlagTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.pd = new ProgressDialog(this.context);

        this.xp_bin_persiapan = p_bin_persiapan;
        this.xp_rak_persiapan = p_rak_persiapan;
        this.xp_no_kendaraan  = p_no_kendaraan;
        this.xp_no_trx        = p_no_trx;
        this.OXRakPersiapan   = ORakPersiapan;
        this.listBinPersiapan = lst;

        // WS init
        populateRakPersiapan = new SoapRetrieveRakPersiapan(config);
        populateRakPersiapan.setContext(context);
        populateRakPersiapan.setDBHelper(dbHelper);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]:", "SoapSetManifestFlagTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait, Flagging On Oracle In Progress ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Toast.makeText(context,"Flagging " + output + " Done", Toast.LENGTH_LONG).show();
        // refresh combobox Rak Persiapan Di TProcessLoading setelah selesai di flag
        if(dbHelper.isNetworkConnected(config, context)){
            try{
                populateRakPersiapan.Retrieve(listBinPersiapan, OXRakPersiapan);
                Log.d("[GudangGaram]:", "SoapSetManifestFlagTaskAsync :: populateRakPersiapan.Retrieve");
            }
            catch(Exception e){
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String NAMESPACE        = config.getString("SoapNamespace", "");
        String SOAP_ADDRESS     = config.getString("SoapAddress", "");
        String OPERATION_NAME   = "Request_Flag_list_loading";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]:", "SOAP REQUEST   :" + request.toString());

        // =============== p_no_kendaraan ====================
        PropertyInfo prop_p_no_kendaraan = new PropertyInfo();
        prop_p_no_kendaraan.setName("p_no_kendaraan");
        prop_p_no_kendaraan.setValue(xp_no_kendaraan);
        prop_p_no_kendaraan.setType(String.class);
        request.addProperty(prop_p_no_kendaraan);
        // =============== p_rak_persiapan ========================
        PropertyInfo prop_p_rak_persiapan = new PropertyInfo();
        prop_p_rak_persiapan.setName("p_rak_persiapan");
        prop_p_rak_persiapan.setValue(xp_rak_persiapan);
        prop_p_rak_persiapan.setType(String.class);
        request.addProperty(prop_p_rak_persiapan);
        // =============== p_bin_persiapan ========================
        PropertyInfo prop_p_bin_persiapan = new PropertyInfo();
        prop_p_bin_persiapan.setName("p_bin_persiapan");
        prop_p_bin_persiapan.setValue(xp_bin_persiapan);
        prop_p_bin_persiapan.setType(String.class);
        request.addProperty(prop_p_bin_persiapan);
        // =============== p_no_trx ========================
        PropertyInfo prop_p_no_trx = new PropertyInfo();
        prop_p_no_trx.setName("p_no_trx");
        prop_p_no_trx.setValue(xp_no_trx);
        prop_p_no_trx.setType(String.class);
        request.addProperty(prop_p_no_trx);


        Log.d("[GudangGaram]:", "p_no_kendaraan     : " + xp_no_kendaraan);
        Log.d("[GudangGaram]:", "p_rak_persiapan    : " + xp_rak_persiapan);
        Log.d("[GudangGaram]:", "p_rak_persiapan    : " + xp_bin_persiapan);
        Log.d("[GudangGaram]:", "p_no_trx           : " + xp_no_trx);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]:", "Catch SoapSetManifestFlagTaskAsync : " + ex.getMessage().toString());
        }
        Log.d("[GudangGaram]:", "SoapSetManifestFlagTaskAsync  :: end doInBackground");

        return "Rak Persiapan : " + xp_rak_persiapan + " Bin Persiapan : " + xp_bin_persiapan;
    }
}
