package com.gudanggaramtbk.mydomae.service;

/**
 * Created by LuckyM on 11/10/2017.
 */

public interface SoapSetManifestFlagTaskAsyncResponse {
    void PostSentAction(String output);
}
