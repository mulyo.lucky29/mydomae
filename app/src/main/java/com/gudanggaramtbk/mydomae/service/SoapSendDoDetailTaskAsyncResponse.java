package com.gudanggaramtbk.mydomae.service;

/**
 * Created by LuckyM on 10/20/2017.
 */

public interface SoapSendDoDetailTaskAsyncResponse {
    void PostSentAction(String output);
}
