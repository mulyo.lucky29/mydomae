package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/10/2017.
 */

public class SoapRetrieveRakPersiapanTaskAsync extends  AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private Spinner RakPersiapan;
    private ListView listBinPersiapan;
    private List<String> Resultrp;
    private ArrayAdapter<String> adapter;

    public SoapRetrieveRakPersiapanTaskAsync(){
    }

    public interface SoapRetrieveRakPersiapanTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveRakPersiapanTaskAsync.SoapRetrieveRakPersiapanTaskAsyncResponse delegate = null;
    public SoapRetrieveRakPersiapanTaskAsync(SoapRetrieveRakPersiapanTaskAsync.SoapRetrieveRakPersiapanTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             Spinner cbo_rakPersiapan,
                             ListView lst){

        Log.d("[GudangGaram]", "SoapRetrieveRakPersiapanTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.pd                = new ProgressDialog(this.context);
        Resultrp               = new ArrayList<String>();
        this.RakPersiapan      = cbo_rakPersiapan;
        this.listBinPersiapan  = lst;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveRakPersiapanTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Rak Persiapan Data From Oracle Inprogress ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    protected  void onProgressUpdate(String[] args) {
        //progressDialog.setMessage(args[0]);
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveRakPersiapanTaskAsync :: onPostExecute >> " + SentResponse);
        try{
            delegate.PostSentAction(output);
            initComboBox(RakPersiapan, Resultrp);
        }
        catch(Exception e){
        }
        Toast.makeText(context,"Retrieve Data Done", Toast.LENGTH_LONG).show();
    }


    private void initComboBox(Spinner spinner, List<String> lvi){
        try{
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, lvi);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = parent.getItemAtPosition(position).toString();
                    // clear list
                    adapter = null;
                    listBinPersiapan.setAdapter(adapter);
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
        catch(Exception e){
        }
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;
        String response = null;
        String rak_persiapan = "";

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_RakPersiapan";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                for (int i = 0; i < obj2.getPropertyCount();i++){
                    obj3 = (SoapObject) obj2.getProperty(i);
                    rak_persiapan = obj3.getProperty(0).toString();
                    Resultrp.add(rak_persiapan);
                    Log.d("[GudangGaram]", "RAK PERSIAPAN :" + obj3.getProperty(0).toString());
                }
            }
            catch(NullPointerException e){
                Toast.makeText(context, "Connection Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            //SentResponse = ex.getMessage().toString();
            //Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
            //Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            //response = "<?xml version='1.0' encoding='utf-8'?><Message response='FAIL' ErrorMessage='The System is under maintenance or having some techical issue.'></Message>";
        }
        Log.d("[GudangGaram]", "SoapRetrieveRakPersiapanTaskAsync  :: end doInBackground");

        return response;
    }
}
