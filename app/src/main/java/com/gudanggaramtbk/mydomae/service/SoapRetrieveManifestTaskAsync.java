package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 10/6/2017.
 */

public class SoapRetrieveManifestTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String ResultResponse;

    private String p_DOLoadingHeader;
    private String p_no_kendaraan;
    private String p_rak_persiapan;
    private String p_bin_persiapan;
    private String p_no_trx;
    private String p_loadedBy;
    private String p_loadedByID;

    private SoapRetrieveRakPersiapan    populateRakPersiapan;

    public SoapRetrieveManifestTaskAsync(){}
    public interface SoapRetrieveManifestTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveManifestTaskAsync.SoapRetrieveManifestTaskAsyncResponse delegate = null;
    public SoapRetrieveManifestTaskAsync(SoapRetrieveManifestTaskAsync.SoapRetrieveManifestTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String DOLoadingHeader,
                             String NoKendaraan,
                             String RakPersiapan,
                             String BinPersiapan,
                             String NoTrx,
                             String LoadedBy,
                             String LoadedByID){

        Log.d("[GudangGaram]", "SoapRetrieveManifestTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.pd = new ProgressDialog(this.context);
        this.p_bin_persiapan   = BinPersiapan;
        this.p_rak_persiapan   = RakPersiapan;
        this.p_no_trx          = NoTrx;
        this.p_no_kendaraan    = NoKendaraan;
        this.p_DOLoadingHeader = DOLoadingHeader;
        this.p_loadedBy        = LoadedBy;
        this.p_loadedByID      = LoadedByID;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveManifestTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve Manifest Data Rak Persiapan = " + p_rak_persiapan + " Bin Persiapan = " + p_bin_persiapan + " No Trx = " + p_no_trx + " From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveManifestTaskAsync :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                Toast.makeText(context,"Retrieve Manifest Data Done", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(context,"Retrieve Manifest Data " + output + " Done", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Long Result;

        String NAMESPACE         = config.getString("SoapNamespace", "");
        String SOAP_ADDRESS      = config.getString("SoapAddress", "");
        // String OPERATION_NAME_MD = "Get_List_Manifest";
        String OPERATION_NAME_MD = "Get_List_Manifest_By_Trx";

        String SOAP_ACTION_MD = NAMESPACE + OPERATION_NAME_MD;

        SoapObject requestMD = new SoapObject(NAMESPACE, OPERATION_NAME_MD);
        Log.d("[GudangGaram]:", "SOAP REQUEST   :" + requestMD.toString());

        // =============== p_no_kendaraan ===================
        PropertyInfo prop_p_MDNoKendaraan = new PropertyInfo();
        prop_p_MDNoKendaraan.setName("p_no_kendaraan");
        prop_p_MDNoKendaraan.setValue(this.p_no_kendaraan);
        prop_p_MDNoKendaraan.setType(String.class);
        requestMD.addProperty(prop_p_MDNoKendaraan);

        // =============== p_rak_persiapan ===================
        PropertyInfo prop_p_MDrakPersiapan = new PropertyInfo();
        prop_p_MDrakPersiapan.setName("p_rak_persiapan");
        prop_p_MDrakPersiapan.setValue(this.p_rak_persiapan);
        prop_p_MDrakPersiapan.setType(String.class);
        requestMD.addProperty(prop_p_MDrakPersiapan);

        // =============== p_bin_persiapan ===================
        PropertyInfo prop_p_MDbinPersiapan = new PropertyInfo();
        prop_p_MDbinPersiapan.setName("p_bin_persiapan");
        prop_p_MDbinPersiapan.setValue(this.p_bin_persiapan);
        prop_p_MDbinPersiapan.setType(String.class);
        requestMD.addProperty(prop_p_MDbinPersiapan);

        // =============== p_rak_persiapan ===================
        PropertyInfo prop_p_MDnoTrx = new PropertyInfo();
        prop_p_MDnoTrx.setName("p_no_trx");
        prop_p_MDnoTrx.setValue(this.p_no_trx);
        prop_p_MDnoTrx.setType(String.class);
        requestMD.addProperty(prop_p_MDnoTrx);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(requestMD);

        //Web method call MD
        try {
            try{
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION_MD, envelope);

                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                for (int i = 0; i < obj2.getPropertyCount();i++){
                    obj3 = (SoapObject) obj2.getProperty(i);


                    Log.d("[GudangGaram]:", ">>> ADD MANIFEST DETAIL >>>");
                    Log.d("[GudangGaram]:", "RAK_PERSIAPAN  :" + obj3.getProperty(0).toString());
                    Log.d("[GudangGaram]:", "BIN_PERSIAPAN  :" + obj3.getProperty(1).toString());
                    Log.d("[GudangGaram]:", "REQUESTOR      :" + obj3.getProperty(2).toString());
                    Log.d("[GudangGaram]:", "LOCATION       :" + obj3.getProperty(3).toString());
                    Log.d("[GudangGaram]:", "TRX_NUMBER     :" + obj3.getProperty(4).toString());
                    Log.d("[GudangGaram]:", "ORG_ID         :" + obj3.getProperty(5).toString());
                    Log.d("[GudangGaram]:", "ITEM_ID        :" + obj3.getProperty(6).toString());
                    Log.d("[GudangGaram]:", "ITEM_CODE      :" + obj3.getProperty(7).toString());
                    Log.d("[GudangGaram]:", "ITEM_DESC      :" + obj3.getProperty(8).toString());
                    Log.d("[GudangGaram]:", "QTY            :" + obj3.getProperty(9).toString());
                    Log.d("[GudangGaram]:", "UOM            :" + obj3.getProperty(10).toString());


                    // add manifest to database local
                    Result = dbHelper.addManifestDetail(
                            p_no_kendaraan,
                            obj3.getProperty(0).toString(),
                            obj3.getProperty(1).toString(),
                            obj3.getProperty(2).toString(),
                            obj3.getProperty(3).toString(),
                            obj3.getProperty(4).toString(),
                            Integer.parseInt(obj3.getProperty(5).toString()),
                            Integer.parseInt(obj3.getProperty(6).toString()),
                            obj3.getProperty(7).toString(),
                            obj3.getProperty(8).toString(),
                            Float.parseFloat(obj3.getProperty(9).toString()),
                            obj3.getProperty(10).toString()
                    );

                    ResultResponse = Result.toString();
                    // edit flag to database local
                    dbHelper.UpdateLoadingManifest(p_DOLoadingHeader, obj3.getProperty(0).toString(), obj3.getProperty(1).toString(), p_no_kendaraan, p_loadedBy, p_loadedByID);
                }

            }
            catch(NullPointerException e){
            }
        }
        catch (Exception ex) {
            ResultResponse = "*";
            Log.d("[GudangGaram]:", "Catch : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]:", "SoapRetrieveManifestTaskAsync  :: end doInBackground");
        }

        return ResultResponse;
    }
}
