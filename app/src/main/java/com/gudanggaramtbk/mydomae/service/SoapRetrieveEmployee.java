package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.activity.MainActivity;
import com.gudanggaramtbk.mydomae.activity.SyncAction;

/**
 * Created by LuckyM on 10/6/2017.
 */

public class SoapRetrieveEmployee {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    //private MainActivity pActivity;
    private SyncAction pActivity;

    // override constructor
    public SoapRetrieveEmployee(SharedPreferences PConfig, Context context, MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]: ", "SoapRetrieveEmployee Constructor");
        this.config = PConfig;
        this.context = context;
        this.dbHelper = dbHelper;
    }
    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }

    public void Retrieve() {
        Cursor XCur;
        MainActivity mx = new MainActivity();

        Log.d("[GudangGaram]: ", "Retrieve Employee Begin");
        try {
            SoapRetrieveEmployeeTaskAsync SoapRequest = new SoapRetrieveEmployeeTaskAsync(new SoapRetrieveEmployeeTaskAsync.SoapRetrieveEmployeeTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                    Log.d("[GudangGaram]: ", "PostSentAction Output : " + output);
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config);
            /*
            if (Build.VERSION.SDK_INT >= 11){
                SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR); //work on sgs3 android 4.0.4
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
            */
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }

            //XCur.moveToNext();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {

        }
    }

}
