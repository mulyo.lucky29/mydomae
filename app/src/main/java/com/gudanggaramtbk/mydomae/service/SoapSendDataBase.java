package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.activity.SyncAction;

/**
 * Created by LuckyM on 5/4/2018.
 */

public class SoapSendDataBase extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;
    private SyncAction pActivity;
    private MySQLiteHelper dbHelper;

    // override constructor
    public SoapSendDataBase(SharedPreferences PConfig, Context context, MySQLiteHelper dbHelper) {
        Log.d("[GudangGaram]: ", "SoapSendDataBase Constructor");
        this.config = PConfig;
        this.context = context;
        this.dbHelper = dbHelper;
    }
    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }

    public void Send(String device_id){
        try {
            // call WS To send Database
            SoapSendDataBaseTaskAsync SoapRequest = new SoapSendDataBaseTaskAsync(new SoapSendDataBaseTaskAsync.SoapSendDataBaseTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, device_id);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                Log.d("[GudangGaram]: ", "Using HC Higer");
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]: ", "Using HC Lower");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]: ", "Send Database Exception >> " + e.getMessage().toString());
        }
    }

}
