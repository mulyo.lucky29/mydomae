package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.BuildConfig;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by LuckyM on 5/4/2018.
 */

public class SoapSendDataBaseTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private Integer LPosition;
    private String deviceid;

    public SoapSendDataBaseTaskAsync(){
    }

    public interface SoapSendDataBaseTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSendDataBaseTaskAsync.SoapSendDataBaseTaskAsyncResponse delegate = null;

    public SoapSendDataBaseTaskAsync(SoapSendDataBaseTaskAsync.SoapSendDataBaseTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String deviceid){

        Log.d("[GudangGaram]", "SoapSendDataBaseTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.deviceid       = deviceid;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendDataBaseTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Process Initialized Please Wait  ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendDataBaseTaskAsync :: onPostExecute >> " + SentResponse);
        delegate.PostSentAction(output);
        Toast.makeText(context,"Process Initialized Complete", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;
        String dbName;

        String NAMESPACE        = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS     = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME   = "Backup_DataBase";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        dbName = dbHelper.Get_DatabaseName();
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        try{
            if (sd.canWrite()) {
                String currentDBPath = "//data//" + BuildConfig.APPLICATION_ID + "//databases//" + dbName;
                File currentDB = new File(data, currentDBPath);
                if (currentDB.exists()) {
                    byte[] bytesArray = new byte[(int) currentDB.length()];
                    FileInputStream fis = new FileInputStream(currentDB);
                    fis.read(bytesArray); //read file into bytes[]
                    fis.close();

                    String  xp_signature            = Base64.encodeToString(bytesArray, Base64.DEFAULT);
                    String  xp_device_id            = deviceid;
                    // =============== p_device_id ====================
                    PropertyInfo prop_p_device_id = new PropertyInfo();
                    prop_p_device_id.setName("p_device_id");
                    prop_p_device_id.setValue(xp_device_id);
                    prop_p_device_id.setType(String.class);
                    request.addProperty(prop_p_device_id);

                    // =============== p_dbcontent ===================
                    PropertyInfo prop_p_dbcontent = new PropertyInfo();
                    prop_p_dbcontent.setName("p_dbcontent");
                    prop_p_dbcontent.setValue(xp_signature);
                    prop_p_dbcontent.setType(String.class);
                    request.addProperty(prop_p_dbcontent);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    //Web method call
                    try {
                        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                        httpTransport.debug = true;
                        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                        httpTransport.call(SOAP_ACTION, envelope);
                    }
                    catch (Exception ex) {
                        Log.d("[GudangGaram]", "SoapSendDataBaseTaskAsync Catch : " + ex.getMessage().toString());
                    }

                }
            }
        }
        catch(Exception e){
        }





        Log.d("[GudangGaram]:", "SoapSendDataBaseTaskAsync  :: end doInBackground");

        return SentResponse;
    }
}
