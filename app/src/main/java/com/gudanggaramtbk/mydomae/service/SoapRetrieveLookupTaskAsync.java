package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 10/5/2017.
 */

public class SoapRetrieveLookupTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;

    public SoapRetrieveLookupTaskAsync(){
    }

    public interface SoapRetrieveLookupTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveLookupTaskAsync.SoapRetrieveLookupTaskAsyncResponse delegate = null;
    public SoapRetrieveLookupTaskAsync(SoapRetrieveLookupTaskAsync.SoapRetrieveLookupTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config){

        Log.d("[GudangGaram]", "SoapRetrieveLookupTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveLookupTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve Lookup Data From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    protected  void onProgressUpdate(String[] args) {
        //progressDialog.setMessage(args[0]);
    }

    @Override
    protected void onPostExecute(String file_url) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveLookupTaskAsync :: onPostExecute >> " + SentResponse);
        delegate.PostSentAction(file_url);
        Toast.makeText(context,"Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;
        //String response = null;
        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Lookup";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
        Log.d("[GudangGaram]", "SOAP_ADDRESS   :" + SOAP_ADDRESS);
        Log.d("[GudangGaram]", "SOAP_ACTION    :" + SOAP_ACTION);

        //SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        SoapObject request = new SoapObject(NAMESPACE,OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject obj, obj1, obj2, obj3;
            obj     = (SoapObject) envelope.getResponse();
            obj1    = (SoapObject) obj.getProperty("diffgram");
            obj2    = (SoapObject) obj1.getProperty("NewDataSet");

            dbHelper.flushTable("GGGG_DOMAE_LOOKUP");

            for (int i = 0; i < obj2.getPropertyCount();i++){
                obj3 = (SoapObject) obj2.getProperty(i);

                dbHelper.addLookup(
                        obj3.getProperty(0).toString(),
                        obj3.getProperty(1).toString(),
                        obj3.getProperty(2).toString());

                Log.d("[GudangGaram]", "ML_CONTEXT      :" + obj3.getProperty(0).toString());
                Log.d("[GudangGaram]", "ML_VALUE        :" + obj3.getProperty(1).toString());
                Log.d("[GudangGaram]", "ML_DESCRIPTION  :" + obj3.getProperty(2).toString());

            }
        }
        catch (Exception ex) {
            SentResponse = ex.getMessage().toString();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
            //response = "<?xml version='1.0' encoding='utf-8'?><Message response='FAIL' ErrorMessage='The System is under maintenance or having some techical issue.'></Message>";
        }
        Log.d("[GudangGaram]", "SoapRetrieveLookupTaskAsync  :: end doInBackground");

        return "";
    }
}
