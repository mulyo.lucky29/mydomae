package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.adapter.CustomListAdapterNoTrx;
import com.gudanggaramtbk.mydomae.holder.HolderNoTrx;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/8/2017.
 */

public class SoapRetrieveBinPersiapanTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;

    //private List<String> Result;
    private List<HolderNoTrx> Result;
    private ArrayAdapter<HolderNoTrx>           trxadapter;
    private List<HolderNoTrx>                   lvitrx;

    private String Rak_Persiapan;
    private ListView listBinPersiapan;

    public SoapRetrieveBinPersiapanTaskAsync(){}

    public interface SoapRetrieveBinPersiapanTaskAsyncResponse {
        void PostSentAction(List<HolderNoTrx> output);
        //void PostSentAction(List<String> output);
        //void PostSentAction(String output);
    }

    public SoapRetrieveBinPersiapanTaskAsync.SoapRetrieveBinPersiapanTaskAsyncResponse delegate = null;
    public SoapRetrieveBinPersiapanTaskAsync(SoapRetrieveBinPersiapanTaskAsync.SoapRetrieveBinPersiapanTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String p_rak_persiapan,
                             ListView olistBinPersiapan){

        Log.d("[GudangGaram]", "SoapRetrieveBinPersiapanTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.Rak_Persiapan  = p_rak_persiapan;
        this.pd = new ProgressDialog(this.context);
        this.listBinPersiapan = olistBinPersiapan;

        //Result = new ArrayList<String>();
        Result = new ArrayList<HolderNoTrx>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveBinPersiapanTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve Data Bin Persiapan From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    protected  void onProgressUpdate(String[] args) {
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        try {
            // populate list bin persiapan
            /*
            listBinPersiapan.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            String[] values = Result.toArray(new String[0]);
            ArrayAdapter<String>  adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_multiple_choice, android.R.id.text1, values);
            listBinPersiapan.setAdapter(adapter);
            */

            listBinPersiapan.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            trxadapter = new CustomListAdapterNoTrx(context, Result, listBinPersiapan);
            trxadapter.notifyDataSetChanged();
            listBinPersiapan.setAdapter(trxadapter);
            long xcount = listBinPersiapan.getCount();
            if (xcount == 0) {
                Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
            }
            delegate.PostSentAction(Result);
        }
        catch(Exception e){
        }
        Toast.makeText(context,"Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        String bin_persiapan = "";
        String no_trx = "";

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Get_List_No_Trx";
        // changed v.1.4 to  Get_List_No_Trx
        //String OPERATION_NAME = "Get_List_BinPersiapan";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_rak_persiapan ===================
        PropertyInfo prop_p_rakPersiapan = new PropertyInfo();
        prop_p_rakPersiapan.setName("p_rak_persiapan");
        prop_p_rakPersiapan.setValue(this.Rak_Persiapan);
        prop_p_rakPersiapan.setType(String.class);
        request.addProperty(prop_p_rakPersiapan);

        Log.d("[GudangGaram]: ", "p_rak_persiapan      : " + this.Rak_Persiapan);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);
            SoapObject obj, obj1, obj2, obj3;
            obj     = (SoapObject) envelope.getResponse();
            obj1    = (SoapObject) obj.getProperty("diffgram");
            obj2    = (SoapObject) obj1.getProperty("NewDataSet");

            Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));
            Result.clear();
            for (int i = 0; i < obj2.getPropertyCount();i++){
                obj3 = (SoapObject) obj2.getProperty(i);

                HolderNoTrx vh = new HolderNoTrx();
                no_trx        = obj3.getProperty(0).toString();
                bin_persiapan = obj3.getProperty(1).toString();

                vh.setNotrx(no_trx);
                vh.setBinpersiapan(bin_persiapan);

                Log.d("[GudangGaram]", "NO TRX  - BIN PERSIAPAN  : " + no_trx + " - " + bin_persiapan);
                //Result.add(bin_persiapan);
                Result.add(vh);
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveBinPersiapanTaskAsync  :: end doInBackground");
        }

        return "";
    }

}
