package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;
import android.widget.Spinner;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.activity.SyncAction;

/**
 * Created by LuckyM on 10/6/2017.
 */

public class SoapRetrieveManifest {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper dbHelper;
    private SyncAction pActivity;
    private ListView          listBinPersiapan;

    // override constructor
    public SoapRetrieveManifest(SharedPreferences PConfig, ListView lst){
        Log.d("[GudangGaram]:", "SoapRetrieveManifest Constructor");
        this.config           = PConfig;
        this.listBinPersiapan = lst;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]:", "SetContext");
        this.context = context;
    }

    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]:", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Retrieve(Spinner OCbo_PL_RakPersiapan,
                         String  DOLoadingHeader,
                         String  NoKendaraan,
                         String  RakPersiapan,
                         String  BinPersiapan,
                         String  NoTrx,
                         String  LoadedBy,
                         String  LoadedByID) {

        final Spinner OXRakPersiapan;
        final String tNoKendaraan;
        final String tRakPersiapan;
        final String tBinPersiapan;
        final String tLoadedBy;
        final String tLoadedByID;
        final String tNoTrx;


        tNoKendaraan     = NoKendaraan;
        tRakPersiapan    = RakPersiapan;
        tBinPersiapan    = BinPersiapan;
        tLoadedBy        = LoadedBy;
        tLoadedByID      = LoadedByID;
        tNoTrx           = NoTrx;
        OXRakPersiapan   = OCbo_PL_RakPersiapan;


        Log.d("[GudangGaram]:", "Retrieve Manifest Begin");
        try {
            // call WS To Retrieve Manifest from oracle and save it to local database table
            SoapRetrieveManifestTaskAsync SoapRequest = new SoapRetrieveManifestTaskAsync(new SoapRetrieveManifestTaskAsync.SoapRetrieveManifestTaskAsyncResponse() {
                @Override
                public void PostSentAction(String outputx) {
                    try {
                        Log.d("[GudangGaram]:", "SendRequestUpdateFlag Start");
                        if(outputx.equals("*") == true){
                        }
                        else {
                            Log.d("[GudangGaram]:", "Update Flag Manifest With ID = " + outputx);
                            // call WS to flag manifest in oracle
                            SoapSetManifestFlagTaskAsync SoapRequest = new SoapSetManifestFlagTaskAsync(new SoapSetManifestFlagTaskAsync.SoapSetManifestFlagTaskAsyncResponse() {
                                @Override
                                public void PostSentAction(String output) {
                                    Log.d("[GudangGaram]:", "Update Flag To Oracle Status : " + output);
                                }
                            });
                            SoapRequest.setAttribute(context, dbHelper, config, tNoKendaraan, tRakPersiapan, tBinPersiapan,tNoTrx, OXRakPersiapan, listBinPersiapan);
                            //SoapRequest.execute();
                            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                //work on sgs3 android 4.0.4
                                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                            }
                            else {
                                SoapRequest.execute(); // work on sgs2 android 2.3
                            }
                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]:", "SendRequestUpdateFlag Exception " + e.getMessage().toString());
                    }
                    finally {
                        Log.d("[GudangGaram]:", "SendRequestUpdateFlag End");
                    }
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config,DOLoadingHeader, NoKendaraan, RakPersiapan,BinPersiapan,tNoTrx, tLoadedBy,tLoadedByID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
            //SoapRequest.execute();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "Retrieve Manifest End");
        }
    }
}
