package com.gudanggaramtbk.mydomae.service;

/**
 * Created by LuckyM on 5/4/2018.
 */

public interface SoapSendDataBaseTaskAsyncResponse {
    void PostSentAction(String output);
}
