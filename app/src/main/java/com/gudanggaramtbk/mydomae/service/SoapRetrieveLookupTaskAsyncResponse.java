package com.gudanggaramtbk.mydomae.service;

/**
 * Created by LuckyM on 10/5/2017.
 */

public interface SoapRetrieveLookupTaskAsyncResponse {
    void PostSentAction(String output);
}
