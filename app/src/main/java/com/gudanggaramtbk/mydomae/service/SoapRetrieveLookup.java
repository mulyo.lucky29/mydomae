package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.activity.SyncAction;

/**
 * Created by LuckyM on 10/5/2017.
 */

public class SoapRetrieveLookup {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private SyncAction pActivity;

    // override constructor
    public SoapRetrieveLookup(SharedPreferences PConfig, Context context, MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]: ", "SoapRetrieveLookup Constructor");
        this.config = PConfig;
        this.context = context;
        this.dbHelper = dbHelper;

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            try {
                Class.forName("android.os.AsyncTask");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }

    public void Retrieve() {
        SyncAction mx = new SyncAction();

        Log.d("[GudangGaram]: ", "Retrieve Lookup Begin");
        try {
            SoapRetrieveLookupTaskAsync SoapRequest = new SoapRetrieveLookupTaskAsync(new SoapRetrieveLookupTaskAsync.SoapRetrieveLookupTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                    Log.d("[GudangGaram]: ", "PostSentAction Output : " + output);
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
            //SoapRequest.execute();
            /*
            if (Build.VERSION.SDK_INT >= 11){
                SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR); //work on sgs3 android 4.0.4
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
            */

        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
    }

}
