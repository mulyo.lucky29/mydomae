package com.gudanggaramtbk.mydomae.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.model.CSendInfo;
import com.gudanggaramtbk.mydomae.model.ManifestDetail;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by LuckyM on 10/20/2017.
 */
        /*
           PROCEDURE INSERT_DO_DETAIL(
                p_device_id         IN VARCHAR2,
                p_header_ora        IN NUMBER,
                p_doh_id            IN NUMBER,
                p_dod_id            IN NUMBER,
                p_vehicle_no        IN VARCHAR2,
                p_requestor         IN VARCHAR2,
                p_location          IN VARCHAR2,
                p_trx_no            IN VARCHAR2,
                p_received_by_id    IN VARCHAR2,
                p_received_by       IN VARCHAR2,
                p_rak_persiapan     IN VARCHAR2,
                p_bin_persiapan     IN VARCHAR2,
                p_org_id            IN NUMBER,
                p_item_id           IN NUMBER,
                p_item_code         IN VARCHAR2,
                p_item_desc         IN VARCHAR2,
                p_quantity          IN NUMBER,
                p_quantity_rcv      IN NUMBER,
                p_uom               IN VARCHAR2,
                p_reject_reason     IN VARCHAR2,
                p_received_flag     IN VARCHAR2,
                p_loading_date      IN VARCHAR2,
                p_send_date         IN VARCHAR2,
                p_created_date      IN VARCHAR2,
                p_real_rcv_by       IN VARCHAR2,
                p_real_rcv_by_nik   IN VARCHAR2,
                p_loaded_by_id      IN VARCHAR2,
                p_loaded_by         IN VARCHAR2,
                p_do_detail_id  OUT NUMBER
        */


public class SoapSendDoDetailTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private  String xpd_do_line_id = "";
    private String SentResponse = " ";
    private List<CSendInfo> objlistheader;

    public interface SoapSendDoDetailTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSendDoDetailTaskAsync.SoapSendDoDetailTaskAsyncResponse delegate = null;
    public SoapSendDoDetailTaskAsync(SoapSendDoDetailTaskAsync.SoapSendDoDetailTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             List<CSendInfo> listheader){
        Log.d("[GudangGaram]", "SoapSendDoDetailTaskAsync :: setAttribute");
        this.context             = context;
        this.dbHelper            = dbHelper;
        this.config              = config;
        objlistheader            = listheader;
        this.pd                  = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendDoDetailTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Send Do Detail To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    @Override
    protected  void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            //pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendDoDetailTaskAsync :: onPostExecute >> " + SentResponse);
        delegate.PostSentAction(output);
        Toast.makeText(context,"Send Do Detail Done", Toast.LENGTH_LONG).show();
    }

    public String replace_format_indo(String str){
        String result;
        result = str.replace("Mei","May").replace("Agt","Aug").replace("Okt","Oct").replace("Des","Dec");
        return result;
    }

    @Override
    protected String doInBackground(String... params) {
        if(objlistheader.size() > 0) {
            // loop through header mapping
            for (CSendInfo hdr : objlistheader) {
                List<ManifestDetail> LDOD = new ArrayList<ManifestDetail>();
                LDOD = dbHelper.get_DODetail(hdr.getDo_header_id());
                Log.d("[GudangGaram]: ", "SentDODetail (" + hdr.getDo_header_id() + ") ToOracle COUNT " + LDOD.size());
                if (LDOD.size() > 0) {
                    for (int idx = 0; idx < LDOD.size(); idx++) {
                        // -------- call web service ---------------
                        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                        String SOAP_ADDRESS = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
                        String OPERATION_NAME = "Insert_DODetail";
                        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

                        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                        String xp_device_id = Settings.Secure.getString(this.context.getContentResolver(), Settings.Secure.ANDROID_ID);
                        Integer xp_header_ora = Integer.parseInt(hdr.getDo_oracle_header_id());
                        Integer xp_doh_id = Integer.parseInt(hdr.getDo_header_id());
                        Integer xp_dod_id = Integer.parseInt(LDOD.get(idx).getMD_ID().toString());
                        String xp_vehicle_no = LDOD.get(idx).getMD_VEHICLE_NO();
                        String xp_requestor = LDOD.get(idx).getMD_REQUESTOR();
                        String xp_location = LDOD.get(idx).getMD_LOCATION();
                        String xp_trx_no = LDOD.get(idx).getMD_TRX_NUMBER();
                        String xp_received_by_id = LDOD.get(idx).getMD_RECEIVED_BY_ID();
                        String xp_received_by = LDOD.get(idx).getMD_RECEIVED_BY();
                        String xp_rak_persiapan = LDOD.get(idx).getMD_RAK_PERSIAPAN();
                        String xp_bin_persiapan = LDOD.get(idx).getMD_BIN_PERSIAPAN();
                        Integer xp_org_id = LDOD.get(idx).getMD_ORG_ID();
                        Integer xp_item_id = LDOD.get(idx).getMD_ITEM_ID();
                        String xp_item_code = LDOD.get(idx).getMD_ITEM_CODE();
                        String xp_item_desc = LDOD.get(idx).getMD_ITEM_DESC();
                        String xp_quantity = LDOD.get(idx).getMD_QTY();
                        String xp_quantity_rcv = LDOD.get(idx).getMD_QTY_RCV();
                        String xp_uom = LDOD.get(idx).getMD_UOM();
                        String xp_reject_reason = LDOD.get(idx).getMD_REASON_CODE();
                        String xp_loading_date = replace_format_indo(LDOD.get(idx).getMD_LOADING_DATE());
                        String xp_check_sent = LDOD.get(idx).getMD_CHECK_SENT();
                        String xp_send_date = replace_format_indo(LDOD.get(idx).getMD_SENT_DATE());
                        String xp_created_date = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));
                        String xp_real_received_by = LDOD.get(idx).getMD_REAL_RECEIVED_BY();
                        String xp_real_received_by_nik = LDOD.get(idx).getMD_REAL_RECEIVED_BY_NIK();
                        String xp_loaded_by_id = LDOD.get(idx).getMD_LOADED_BY_ID();
                        String xp_loaded_by = LDOD.get(idx).getMD_LOADED_BY();

                        Log.d("[GudangGaram]: ", "SOAP REQUEST >> Insert_DODetail");
                        // =============== p_device_id ====================
                        PropertyInfo prop_p_device_id = new PropertyInfo();
                        prop_p_device_id.setName("p_device_id");
                        prop_p_device_id.setValue(xp_device_id);
                        prop_p_device_id.setType(String.class);
                        request.addProperty(prop_p_device_id);
                        // =============== p_header_ora ========================
                        PropertyInfo prop_p_header_ora = new PropertyInfo();
                        prop_p_header_ora.setName("p_header_ora");
                        prop_p_header_ora.setValue(xp_header_ora);
                        prop_p_header_ora.setType(Number.class);
                        request.addProperty(prop_p_header_ora);
                        // =============== p_doh_id ========================
                        PropertyInfo prop_p_doh_id = new PropertyInfo();
                        prop_p_doh_id.setName("p_doh_id");
                        prop_p_doh_id.setValue(xp_doh_id);
                        prop_p_doh_id.setType(Number.class);
                        request.addProperty(prop_p_doh_id);
                        // =============== p_dod_id ========================
                        PropertyInfo prop_p_dod_id = new PropertyInfo();
                        prop_p_dod_id.setName("p_dod_id");
                        prop_p_dod_id.setValue(xp_dod_id);
                        prop_p_dod_id.setType(Number.class);
                        request.addProperty(prop_p_dod_id);
                        // =============== p_vehicle_no ===================
                        PropertyInfo prop_p_vehicle_no = new PropertyInfo();
                        prop_p_vehicle_no.setName("p_vehicle_no");
                        prop_p_vehicle_no.setValue(xp_vehicle_no);
                        prop_p_vehicle_no.setType(String.class);
                        request.addProperty(prop_p_vehicle_no);
                        // =============== p_requestor =====================
                        PropertyInfo prop_p_requestor = new PropertyInfo();
                        prop_p_requestor.setName("p_requestor");
                        prop_p_requestor.setValue(xp_requestor);
                        prop_p_requestor.setType(String.class);
                        request.addProperty(prop_p_requestor);
                        // =============== p_location =====================
                        PropertyInfo prop_p_location = new PropertyInfo();
                        prop_p_location.setName("p_location");
                        prop_p_location.setValue(xp_location);
                        prop_p_location.setType(String.class);
                        request.addProperty(prop_p_location);
                        // =============== p_trx_no ======================
                        PropertyInfo prop_p_trx_no = new PropertyInfo();
                        prop_p_trx_no.setName("p_trx_no");
                        prop_p_trx_no.setValue(xp_trx_no);
                        prop_p_trx_no.setType(String.class);
                        request.addProperty(prop_p_trx_no);
                        // =============== p_received_by_id ==================
                        PropertyInfo prop_p_received_by_id = new PropertyInfo();
                        prop_p_received_by_id.setName("p_received_by_id");
                        prop_p_received_by_id.setValue(xp_received_by_id);
                        prop_p_received_by_id.setType(String.class);
                        request.addProperty(prop_p_received_by_id);
                        // =============== p_received_by ==================
                        PropertyInfo prop_p_received_by = new PropertyInfo();
                        prop_p_received_by.setName("p_received_by");
                        prop_p_received_by.setValue(xp_received_by);
                        prop_p_received_by.setType(String.class);
                        request.addProperty(prop_p_received_by);
                        // =============== p_rak_persiapan ===================
                        PropertyInfo prop_p_rak_persiapan = new PropertyInfo();
                        prop_p_rak_persiapan.setName("p_rak_persiapan");
                        prop_p_rak_persiapan.setValue(xp_rak_persiapan);
                        prop_p_rak_persiapan.setType(String.class);
                        request.addProperty(prop_p_rak_persiapan);
                        // =============== p_bin_persiapan ===================
                        PropertyInfo prop_p_bin_persiapan = new PropertyInfo();
                        prop_p_bin_persiapan.setName("p_bin_persiapan");
                        prop_p_bin_persiapan.setValue(xp_bin_persiapan);
                        prop_p_bin_persiapan.setType(String.class);
                        request.addProperty(prop_p_bin_persiapan);
                        // =============== p_org_id ==================
                        PropertyInfo prop_p_org_id = new PropertyInfo();
                        prop_p_org_id.setName("p_org_id");
                        prop_p_org_id.setValue(xp_org_id);
                        prop_p_org_id.setType(Number.class);
                        request.addProperty(prop_p_org_id);
                        // =============== p_item_id ==================
                        PropertyInfo prop_p_item_id = new PropertyInfo();
                        prop_p_item_id.setName("p_item_id");
                        prop_p_item_id.setValue(xp_item_id);
                        prop_p_item_id.setType(Number.class);
                        request.addProperty(prop_p_item_id);
                        // =============== p_item_code ==================
                        PropertyInfo prop_p_item_code = new PropertyInfo();
                        prop_p_item_code.setName("p_item_code");
                        prop_p_item_code.setValue(xp_item_code);
                        prop_p_item_code.setType(String.class);
                        request.addProperty(prop_p_item_code);
                        // =============== p_item_desc ==================
                        PropertyInfo prop_p_item_desc = new PropertyInfo();
                        prop_p_item_desc.setName("p_item_desc");
                        prop_p_item_desc.setValue(xp_item_desc);
                        prop_p_item_desc.setType(String.class);
                        request.addProperty(prop_p_item_desc);
                        // =============== p_quantity ==================
                        PropertyInfo prop_p_qty = new PropertyInfo();
                        prop_p_qty.setName("p_quantity");
                        prop_p_qty.setValue(xp_quantity);
                        prop_p_qty.setType(String.class);
                        request.addProperty(prop_p_qty);
                        // =============== p_quantity_rcv ==================
                        PropertyInfo prop_p_qty_rcv = new PropertyInfo();
                        prop_p_qty_rcv.setName("p_quantity_rcv");
                        prop_p_qty_rcv.setValue(xp_quantity_rcv);
                        prop_p_qty_rcv.setType(String.class);
                        request.addProperty(prop_p_qty_rcv);
                        // =============== p_uom ==================
                        PropertyInfo prop_p_uom = new PropertyInfo();
                        prop_p_uom.setName("p_uom");
                        prop_p_uom.setValue(xp_uom);
                        prop_p_uom.setType(String.class);
                        request.addProperty(prop_p_uom);
                        // =============== p_reject_reason ==================
                        PropertyInfo prop_p_reject_reason = new PropertyInfo();
                        prop_p_reject_reason.setName("p_reject_reason");
                        prop_p_reject_reason.setValue(xp_reject_reason);
                        prop_p_reject_reason.setType(String.class);
                        request.addProperty(prop_p_reject_reason);

                        // =============== p_received_flag ==================
                        PropertyInfo prop_p_received_flag = new PropertyInfo();
                        prop_p_received_flag.setName("p_received_flag");
                        prop_p_received_flag.setValue(xp_check_sent);
                        prop_p_received_flag.setType(String.class);
                        request.addProperty(prop_p_received_flag);

                        // =============== p_loading_date ==================
                        PropertyInfo prop_p_loading_date = new PropertyInfo();
                        prop_p_loading_date.setName("p_loading_date");
                        prop_p_loading_date.setValue(xp_loading_date);
                        prop_p_loading_date.setType(String.class);
                        request.addProperty(prop_p_loading_date);
                        // =============== p_send_date ==================
                        PropertyInfo prop_p_send_date = new PropertyInfo();
                        prop_p_send_date.setName("p_send_date");
                        prop_p_send_date.setValue(xp_send_date);
                        prop_p_send_date.setType(String.class);
                        request.addProperty(prop_p_send_date);
                        // =============== p_created_date ==================
                        PropertyInfo prop_p_created_date = new PropertyInfo();
                        prop_p_created_date.setName("p_created_date");
                        prop_p_created_date.setValue(xp_created_date);
                        prop_p_created_date.setType(String.class);
                        request.addProperty(prop_p_created_date);

                        // =============== p_real_rcv_by ==================
                        PropertyInfo prop_p_real_rcv_by = new PropertyInfo();
                        prop_p_real_rcv_by.setName("p_real_rcv_by");
                        prop_p_real_rcv_by.setValue(xp_real_received_by);
                        prop_p_real_rcv_by.setType(String.class);
                        request.addProperty(prop_p_real_rcv_by);

                        // =============== p_real_rcv_by_nik ==================
                        PropertyInfo prop_p_real_rcv_by_nik = new PropertyInfo();
                        prop_p_real_rcv_by_nik.setName("p_real_rcv_by_nik");
                        prop_p_real_rcv_by_nik.setValue(xp_real_received_by_nik);
                        prop_p_real_rcv_by_nik.setType(String.class);
                        request.addProperty(prop_p_real_rcv_by_nik);

                        // =============== p_loaded_by_id ==================
                        PropertyInfo prop_p_loaded_by_id = new PropertyInfo();
                        prop_p_loaded_by_id.setName("p_loaded_by_id");
                        prop_p_loaded_by_id.setValue(xp_loaded_by_id);
                        prop_p_loaded_by_id.setType(String.class);
                        request.addProperty(prop_p_loaded_by_id);

                        // =============== p_loaded_by ==================
                        PropertyInfo prop_p_loaded_by = new PropertyInfo();
                        prop_p_loaded_by.setName("p_loaded_by");
                        prop_p_loaded_by.setValue(xp_loaded_by);
                        prop_p_loaded_by.setType(String.class);
                        request.addProperty(prop_p_loaded_by);


                        Log.d("[GudangGaram]: ", "p_device_id       : " + xp_device_id);
                        Log.d("[GudangGaram]: ", "p_header_ora      : " + Integer.toString(xp_header_ora));
                        Log.d("[GudangGaram]: ", "p_doh_id          : " + Integer.toString(xp_doh_id));
                        Log.d("[GudangGaram]: ", "p_dod_id          : " + Integer.toString(xp_dod_id));
                        Log.d("[GudangGaram]: ", "p_vehicle_no      : " + xp_vehicle_no);
                        Log.d("[GudangGaram]: ", "p_requestor       : " + xp_requestor);
                        Log.d("[GudangGaram]: ", "p_location        : " + xp_location);
                        Log.d("[GudangGaram]: ", "p_trx_no          : " + xp_trx_no);
                        Log.d("[GudangGaram]: ", "p_received_by_id  : " + xp_received_by_id);
                        Log.d("[GudangGaram]: ", "p_received_by     : " + xp_received_by);
                        Log.d("[GudangGaram]: ", "p_rak_persiapan   : " + xp_rak_persiapan);
                        Log.d("[GudangGaram]: ", "p_bin_persiapan   : " + xp_bin_persiapan);
                        Log.d("[GudangGaram]: ", "p_org_id          : " + Integer.toString(xp_org_id));
                        Log.d("[GudangGaram]: ", "p_item_id         : " + Integer.toString(xp_item_id));
                        Log.d("[GudangGaram]: ", "p_item_code       : " + xp_item_code);
                        Log.d("[GudangGaram]: ", "p_item_desc       : " + xp_item_desc);
                        Log.d("[GudangGaram]: ", "p_qty             : " + xp_quantity.toString());
                        Log.d("[GudangGaram]: ", "p_qty_rcv         : " + xp_quantity_rcv.toString());
                        Log.d("[GudangGaram]: ", "p_uom             : " + xp_uom);
                        Log.d("[GudangGaram]: ", "p_reject_reason   : " + xp_reject_reason);
                        Log.d("[GudangGaram]: ", "p_received_flag   : " + xp_check_sent);
                        Log.d("[GudangGaram]: ", "p_loading_date    : " + xp_loading_date);
                        Log.d("[GudangGaram]: ", "p_send_date       : " + xp_send_date);
                        Log.d("[GudangGaram]: ", "p_created_date    : " + xp_created_date);
                        Log.d("[GudangGaram]: ", "p_real_rcv_by     : " + xp_real_received_by);
                        Log.d("[GudangGaram]: ", "p_real_rcv_by_nik : " + xp_real_received_by_nik);
                        Log.d("[GudangGaram]: ", "p_loaded_by_id    : " + xp_loaded_by_id);
                        Log.d("[GudangGaram]: ", "p_loaded_by       : " + xp_loaded_by);

                        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                        envelope.dotNet = true;
                        envelope.setOutputSoapObject(request);

                        //Web method call
                        try {
                            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                            httpTransport.debug = true;
                            httpTransport.call(SOAP_ACTION, envelope);
                            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                            try {
                                SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                                xpd_do_line_id = resultsRequestSOAP.getProperty(0).toString();
                                Log.d("[GudangGaram]", "Get Oracle DO Line ID :" + xpd_do_line_id);

                                // check if insert succeed to get id detail from oracle
                                if (xpd_do_line_id.length() > 0) {
                                    SentResponse = xp_dod_id.toString();
                                    // delete manifest sent from local
                                    // dbHelper.delManifestSent(xpd_do_line_id);
                                } else {
                                    SentResponse = "*";
                                }
                            } catch (Exception e) {
                                Log.d("[GudangGaram]", "HTTP RESPONSE Exception:" + e.getMessage().toString());
                            }
                        } catch (Exception ex) {
                            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
                        }
                        // ----------- publish notification ----------
                        Log.d("[GudangGaram]:", "SoapSendDoDetailTaskAsync  :: end doInBackground");
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // publish progress
                        publishProgress("Sending Oracle Do Line ID = " + xpd_do_line_id);

                    } // end for
                } // end if
                try {
                    dbHelper.delManifestHeader(hdr.getDo_header_id());
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } //end for each
        }
        else{
            SentResponse = "";
        }
        return SentResponse;
    }
}
