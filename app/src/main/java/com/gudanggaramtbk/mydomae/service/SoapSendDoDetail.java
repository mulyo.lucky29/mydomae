package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.mydomae.activity.SyncAction;
import com.gudanggaramtbk.mydomae.model.CSendInfo;
import com.gudanggaramtbk.mydomae.model.ManifestDetail;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import java.util.List;

public class SoapSendDoDetail {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private SyncAction pActivity;
    private List<ManifestDetail> listparam;
    private List<CSendInfo> objlistheader;

    // override constructor
    public SoapSendDoDetail(SharedPreferences PConfig, Context context, MySQLiteHelper dbHelper, List<CSendInfo> listheader) {
        Log.d("[GudangGaram]: ", "SoapSendDoDetail Constructor");
        this.config = PConfig;
        this.context = context;
        this.dbHelper = dbHelper;
        this.objlistheader = listheader;
    }
    public void setParentActivity(SyncAction pActivity) {
        this.pActivity = pActivity;
    }

    public void Send() {
        Log.d("[GudangGaram]: ", "SentDODetailToOracle Begin");
        try {
            // call web service to send detail
            SoapSendDoDetailTaskAsync SoapRequest = new SoapSendDoDetailTaskAsync(new SoapSendDoDetailTaskAsync.SoapSendDoDetailTaskAsyncResponse() {
                @Override
                public void PostSentAction(String detail_id) {
                    Log.d("[GudangGaram]: ", "SoapSendDoDetailTaskAsync Response "  + detail_id);
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, objlistheader);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                Log.d("[GudangGaram]: ", "Using HC Higer");
            }
            else {
                Log.d("[GudangGaram]: ", "Using HC Lower");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]: ", "SentDODetailToOracle Exception " + e.getMessage().toString());
        }
    }
}
