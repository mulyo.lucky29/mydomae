package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.model.ManifestDetail;
import com.gudanggaramtbk.mydomae.model.ManifestHeader;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.activity.SyncAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 10/9/2017.
 */

public class SoapSendDoHeader {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private SyncAction pActivity;

    // override constructor
    public SoapSendDoHeader(SharedPreferences PConfig, Context ctx, MySQLiteHelper dbHelper) {
        Log.d("[GudangGaram]: ", "SoapSendDoHeader Constructor");
        this.config   = PConfig;
        this.context  = ctx;
        this.dbHelper = dbHelper;
    }
    public void setParentActivity(SyncAction pActivity) {
        this.pActivity = pActivity;
    }

    public void Send(){
        long RecordCount = 0;
        //String pwhere = "WHERE MD_CHECK_LOADING = 'Y' and MD_CHECK_SENT = 'Y' ";
        String pwhere = "WHERE MD_CHECK_LOADING = 'Y' and MD_REF_SENT IS NOT NULL ";
        try{
            // check if data to be sent is exists first to avoid duplicate
            RecordCount = dbHelper.getRecordCount("GGGG_DOMAE_DEVICE_D",pwhere);
            if(RecordCount > 0){
                Send_DO_Header();
            }
            else{
                Toast.makeText(context,"Nothing Can Be Sent To Oracle", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e){
            Log.d("[GudangGaram]: ", "Send Exception >> " + e.getMessage().toString());
        }
    }

    public void Send_DO_Header() {
        Integer index;
        //byte[]  hmx_MDO_SIGNATURE;
        Log.d("[GudangGaram]:", "SentDOHeaderToOracle Begin");

        List<ManifestHeader> LDOH = new ArrayList<ManifestHeader>();
        LDOH = dbHelper.get_DOHeader();
        Log.d("[GudangGaram]:", "SentDOHeaderToOracle COUNT " + LDOH.size());
        if(LDOH.size() > 0){
            try{
                    // call WS To send Header To Oracle
                    SoapSendDoHeaderTaskAsync SoapRequest = new SoapSendDoHeaderTaskAsync(new SoapSendDoHeaderTaskAsync.SoapSendDoHeaderTaskAsyncResponse() {
                        @Override
                        public void PostSentAction(String do_oracle_header_id) {
                            Log.d("[GudangGaram]", " SentDOHeaderPostSentAction Output Oracle Header ID >> " + do_oracle_header_id);
                        }
                    });
                    SoapRequest.setAttribute(context, dbHelper, config, LDOH);
                    if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                        //work on sgs3 android 4.0.4
                        //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                        Log.d("[GudangGaram]: ", "Using HC Higer");
                        SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                    }
                    else {
                        Log.d("[GudangGaram]: ", "Using HC Lower");
                        SoapRequest.execute(); // work on sgs2 android 2.3
                    }
            }
            catch(Exception e){
                Log.d("[GudangGaram]:", "SentDOHeaderToOracle Exception " + e.getMessage().toString());
            }
        }
        else{
            Log.d("[GudangGaram]:", "SentDOHeaderToOracle No Data");
        }
        Log.d("[GudangGaram]:", "SentDOHeaderToOracle End");
    }
}
