package com.gudanggaramtbk.mydomae.service;

/**
 * Created by LuckyM on 10/6/2017.
 */

public interface SoapRetrieveEmployeeTaskAsyncResponse {
    void PostSentAction(String output);
}
