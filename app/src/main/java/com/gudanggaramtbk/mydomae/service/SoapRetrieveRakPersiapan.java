package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;
import android.widget.Spinner;

import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.activity.SyncAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/10/2017.
 */

public class SoapRetrieveRakPersiapan {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private SyncAction pActivity;

    // override constructor
    public SoapRetrieveRakPersiapan(SharedPreferences PConfig){
        Log.d("[GudangGaram]: ", "SoapRetrieveRakPersiapan Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]: ", "SetContext");
        this.context = context;
    }
    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]: ", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public String Retrieve(ListView lst, Spinner cbo) {
        final List<String> Result = new ArrayList<String>();
        SyncAction mx = new SyncAction();
        try {
            SoapRetrieveRakPersiapanTaskAsync SoapRequest = new SoapRetrieveRakPersiapanTaskAsync(new SoapRetrieveRakPersiapanTaskAsync.SoapRetrieveRakPersiapanTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config,cbo, lst);
            //SoapRequest.execute();
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
        return "";
    }

}
