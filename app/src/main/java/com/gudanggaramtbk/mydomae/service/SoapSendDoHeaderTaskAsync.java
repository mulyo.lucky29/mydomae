package com.gudanggaramtbk.mydomae.service;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.mydomae.adapter.CustomListAdapter;
import com.gudanggaramtbk.mydomae.holder.HolderManifestList;
import com.gudanggaramtbk.mydomae.model.CSendInfo;
import com.gudanggaramtbk.mydomae.model.ManifestHeader;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by LuckyM on 10/9/2017.
 */

        /*
         PROCEDURE INSERT_DO_HEADER(
                p_device_id       IN VARCHAR2,
                p_do_id           IN NUMBER,
                p_vehicle_no      IN VARCHAR2,
                p_requestor       IN VARCHAR2,
                p_location        IN VARCHAR2,
                p_trx_no          IN VARCHAR2,
                p_received_by_id  IN VARCHAR2,
                p_received_by     IN VARCHAR2,
                p_signature       IN BLOB,
                p_signature_fn    IN VARCHAR2,
                p_signature_fs    IN NUMBER,
                p_created_date    IN VARCHAR2,
                p_real_rcv_by     IN VARCHAR2,
                p_real_rcv_by_nik IN VARCHAR2,
                p_do_header_id   OUT NUMBER
        */


public class SoapSendDoHeaderTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private Integer LPosition;
    private String xpd_do_header_id;
    private SoapSendDoDetail DoDetail;
    private List<ManifestHeader> LDOH;
    private List<CSendInfo> listheader;

    public SoapSendDoHeaderTaskAsync(){ }
    public interface SoapSendDoHeaderTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSendDoHeaderTaskAsync.SoapSendDoHeaderTaskAsyncResponse delegate = null;
    public SoapSendDoHeaderTaskAsync(SoapSendDoHeaderTaskAsync.SoapSendDoHeaderTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             List<ManifestHeader> oMH){

        Log.d("[GudangGaram]", "SoapSendDoHeaderTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.pd = new ProgressDialog(this.context);
        LDOH = new ArrayList<ManifestHeader>();
        LDOH = oMH;

        listheader = new ArrayList<>();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendDoHeaderTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Send Do Header To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        // after finish send header to oracle then proceed detail sending
        try{
            // after header sent then summon procedure to send do detail based on list pair do_header_id and oracle id
            if(listheader.size() > 0){
                DoDetail = new SoapSendDoDetail(config, context, dbHelper, listheader);
                DoDetail.Send();
                Log.d("[GudangGaram]", " listheader count -> " + listheader.size());
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", " SentDOHeaderPostSentAction Exception : " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "SoapSendDoHeaderTaskAsync :: onPostExecute >> " + output);
        delegate.PostSentAction(output);
    }

    public String replace_format_indo(String str){
        String result;
        result = str.replace("Mei","May").replace("Agt","Aug").replace("Okt","Oct").replace("Des","Dec");
        return result;
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;

        xpd_do_header_id = "-1";
        //String response = null;
        if(LDOH.size() > 0){
            for(int idx=0; idx < LDOH.size(); idx++) {
                //final String do_header_id         = hmx_MDO_ID;
                String NAMESPACE        = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                String SOAP_ADDRESS     = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
                String OPERATION_NAME   = "Insert_DOHeader";
                String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
                SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                String  xp_device_id            = Settings.Secure.getString(this.context.getContentResolver(), Settings.Secure.ANDROID_ID);
                Integer xp_do_header_id         = Integer.parseInt(LDOH.get(idx).getMDO_ID().toString());
                String  xp_vehicle_no           = LDOH.get(idx).getMDO_VEHICLE_NUMBER();
                String  xp_requestor            = LDOH.get(idx).getMDO_REQUESTOR();
                String  xp_location             = LDOH.get(idx).getMDO_LOCATION();
                String  xp_trx_no               = LDOH.get(idx).getMDO_TRX_NUMBER();
                String  xp_received_by_id       = LDOH.get(idx).getMDO_RECEIVED_BY_ID();
                String  xp_received_by          = LDOH.get(idx).getMDO_RECEIVED_BY();
                String  xp_signature            = Base64.encodeToString(LDOH.get(idx).getMDO_SIGNATURE(), Base64.DEFAULT);;
                String  xp_signature_fn         = LDOH.get(idx).getMDO_SIGNATURE_FN();
                Integer xp_signature_fs         = LDOH.get(idx).getMDO_SIGNATURE_FS();
                String  xp_created_date         = replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));
                String  xp_real_received_by     = LDOH.get(idx).getMDO_REAL_RECEIVED_BY();
                String  xp_real_received_by_nik = LDOH.get(idx).getMDO_REAL_RECEIVED_BY_NIK();

                Log.d("[GudangGaram]: ", "SOAP REQUEST >> Insert_DOHeader");
                // =============== p_device_id ====================
                PropertyInfo prop_p_device_id = new PropertyInfo();
                prop_p_device_id.setName("p_device_id");
                prop_p_device_id.setValue(xp_device_id);
                prop_p_device_id.setType(String.class);
                request.addProperty(prop_p_device_id);
                // =============== p_do_id ========================
                PropertyInfo prop_p_do_id = new PropertyInfo();
                prop_p_do_id.setName("p_do_id");
                prop_p_do_id.setValue(xp_do_header_id);
                prop_p_do_id.setType(Number.class);
                request.addProperty(prop_p_do_id);
                // =============== p_vehicle_no ===================
                PropertyInfo prop_p_vehicle_no = new PropertyInfo();
                prop_p_vehicle_no.setName("p_vehicle_no");
                prop_p_vehicle_no.setValue(xp_vehicle_no);
                prop_p_vehicle_no.setType(String.class);
                request.addProperty(prop_p_vehicle_no);
                // =============== p_requestor =====================
                PropertyInfo prop_p_requestor = new PropertyInfo();
                prop_p_requestor.setName("p_requestor");
                prop_p_requestor.setValue(xp_requestor);
                prop_p_requestor.setType(String.class);
                request.addProperty(prop_p_requestor);
                // =============== p_location =====================
                PropertyInfo prop_p_location = new PropertyInfo();
                prop_p_location.setName("p_location");
                prop_p_location.setValue(xp_location);
                prop_p_location.setType(String.class);
                request.addProperty(prop_p_location);
                // =============== p_trx_no ======================
                PropertyInfo prop_p_trx_no = new PropertyInfo();
                prop_p_trx_no.setName("p_trx_no");
                prop_p_trx_no.setValue(xp_trx_no);
                prop_p_trx_no.setType(String.class);
                request.addProperty(prop_p_trx_no);
                // =============== p_received_by_id ==================
                PropertyInfo prop_p_received_by_id  = new PropertyInfo();
                prop_p_received_by_id.setName("p_received_by_id");
                prop_p_received_by_id.setValue(xp_received_by_id);
                prop_p_received_by_id.setType(String.class);
                request.addProperty(prop_p_received_by_id);
                // =============== p_received_by ==================
                PropertyInfo prop_p_received_by  = new PropertyInfo();
                prop_p_received_by.setName("p_received_by");
                prop_p_received_by.setValue(xp_received_by);
                prop_p_received_by.setType(String.class);
                request.addProperty(prop_p_received_by);
                // =============== p_signature ===================
                PropertyInfo prop_p_signature = new PropertyInfo();
                prop_p_signature.setName("p_signature");
                prop_p_signature.setValue(xp_signature);
                prop_p_signature.setType(String.class);
                request.addProperty(prop_p_signature);
                // =============== p_signature_fn ==================
                PropertyInfo prop_p_signature_fn  = new PropertyInfo();
                prop_p_signature_fn.setName("p_signature_fn");
                prop_p_signature_fn.setValue(xp_signature_fn);
                prop_p_signature_fn.setType(String.class);
                request.addProperty(prop_p_signature_fn);
                // =============== p_signature_fs ==================
                PropertyInfo prop_p_signature_fs  = new PropertyInfo();
                prop_p_signature_fs.setName("p_signature_fs");
                prop_p_signature_fs.setValue(xp_signature_fs);
                prop_p_signature_fs.setType(Number.class);
                request.addProperty(prop_p_signature_fs);
                // =============== p_created_date ===================
                PropertyInfo prop_p_created_date = new PropertyInfo();
                prop_p_created_date.setName("p_created_date");
                prop_p_created_date.setValue(xp_created_date);
                prop_p_created_date.setType(String.class);
                request.addProperty(prop_p_created_date);
                // =============== p_real_rcv_by ==================
                PropertyInfo prop_p_real_rcv_by  = new PropertyInfo();
                prop_p_real_rcv_by.setName("p_real_rcv_by");
                prop_p_real_rcv_by.setValue(xp_real_received_by);
                prop_p_real_rcv_by.setType(String.class);
                request.addProperty(prop_p_real_rcv_by);
                // =============== p_real_rcv_by_nik ==================
                PropertyInfo prop_p_real_rcv_by_nik  = new PropertyInfo();
                prop_p_real_rcv_by_nik.setName("p_real_rcv_by_nik");
                prop_p_real_rcv_by_nik.setValue(xp_real_received_by_nik);
                prop_p_real_rcv_by_nik.setType(String.class);
                request.addProperty(prop_p_real_rcv_by_nik);

                Log.d("[GudangGaram]: ", "p_device_id       : " + xp_device_id);
                Log.d("[GudangGaram]: ", "p_do_id           : " + Integer.toString(xp_do_header_id));
                Log.d("[GudangGaram]: ", "p_vehicle_no      : " + xp_vehicle_no);
                Log.d("[GudangGaram]: ", "p_requestor       : " + xp_requestor);
                Log.d("[GudangGaram]: ", "p_location        : " + xp_location);
                Log.d("[GudangGaram]: ", "p_trx_no          : " + xp_trx_no);
                Log.d("[GudangGaram]: ", "p_received_by_id  : " + xp_received_by_id);
                Log.d("[GudangGaram]: ", "p_received_by     : " + xp_received_by);
                Log.d("[GudangGaram]: ", "p_signature       : " + xp_signature);
                Log.d("[GudangGaram]: ", "p_signature_fn    : " + xp_signature_fn);
                Log.d("[GudangGaram]: ", "p_signature_fs    : " + Integer.toString(xp_signature_fs));
                Log.d("[GudangGaram]: ", "p_created_date    : " + xp_created_date);
                Log.d("[GudangGaram]: ", "p_real_rcv_by     : " + xp_real_received_by);
                Log.d("[GudangGaram]: ", "p_real_rcv_by_nik : " + xp_real_received_by_nik);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.implicitTypes=false;
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                //Web method call
                try {
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    //HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,3000);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    httpTransport.call(SOAP_ACTION, envelope);

                    SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                    xpd_do_header_id = resultsRequestSOAP.getProperty(0).toString();
                    SentResponse = xpd_do_header_id;
                }
                catch (Exception ex) {
                    xpd_do_header_id = "-1";
                    SentResponse = ex.getMessage().toString();
                    Log.d("[GudangGaram]", "Catch : " + SentResponse);
                    //response = "<?xml version='1.0' encoding='utf-8'?><Message response='FAIL' ErrorMessage='The System is under maintenance or having some techical issue.'></Message>";
                }
                finally {
                    // ------- collecting list mapping info header sent
                    CSendInfo ci = new CSendInfo();
                    ci.setDo_header_id(xp_do_header_id.toString());
                    ci.setDo_oracle_header_id(xpd_do_header_id);
                    listheader.add(ci);
                }

                // ----------- publish notification ----------
                try {
                    Thread.sleep(20);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // publish progress
                publishProgress("Sending Oracle Do Header ID = " + xpd_do_header_id.toString());
            } // end for
        } // end if

        Log.d("[GudangGaram]:", "SoapSendDoHeaderTaskAsync  :: end doInBackground");

        return SentResponse;
    }
}
