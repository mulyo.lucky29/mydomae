package com.gudanggaramtbk.mydomae.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;

import com.gudanggaramtbk.mydomae.holder.HolderNoTrx;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.activity.SyncAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/8/2017.
 */

public class SoapRetrieveBinPersiapan {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private SyncAction pActivity;
    //List<String> TResult;
    List<HolderNoTrx> TResult;

    // override constructor
    public SoapRetrieveBinPersiapan(SharedPreferences PConfig){
        Log.d("[GudangGaram]: ", "SoapRetrieveBinPersiapan Constructor");
        this.config = PConfig;
        //TResult = new ArrayList<String>();

        TResult = new ArrayList<HolderNoTrx>();
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]: ", "SetContext");
        this.context = context;
    }
    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]: ", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    /*
    public List<String> Retrieve(ListView lvi, String rak_persiapan) {
        final List<String> Result = new ArrayList<String>();

        SyncAction mx = new SyncAction();
        Log.d("[GudangGaram]: ", "Retrieve Bin Persiapan Begin >> " + rak_persiapan);
        try {
            SoapRetrieveBinPersiapanTaskAsync SoapRequest = new SoapRetrieveBinPersiapanTaskAsync(new SoapRetrieveBinPersiapanTaskAsync.SoapRetrieveBinPersiapanTaskAsyncResponse() {
                @Override
                public void PostSentAction(List<String> output) {
                    TResult = output;
                    Log.d("[GudangGaram]: ", "PostSentAction Output : " + output.size());

                }
            });
            SoapRequest.setAttribute(context, dbHelper, config,rak_persiapan,lvi);
            SoapRequest.execute();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
        return TResult;
    }
    */

    public List<HolderNoTrx> Retrieve(ListView lvi, String rak_persiapan) {
        //final List<String> Result = new ArrayList<String>();
        SyncAction mx = new SyncAction();
        Log.d("[GudangGaram]: ", "Retrieve Bin Persiapan Begin >> " + rak_persiapan);
        try {
            SoapRetrieveBinPersiapanTaskAsync SoapRequest = new SoapRetrieveBinPersiapanTaskAsync(new SoapRetrieveBinPersiapanTaskAsync.SoapRetrieveBinPersiapanTaskAsyncResponse() {
                @Override
                public void PostSentAction(List<HolderNoTrx> output) {
                    TResult = output;
                    Log.d("[GudangGaram]: ", "PostSentAction Output : " + output.size());
                    /*
                    // for debug only just open this comment
                    for (int a=0;a<output.size();a++){
                        Log.d("[GudangGaram]: ", "PostSentAction Output - No Trx       : " + TResult.get(a).getNotrx());
                        Log.d("[GudangGaram]: ", "PostSentAction Output - Bin Persiapan: " + TResult.get(a).getBinpersiapan());
                    }
                    */
                }
                /*
                public void PostSentAction(List<String> output) {
                    TResult = output;
                    Log.d("[GudangGaram]: ", "PostSentAction Output : " + output.size());
                }
                */
            });
            SoapRequest.setAttribute(context, dbHelper, config,rak_persiapan,lvi);
            //SoapRequest.execute();
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
        return TResult;
    }

}
