package com.gudanggaramtbk.mydomae.viewholder;

import android.widget.TextView;

/**
 * Created by LuckyM on 11/15/2017.
 */

public class ViewHolder {
    public TextView Xtxt_ID;
    public TextView Xtxt_trx_num;
    public TextView Xtxt_loadingDate;
    public TextView Xtxt_ReceivedBy;
    public TextView Xtxt_ReceivedDate;
    public TextView Xtxt_vehicle_no;
    public TextView Xtxt_location;
}
