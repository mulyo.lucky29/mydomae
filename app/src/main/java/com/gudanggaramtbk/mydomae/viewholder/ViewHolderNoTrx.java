package com.gudanggaramtbk.mydomae.viewholder;

import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by LuckyM on 5/2/2018.
 */

public class ViewHolderNoTrx {
    public TextView Xtxt_no_trx;
    public TextView Xtxt_bin_persiapan;
    public CheckBox Xchk_select;
}
