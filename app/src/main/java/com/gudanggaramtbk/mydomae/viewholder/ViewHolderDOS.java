package com.gudanggaramtbk.mydomae.viewholder;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by LuckyM on 10/10/2017.
 */

public class ViewHolderDOS {
    public TextView XID;
    public TextView XITEM_CODE;
    public TextView XITEM_DESC;
    public TextView XQTY;
    public EditText XQTYR;
    public TextView XUOM;
    public CheckBox XCHECK;
    public Spinner  XREASON_CODE;

}
