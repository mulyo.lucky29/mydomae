package com.gudanggaramtbk.mydomae.viewholder;

import android.widget.TextView;

public class ViewHolderGEmployeeLov {
    public TextView XPersonID;
    public TextView XPersonName;
    public TextView XPersonNIK;

    // ----------- getter ---------
    public String getXPersonID()   { return XPersonID.getText().toString(); }
    public String getXPersonName() { return XPersonName.getText().toString(); }
    public String getXPersonNIK()     { return XPersonNIK.getText().toString(); }

    // ------------ setter ----------
    public void setXPersonID(TextView XPersonID) {
        this.XPersonID = XPersonID;
    }
    public void setXPersonName(TextView XPersonName) {
        this.XPersonName = XPersonName;
    }
    public void setXPersonNIK(String StrSegment1) { this.XPersonNIK.setText(StrSegment1); }
}
