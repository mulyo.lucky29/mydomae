package com.gudanggaramtbk.mydomae.model;

/**
 * Created by LuckyM on 10/9/2017.
 */


public class ManifestDetail {

    private Integer MD_HEADER_ORA;
    private Integer MD_ID;
    private Integer MH_ID;
    private String  MD_VEHICLE_NO;
    private String  MD_REQUESTOR;
    private String  MD_LOCATION;
    private String  MD_TRX_NUMBER;
    private String  MD_RECEIVED_BY_ID;
    private String  MD_RECEIVED_BY;
    private String  MD_RAK_PERSIAPAN;
    private String  MD_BIN_PERSIAPAN;
    private Integer MD_ORG_ID;
    private Integer MD_ITEM_ID;
    private String  MD_ITEM_CODE;
    private String  MD_ITEM_DESC;
    private String  MD_QTY;
    private String  MD_QTY_RCV;
    private String  MD_UOM;
    private String  MD_CHECK_LOADING;
    private String  MD_LOADING_DATE;
    private String  MD_CHECK_SENT;
    private String  MD_SENT_DATE;
    private String  MD_REASON_CODE;
    private String  MD_CREATED_DATE;
    private String  MD_DEVICE_ID;
    private String  MD_REAL_RECEIVED_BY;
    private String  MD_REAL_RECEIVED_BY_NIK;
    private String  MD_LOADED_BY_ID;
    private String  MD_LOADED_BY;


    public Integer getMD_HEADER_ORA() {
        return MD_HEADER_ORA;
    }
    public Integer getMD_ID() {
        return MD_ID;
    }
    public Integer getMH_ID() {
        return MH_ID;
    }
    public String getMD_VEHICLE_NO() {
        return MD_VEHICLE_NO;
    }
    public String getMD_REQUESTOR() {
        return MD_REQUESTOR;
    }
    public String getMD_LOCATION() {
        return MD_LOCATION;
    }
    public String getMD_TRX_NUMBER() {
        return MD_TRX_NUMBER;
    }
    public String getMD_RECEIVED_BY_ID() {
        return MD_RECEIVED_BY_ID;
    }
    public String getMD_RECEIVED_BY() {
        return MD_RECEIVED_BY;
    }
    public String getMD_RAK_PERSIAPAN() {
        return MD_RAK_PERSIAPAN;
    }
    public String getMD_BIN_PERSIAPAN() {
        return MD_BIN_PERSIAPAN;
    }
    public Integer getMD_ORG_ID() {
        return MD_ORG_ID;
    }
    public Integer getMD_ITEM_ID() {
        return MD_ITEM_ID;
    }
    public String getMD_ITEM_CODE() {
        return MD_ITEM_CODE;
    }
    public String getMD_ITEM_DESC() {
        return MD_ITEM_DESC;
    }
    public String getMD_QTY() {
        return MD_QTY;
    }
    public String getMD_QTY_RCV() {
        return MD_QTY_RCV;
    }
    public String getMD_UOM() {
        return MD_UOM;
    }
    public String getMD_CHECK_LOADING() {
        return MD_CHECK_LOADING;
    }
    public String getMD_LOADING_DATE() {
        return MD_LOADING_DATE;
    }
    public String getMD_CHECK_SENT() {
        return MD_CHECK_SENT;
    }
    public String getMD_SENT_DATE() {
        return MD_SENT_DATE;
    }
    public String getMD_REASON_CODE() {
        return MD_REASON_CODE;
    }
    public String getMD_CREATED_DATE() {
        return MD_CREATED_DATE;
    }
    public String getMD_DEVICE_ID() {
        return MD_DEVICE_ID;
    }
    public String getMD_REAL_RECEIVED_BY_NIK() { return MD_REAL_RECEIVED_BY_NIK; }
    public String getMD_REAL_RECEIVED_BY() { return MD_REAL_RECEIVED_BY; }
    public String getMD_LOADED_BY_ID() { return MD_LOADED_BY_ID; }
    public String getMD_LOADED_BY() { return MD_LOADED_BY; }


    public void setMD_HEADER_ORA(Integer MD_HEADER_ORA) {
        this.MD_HEADER_ORA = MD_HEADER_ORA;
    }
    public void setMD_ID(Integer MD_ID) {
        this.MD_ID = MD_ID;
    }
    public void setMH_ID(Integer MH_ID) {
        this.MH_ID = MH_ID;
    }
    public void setMD_VEHICLE_NO(String MD_VEHICLE_NO) {
        this.MD_VEHICLE_NO = MD_VEHICLE_NO;
    }
    public void setMD_REQUESTOR(String MD_REQUESTOR) {
        this.MD_REQUESTOR = MD_REQUESTOR;
    }
    public void setMD_LOCATION(String MD_LOCATION) {
        this.MD_LOCATION = MD_LOCATION;
    }
    public void setMD_TRX_NUMBER(String MD_TRX_NUMBER) {
        this.MD_TRX_NUMBER = MD_TRX_NUMBER;
    }
    public void setMD_RECEIVED_BY_ID(String MD_RECEIVED_BY_ID) { this.MD_RECEIVED_BY_ID = MD_RECEIVED_BY_ID; }
    public void setMD_RECEIVED_BY(String MD_RECEIVED_BY) {
        this.MD_RECEIVED_BY = MD_RECEIVED_BY;
    }
    public void setMD_RAK_PERSIAPAN(String MD_RAK_PERSIAPAN) { this.MD_RAK_PERSIAPAN = MD_RAK_PERSIAPAN; }
    public void setMD_BIN_PERSIAPAN(String MD_BIN_PERSIAPAN) { this.MD_BIN_PERSIAPAN = MD_BIN_PERSIAPAN; }
    public void setMD_ORG_ID(Integer MD_ORG_ID) {
        this.MD_ORG_ID = MD_ORG_ID;
    }
    public void setMD_ITEM_ID(Integer MD_ITEM_ID) {
        this.MD_ITEM_ID = MD_ITEM_ID;
    }
    public void setMD_ITEM_CODE(String MD_ITEM_CODE) {
        this.MD_ITEM_CODE = MD_ITEM_CODE;
    }
    public void setMD_ITEM_DESC(String MD_ITEM_DESC) {
        this.MD_ITEM_DESC = MD_ITEM_DESC;
    }
    public void setMD_QTY(String MD_QTY) {
        this.MD_QTY = MD_QTY;
    }
    public void setMD_QTY_RCV(String MD_QTY_RCV) {
        this.MD_QTY_RCV = MD_QTY_RCV;
    }
    public void setMD_UOM(String MD_UOM) {
        this.MD_UOM = MD_UOM;
    }
    public void setMD_CHECK_LOADING(String MD_CHECK_LOADING) { this.MD_CHECK_LOADING = MD_CHECK_LOADING; }
    public void setMD_LOADING_DATE(String MD_LOADING_DATE) { this.MD_LOADING_DATE = MD_LOADING_DATE; }
    public void setMD_CHECK_SENT(String MD_CHECK_SENT) {
        this.MD_CHECK_SENT = MD_CHECK_SENT;
    }
    public void setMD_SENT_DATE(String MD_SENT_DATE) {
        this.MD_SENT_DATE = MD_SENT_DATE;
    }
    public void setMD_REASON_CODE(String MD_REASON_CODE) {
        this.MD_REASON_CODE = MD_REASON_CODE;
    }
    public void setMD_CREATED_DATE(String MD_CREATED_DATE) { this.MD_CREATED_DATE = MD_CREATED_DATE; }
    public void setMD_DEVICE_ID(String MD_DEVICE_ID) {
        this.MD_DEVICE_ID = MD_DEVICE_ID;
    }
    public void setMD_REAL_RECEIVED_BY_NIK(String MD_REAL_RECEIVED_BY_NIK) { this.MD_REAL_RECEIVED_BY_NIK = MD_REAL_RECEIVED_BY_NIK; }
    public void setMD_REAL_RECEIVED_BY(String MD_REAL_RECEIVED_BY) { this.MD_REAL_RECEIVED_BY = MD_REAL_RECEIVED_BY; }
    public void setMD_LOADED_BY_ID(String MD_LOADED_BY_ID) { this.MD_LOADED_BY_ID = MD_LOADED_BY_ID; }
    public void setMD_LOADED_BY(String MD_LOADED_BY) { this.MD_LOADED_BY = MD_LOADED_BY; }


}
