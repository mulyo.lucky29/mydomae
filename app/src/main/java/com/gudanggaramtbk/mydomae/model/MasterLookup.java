package com.gudanggaramtbk.mydomae.model;

/**
 * Created by LuckyM on 10/4/2017.
 */

public class MasterLookup {
    private String ML_Context;
    private String ML_Value;
    private String ML_Description;


    // =============== getter =====================
    public String getML_Context() {
        return ML_Context;
    }
    public String getML_Value() {
        return ML_Value;
    }
    public String getML_Description() {
        return ML_Description;
    }

    // ================ setter ===================
    public void setML_Context(String ML_Context) {
        this.ML_Context = ML_Context;
    }
    public void setML_Value(String ML_Value) {
        this.ML_Value = ML_Value;
    }
    public void setML_Description(String ML_Description) {
        this.ML_Description = ML_Description;
    }
}
