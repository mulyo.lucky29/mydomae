package com.gudanggaramtbk.mydomae.model;

public class CSendInfo {
    private String do_header_id;
    private String do_oracle_header_id;

    public String getDo_header_id() {
        return do_header_id;
    }
    public String getDo_oracle_header_id() {
        return do_oracle_header_id;
    }

    public void setDo_header_id(String do_header_id) {
        this.do_header_id = do_header_id;
    }
    public void setDo_oracle_header_id(String do_oracle_header_id) {
        this.do_oracle_header_id = do_oracle_header_id;
    }


}
