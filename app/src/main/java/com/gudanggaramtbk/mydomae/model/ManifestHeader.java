package com.gudanggaramtbk.mydomae.model;

/**
 * Created by LuckyM on 10/19/2017.
 */

public class ManifestHeader {
    private Integer MDO_ID;
    private String  MDO_VEHICLE_NUMBER;
    private String  MDO_REQUESTOR;
    private String  MDO_LOCATION;
    private String  MDO_TRX_NUMBER;
    private String  MDO_RECEIVED_BY_ID;
    private String  MDO_RECEIVED_BY;

    private byte[]  MDO_SIGNATURE;
    private String  MDO_SIGNATURE_FN;
    private Integer MDO_SIGNATURE_FS;
    private String  MDO_DEVICE_ID;

    private String  MDO_REAL_RECEIVED_BY;
    private String  MDO_REAL_RECEIVED_BY_NIK;


    public Integer getMDO_ID() {
        return MDO_ID;
    }
    public String  getMDO_VEHICLE_NUMBER() {
        return MDO_VEHICLE_NUMBER;
    }
    public String  getMDO_REQUESTOR() {
        return MDO_REQUESTOR;
    }
    public String  getMDO_LOCATION() {
        return MDO_LOCATION;
    }
    public String  getMDO_TRX_NUMBER() {
        return MDO_TRX_NUMBER;
    }
    public String  getMDO_RECEIVED_BY() {
        return MDO_RECEIVED_BY;
    }
    public String  getMDO_RECEIVED_BY_ID() { return MDO_RECEIVED_BY_ID; }
    public byte[]  getMDO_SIGNATURE() {
        return MDO_SIGNATURE;
    }
    public String  getMDO_SIGNATURE_FN() {
        return MDO_SIGNATURE_FN;
    }
    public Integer getMDO_SIGNATURE_FS() {
        return MDO_SIGNATURE_FS;
    }
    public String  getMDO_DEVICE_ID() {
        return MDO_DEVICE_ID;
    }
    public String  getMDO_REAL_RECEIVED_BY() { return MDO_REAL_RECEIVED_BY; }
    public String  getMDO_REAL_RECEIVED_BY_NIK() { return MDO_REAL_RECEIVED_BY_NIK; }


    public void setMDO_ID(Integer MDO_ID) {
        this.MDO_ID = MDO_ID;
    }
    public void setMDO_VEHICLE_NUMBER(String MDO_VEHICLE_NUMBER) { this.MDO_VEHICLE_NUMBER = MDO_VEHICLE_NUMBER; }
    public void setMDO_REQUESTOR(String MDO_REQUESTOR) {
        this.MDO_REQUESTOR = MDO_REQUESTOR;
    }
    public void setMDO_LOCATION(String MDO_LOCATION) {
        this.MDO_LOCATION = MDO_LOCATION;
    }
    public void setMDO_TRX_NUMBER(String MDO_TRX_NUMBER) {
        this.MDO_TRX_NUMBER = MDO_TRX_NUMBER;
    }
    public void setMDO_RECEIVED_BY_ID(String MDO_RECEIVED_BY_ID) { this.MDO_RECEIVED_BY_ID = MDO_RECEIVED_BY_ID; }
    public void setMDO_RECEIVED_BY(String MDO_RECEIVED_BY) { this.MDO_RECEIVED_BY = MDO_RECEIVED_BY; }
    public void setMDO_SIGNATURE(byte[] MDO_SIGNATURE) {
        this.MDO_SIGNATURE = MDO_SIGNATURE;
    }
    public void setMDO_SIGNATURE_FN(String MDO_SIGNATURE_FN) { this.MDO_SIGNATURE_FN = MDO_SIGNATURE_FN; }
    public void setMDO_SIGNATURE_FS(Integer MDO_SIGNATURE_FS) { this.MDO_SIGNATURE_FS = MDO_SIGNATURE_FS; }
    public void setMDO_DEVICE_ID(String MDO_DEVICE_ID) {
        this.MDO_DEVICE_ID = MDO_DEVICE_ID;
    }

    public void setMDO_REAL_RECEIVED_BY(String MDO_REAL_RECEIVED_BY) { this.MDO_REAL_RECEIVED_BY = MDO_REAL_RECEIVED_BY; }
    public void setMDO_REAL_RECEIVED_BY_NIK(String MDO_REAL_RECEIVED_BY_NIK) { this.MDO_REAL_RECEIVED_BY_NIK = MDO_REAL_RECEIVED_BY_NIK; }

}
