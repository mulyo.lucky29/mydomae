package com.gudanggaramtbk.mydomae.model;

/**
 * Created by LuckyM on 9/28/2017.
 */

public class MasterEmployee {
    private int CPersonID;
    private String CEmployeName;
    private String CEmployeeNumber;
    private String CEmailAddress;
    private String CAttribute;

    public void MasterEmployee(int PCPerson_ID,
                               String PCEmployeeName,
                               String PCEmployeeNumber,
                               String PCEmailAddress,
                               String PCAttribute){
        CPersonID       = PCPerson_ID;
        CEmployeName    = PCEmployeeName;
        CEmployeeNumber = PCEmployeeNumber;
        CEmailAddress   = PCEmailAddress;
        CAttribute      = PCAttribute;
    }

    // =================== getter =======================
    public String getCEmployeeNumber() {
        return CEmployeeNumber;
    }
    public String getCEmailAddress() {
        return CEmailAddress;
    }
    public String getCEmployeName() {
        return CEmployeName;
    }
    public int getCPersonID() {
        return CPersonID;
    }
    public String getCAttribute() { return CAttribute; }


    // =================== setter =======================
    public void setCPersonID(int pCPersonID) {
        this.CPersonID = pCPersonID;
    }
    public void setCEmployeeNumber(String pCEmployeeNumber) { this.CEmployeeNumber = pCEmployeeNumber; }
    public void setCEmailAddress(String pCEmailAddress) {
        this.CEmailAddress = pCEmailAddress;
    }
    public void setCEmployeName(String pCEmployeName) {
        this.CEmployeName = pCEmployeName;
    }
    public void setCAttribute(String pCAttribute) { this.CAttribute = pCAttribute; }

}
