package com.gudanggaramtbk.mydomae.model;

public class CEmployee {
    private Integer PersonID;
    private String  PersonNIK;
    private String  PersonName;

    // ==================== getter =======================
    public Integer getPersonID() {
        return PersonID;
    }
    public String getPersonNIK() {
        return PersonNIK;
    }
    public String getPersonName() {
        return PersonName;
    }

    // ==================== setter ========================
    public void setPersonID(Integer personID) {
        PersonID = personID;
    }
    public void setPersonNIK(String personNIK) {
        PersonNIK = personNIK;
    }
    public void setPersonName(String personName) {
        PersonName = personName;
    }

}
