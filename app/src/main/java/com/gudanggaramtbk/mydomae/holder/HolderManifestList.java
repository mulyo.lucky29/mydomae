package com.gudanggaramtbk.mydomae.holder;

import android.widget.TextView;

/**
 * Created by LuckyM on 11/15/2017.
 */

public class HolderManifestList {
    private String ID;
    private String trx_num;
    private String loadingDate;
    private String ReceivedBy;
    private String ReceivedDate;
    private String vehicle_no;
    private String location;


    public String getID() {
        return ID;
    }
    public String getTrx_num() {
        return trx_num;
    }
    public String getLoadingDate() {
        return loadingDate;
    }
    public String getReceivedBy() {
        return ReceivedBy;
    }
    public String getReceivedDate() {
        return ReceivedDate;
    }
    public String getVehicle_no() {
        return vehicle_no;
    }
    public String getLocation() {
        return location;
    }


    public void setID(String ID) {
        this.ID = ID;
    }
    public void setTrx_num(String trx_num) {
        this.trx_num = trx_num;
    }
    public void setLoadingDate(String loadingDate) {
        this.loadingDate = loadingDate;
    }
    public void setReceivedBy(String receivedBy) {
        ReceivedBy = receivedBy;
    }
    public void setReceivedDate(String receivedDate) {
        ReceivedDate = receivedDate;
    }
    public void setVehicle_no(String vehicle_no) {
        this.vehicle_no = vehicle_no;
    }
    public void setLocation(String location) {
        this.location = location;
    }

}
