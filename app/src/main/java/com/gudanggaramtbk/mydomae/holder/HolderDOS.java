package com.gudanggaramtbk.mydomae.holder;

/**
 * Created by luckym on 5/8/2018.
 */

public class HolderDOS {
    private String  SID;
    private String  SItemCode;
    private String  SItemDesc;
    private Number  SQty;
    private Number  SQtyR;
    private String  SUOM;
    private boolean SCheck;
    private String  SReasonCode;
    private Integer SReasonIndex;
    private String  SRequestor;
    private Integer RecordNumber;
    private Boolean enableReason;


    // ================ setter getter ==================
    public String  getSID() {
        return SID;
    }
    public String  getSItemCode() {
        return SItemCode;
    }
    public String  getSItemDesc() {
        return SItemDesc;
    }
    public Number  getSQty() {
        return SQty;
    }
    public Number  getSQtyR() { return SQtyR; }
    public String  getSUOM() {
        return SUOM;
    }
    public boolean getSCheck() {return SCheck;}
    public String  getSReasonCode() {
        return SReasonCode;
    }
    public String  getSRequestor() {
        return SRequestor;
    }
    public Integer getRecordNumber() {
        return RecordNumber;
    }
    public Integer getSReasonIndex() {
        return SReasonIndex;
    }
    public Boolean getEnableReason() {
        return enableReason;
    }

    public void setSID(String SID) {
        this.SID = SID;
    }
    public void setSItemCode(String SItemCode) {
        this.SItemCode = SItemCode;
    }
    public void setSItemDesc(String SItemDesc) {
        this.SItemDesc = SItemDesc;
    }
    public void setSQty(Number SQty) {
        this.SQty = SQty;
    }
    public void setSQtyR(Number SQtyR) { this.SQtyR = SQtyR; }
    public void setSUOM(String SUOM) {
        this.SUOM = SUOM;
    }
    public void setSCheck(boolean SCheck) {
        this.SCheck = SCheck;
    }
    public void setSReasonCode(String SReasonCode) {
        this.SReasonCode = SReasonCode;
    }
    public void setSRequestor(String SRequestor) {
        this.SRequestor = SRequestor;
    }
    public void setRecordNumber(Integer recordNumber) {
        RecordNumber = recordNumber;
    }
    public void setSReasonIndex(Integer SReasonIndex) {
        this.SReasonIndex = SReasonIndex;
    }
    public void setEnableReason(Boolean enableReason) {
        this.enableReason = enableReason;
    }

}
