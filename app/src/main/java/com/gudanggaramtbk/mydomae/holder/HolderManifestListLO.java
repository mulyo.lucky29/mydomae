package com.gudanggaramtbk.mydomae.holder;

/**
 * Created by luckym on 12/6/2017.
 */

public class HolderManifestListLO {
    private String vehicle_no;
    private String location;
    private String RakPersiapan;
    private String BinPersiapan;
    private String trx_num;


    public String getVehicle_no() {
        return vehicle_no;
    }
    public String getLocation() {
        return location;
    }
    public String getRakPersiapan() {
        return RakPersiapan;
    }
    public String getBinPersiapan() {
        return BinPersiapan;
    }
    public String getTrx_num() {
        return trx_num;
    }


    public void setVehicle_no(String vehicle_no) {
        this.vehicle_no = vehicle_no;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public void setRakPersiapan(String rakPersiapan) {
        RakPersiapan = rakPersiapan;
    }
    public void setBinPersiapan(String binPersiapan) {
        BinPersiapan = binPersiapan;
    }
    public void setTrx_num(String trx_num) {
        this.trx_num = trx_num;
    }

}
