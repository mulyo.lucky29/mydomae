package com.gudanggaramtbk.mydomae.holder;

/**
 * Created by LuckyM on 5/2/2018.
 */

public class HolderNoTrx {
    private String notrx;
    private String binpersiapan;
    private Boolean selected;

    // -------- setter ---------------
    public void setNotrx(String notrx) {
        this.notrx = notrx;
    }
    public void setBinpersiapan(String binpersiapan) {
        this.binpersiapan = binpersiapan;
    }
    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
    // -------- getter --------------
    public String getNotrx() {
        return notrx;
    }
    public String getBinpersiapan() {
        return binpersiapan;
    }
    public Boolean getSelected() {
        return selected;
    }
}
