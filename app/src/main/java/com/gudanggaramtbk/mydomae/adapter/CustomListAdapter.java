package com.gudanggaramtbk.mydomae.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gudanggaramtbk.mydomae.holder.HolderManifestList;
import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.viewholder.ViewHolder;

import java.util.List;

/**
 * Created by LuckyM on 11/15/2017.
 */

public class CustomListAdapter extends ArrayAdapter<HolderManifestList> {
        Context mcontext;
        List<HolderManifestList> scanList;
        private LayoutInflater mInflater;

        public CustomListAdapter(Context context, List<HolderManifestList> list)
        {
            super(context,0,list);
            mcontext = context;
            scanList = list;
            mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView == null) {
                convertView = mInflater.inflate(R.layout.rowlist, parent, false);
                holder = new ViewHolder();
                holder.Xtxt_ID           = (TextView) convertView.findViewById(R.id.txt_ID);
                holder.Xtxt_trx_num      = (TextView) convertView.findViewById(R.id.txt_trx_num);
                holder.Xtxt_ReceivedBy   = (TextView) convertView.findViewById(R.id.txt_ReceivedBy);
                holder.Xtxt_ReceivedDate = (TextView) convertView.findViewById(R.id.txt_ReceivedDate);
                holder.Xtxt_vehicle_no   = (TextView) convertView.findViewById(R.id.txt_vehicle_no);
                holder.Xtxt_location     = (TextView) convertView.findViewById(R.id.txt_location);

                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolder) convertView.getTag();
            }

            TextView Ttxt_ID             =  holder.Xtxt_ID;
            TextView Ttxt_trx_num        =  holder.Xtxt_trx_num;
            TextView Ttxt_ReceivedBy     =  holder.Xtxt_ReceivedBy;
            TextView Ttxt_ReceivedDate   =  holder.Xtxt_ReceivedDate;
            TextView Ttxt_vehicle_no     =  holder.Xtxt_vehicle_no;
            TextView Ttxt_location       =  holder.Xtxt_location;


            HolderManifestList o = getItem(position);

            Ttxt_ID.setText(o.getID().toString());
            Ttxt_trx_num.setText(o.getTrx_num().toString());
            Ttxt_ReceivedBy.setText(o.getReceivedBy().toString());
            Ttxt_ReceivedDate.setText(o.getReceivedDate().toString());
            Ttxt_vehicle_no.setText(o.getVehicle_no().toString());
            Ttxt_location.setText(o.getLocation().toString());


            /*
            if(modelItems[position].getValue() == 1)
                cb.setChecked(true);
            else
                cb.setChecked(false);
            */

            return convertView;
        }

    }
