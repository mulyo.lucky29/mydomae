package com.gudanggaramtbk.mydomae.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.gudanggaramtbk.mydomae.holder.HolderNoTrx;
import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.viewholder.ViewHolderNoTrx;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 5/2/2018.
 */

public class CustomListAdapterNoTrx extends ArrayAdapter<HolderNoTrx> {
    Context mcontext;
    List<HolderNoTrx> TrxList;
    ListView olistBinPersiapan;
    ArrayList<Boolean> positionArray;

    private LayoutInflater mInflater;
    public boolean checkBoxState[];

    public CustomListAdapterNoTrx (Context context, List<HolderNoTrx> list, ListView listBinPersiapan)
    {
        super(context,0,list);
        mcontext = context;
        TrxList = list;
        checkBoxState=new boolean[list.size()];
        olistBinPersiapan = listBinPersiapan;
        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        positionArray = new ArrayList<Boolean>(list.size());
        for(int i =0;i<list.size();i++){
            positionArray.add(false);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderNoTrx holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlisttrx, parent, false);
            holder = new ViewHolderNoTrx();
            holder.Xtxt_no_trx             = (TextView) convertView.findViewById(R.id.txt_no_trx);
            holder.Xtxt_bin_persiapan      = (TextView) convertView.findViewById(R.id.txt_bin_persiapan);
            holder.Xchk_select             = (CheckBox) convertView.findViewById(R.id.chk_no_trx);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderNoTrx) convertView.getTag();
            holder.Xchk_select.setOnCheckedChangeListener(null);
        }

        holder.Xchk_select.setFocusable(false);
        holder.Xchk_select.setChecked(positionArray.get(position));
        holder.Xchk_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()) {
                    olistBinPersiapan.setItemChecked(position,true);
                    v.setSelected(true);
                    positionArray.set(position, true);
                    Log.d("[GudangGaram]: ", "check (" + position + ") true");
                }
                else{
                    olistBinPersiapan.setItemChecked(position,false);
                    v.setSelected(false);
                    positionArray.set(position, false);
                    Log.d("[GudangGaram]: ", "check (" + position + ") false");
                }
            }
        });

        TextView Ttxt_no_trx              =  holder.Xtxt_no_trx;
        TextView Ttxt_bin_persiapan       =  holder.Xtxt_bin_persiapan;
        CheckBox Tchk_select              =  holder.Xchk_select;

        HolderNoTrx o = getItem(position);
        Ttxt_no_trx.setText(o.getNotrx().toString());
        Ttxt_bin_persiapan.setText(o.getBinpersiapan().toString());

        return convertView;
    }
}
