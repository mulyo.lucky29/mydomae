package com.gudanggaramtbk.mydomae.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.model.CEmployee;
import com.gudanggaramtbk.mydomae.viewholder.ViewHolderGEmployeeLov;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CustomGEmpLovAdapter extends ArrayAdapter<CEmployee> implements Filterable {
    private Context mcontext;
    private List<CEmployee> originalData = null;
    private List<CEmployee>           filteredData = null;
    private LayoutInflater mInflater;


    public CustomGEmpLovAdapter(Context context,  List<CEmployee> list) {
        super(context, 0, list);
        this.mcontext = context;
        this.filteredData  = list;
        this.originalData  = list;
        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }
    public CEmployee getItemAtPosition(int position){
        return filteredData.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderGEmployeeLov holder;
        Log.d("[GudangGaram]", "CustomGLocLovAdapter :: getView");

        // ------- initialized holder --------
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_lov_gemployee, parent, false);
            holder = new ViewHolderGEmployeeLov();
            holder.XPersonID      = (TextView) convertView.findViewById(R.id.lbl_glov_personID);
            holder.XPersonName    = (TextView) convertView.findViewById(R.id.lbl_glov_personName);
            holder.XPersonNIK     = (TextView) convertView.findViewById(R.id.lbl_glov_personNIK);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderGEmployeeLov) convertView.getTag();
        }

        // ------- assign data to holder ---------
        try{
            holder.XPersonID.setText(filteredData.get(position).getPersonID().toString());
            holder.XPersonName.setText(filteredData.get(position).getPersonName().toString());
            holder.XPersonNIK.setText(filteredData.get(position).getPersonNIK().toString());
        }
        catch(Exception e){
            //Log.d("[GudangGaram]", "CustomGLocLovAdapter :: getView Exception : " + e.getMessage().toString());
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                String FindWord = charText.toString().toLowerCase(Locale.getDefault());
                String filterableString;

                FilterResults filterResults = new FilterResults();
                final List<CEmployee> list = originalData;
                int count = list.size();
                final ArrayList<CEmployee> nlist = new ArrayList<CEmployee>(count);

                for(int i = 0; i< count; i++){
                    filterableString = list.get(i).getPersonName().toString() + list.get(i).getPersonNIK().toString();
                    if(filterableString.toLowerCase().contains(FindWord)){
                        nlist.add(list.get(i));
                    }
                }
                filterResults.values = nlist;
                filterResults.count  = nlist.size();

                return filterResults;
            };

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charText, FilterResults results) {
                Log.d("[GudangGaram]", "CustomGLocLovAdapter :: publishResults charText :: " + charText.toString());
                filteredData = (ArrayList<CEmployee>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
