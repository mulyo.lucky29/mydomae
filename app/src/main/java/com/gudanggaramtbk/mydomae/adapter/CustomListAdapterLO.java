package com.gudanggaramtbk.mydomae.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gudanggaramtbk.mydomae.holder.HolderManifestListLO;
import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.viewholder.ViewHolderLO;

import java.util.List;

/**
 * Created by luckym on 12/6/2017.
 */

public class CustomListAdapterLO extends ArrayAdapter<HolderManifestListLO> {
    Context                     mcontext;
    List<HolderManifestListLO>  LoList;
    private LayoutInflater      mInflater;

    public CustomListAdapterLO(Context context, List<HolderManifestListLO> list)
    {
        super(context,0,list);
        mcontext = context;
        LoList = list;
        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderLO holder;
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlistlo, parent, false);
            holder = new ViewHolderLO();
            holder.Xtxt_lo_vehicle_no    = (TextView) convertView.findViewById(R.id.txt_lo_vehicle_no);
            holder.Xtxt_lo_location      = (TextView) convertView.findViewById(R.id.txt_lo_location);
            holder.Xtxt_lo_RakPersiapan  = (TextView) convertView.findViewById(R.id.txt_lo_RakPersiapan);
            holder.Xtxt_lo_BinPersiapan  = (TextView) convertView.findViewById(R.id.txt_lo_BinPersiapan);
            holder.Xtxt_lo_trx_num       = (TextView) convertView.findViewById(R.id.txt_lo_trx_num);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderLO) convertView.getTag();
        }

        TextView Ttxt_lo_vehicle_no     =  holder.Xtxt_lo_vehicle_no;
        TextView Ttxt_lo_location       =  holder.Xtxt_lo_location;
        TextView Ttxt_lo_RakPersiapan   =  holder.Xtxt_lo_RakPersiapan;
        TextView Ttxt_lo_BinPersiapan   =  holder.Xtxt_lo_BinPersiapan;
        TextView Ttxt_lo_trx_num        =  holder.Xtxt_lo_trx_num;

        HolderManifestListLO o = getItem(position);

        Ttxt_lo_vehicle_no.setText(o.getVehicle_no().toString());
        Ttxt_lo_location.setText(o.getLocation().toString());
        Ttxt_lo_RakPersiapan.setText(o.getRakPersiapan().toString());
        Ttxt_lo_BinPersiapan.setText(o.getBinPersiapan().toString());
        Ttxt_lo_trx_num.setText(o.getTrx_num().toString());


        return convertView;
    }
}
