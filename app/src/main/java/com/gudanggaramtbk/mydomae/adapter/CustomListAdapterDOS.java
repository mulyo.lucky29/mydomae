package com.gudanggaramtbk.mydomae.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.graphics.Typeface;

import com.gudanggaramtbk.mydomae.holder.HolderDOS;
import com.gudanggaramtbk.mydomae.util.MySQLiteHelper;
import com.gudanggaramtbk.mydomae.R;
import com.gudanggaramtbk.mydomae.viewholder.ViewHolderDOS;


/**
 * Created by LuckyM on 10/10/2017.
 */

public class CustomListAdapterDOS extends BaseExpandableListAdapter {
    private Context                              _context;
    private List<String>                         header; // header titles
    private HashMap<String, List<HolderDOS>>     child;
    SparseBooleanArray                           mItemChecked;
    private Boolean                              BCheck;
    private MySQLiteHelper dbHelper;
    private HolderDOS o;
    private ArrayList<ArrayList<HolderDOS>>      oMatrix;
    public CustomListAdapterDOS(){}

    public CustomListAdapterDOS(Context context, List<String> listDataHeader, HashMap<String, List<HolderDOS>> listChildData, ArrayList<ArrayList<HolderDOS>> Matrix) {
        Log.d("[GudangGaram]", "CLADOS -> Constructor CListHeader :" + listDataHeader.size() + " CListChild :" + listChildData.size());
        this._context = context;
        this.header   = listDataHeader;
        this.child    = listChildData;
        this.BCheck   = false;
        this.oMatrix  = Matrix;
        dbHelper = new MySQLiteHelper(context);
        dbHelper.getWritableDatabase();

        // init arraylist load from query
        for(int i = 0; i < oMatrix.size() ; i++) {
            //Log.d("[GudangGaram]", "CLADOS -> " + oMatrix.get(0).size());
            for (int j = 0; j < oMatrix.get(i).size() ; j++){
                Log.d("[GudangGaram]", "CLADOS -> Matrix[" + i + "][" + j + "] :" + oMatrix.get(i).get(j).getSItemCode());
            }
        }
    }

    // ok
    @Override
    public int getGroupCount() {
        // Get header size
        Log.d("[GudangGaram]", "CLADOS -> getGroupCount : " + this.header.size());
        return this.header.size();
    }
    // ok
    @Override
    public long getGroupId(int groupPosition) {
        //Log.d("[GudangGaram]", "CLADOS -> getGroupId : " + groupPosition);
        return groupPosition;
    }
    // ok
    @Override
    public int getChildrenCount(int groupPosition) {
        // return children count
        Log.d("[GudangGaram]", "CLADOS -> getChildrenCount groupPosition (" + groupPosition + ") -> " + oMatrix.get(groupPosition).size());
        return this.child.get(this.header.get(groupPosition)).size();
    }

    // ok
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        //Log.d("[GudangGaram]", "CLADOS -> getChildID groupPosition : " + groupPosition + " childPosition : " + childPosition);
        return childPosition;
    }
    // ok
    @Override
    public Object getGroup(int groupPosition) {
        // Get header position
        Log.d("[GudangGaram]", "CLADOS -> getGroup groupPosition : " + groupPosition);
        return this.header.get(groupPosition);
    }
    // ok
    @Override
    public boolean hasStableIds() {
        return true;
        //return false;
    }
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        // This will return the child
        Log.d("[GudangGaram]:", "CLADOS -> getChild groupPosition : " + groupPosition + " childPosititon : " + childPosititon);
        return this.child.get(this.header.get(groupPosition)).get(childPosititon);
    }

    // ok
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // Getting header title
        TextView Group_Name_text;
        String headerTitle = (String) getGroup(groupPosition);
        Log.d("[GudangGaram]", "CLADOS -> getGroupView [" + groupPosition + "] : [" + isExpanded + "] = " + headerTitle);

        if (convertView == null) {
            // Inflating header layout and setting text
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //convertView = infalInflater.inflate(R.layout.list_dos_header, parent, false);
            convertView = infalInflater.inflate(R.layout.list_dos_header,null);
        }

        Group_Name_text = (TextView) convertView.findViewById(R.id.txt_Group_Name);
        Group_Name_text.setText(headerTitle);

        // If group is expanded then change the text into bold and change the icon
        if (isExpanded) {
            Log.d("[GudangGaram]", "CLADOS -> getGroupView Expand");
            Group_Name_text.setTypeface(null, Typeface.BOLD);
            Group_Name_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
        } else {
            Log.d("[GudangGaram]", "CLADOS -> getGroupView Collapse");
            // If group is not expanded then change the text back into normal and change the icon
            Group_Name_text.setTypeface(null, Typeface.NORMAL);
            Group_Name_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        }

        convertView.setTag(headerTitle);

        return convertView;
    }

    public void EH_CMD_XREASON_CODE_SELECTED(AdapterView<?> parent, View view, int position, long id, ViewHolderDOS hd, int groupPosition, int childPosition){
        String TReason = parent.getItemAtPosition(position).toString();
        String TStatus = "";
        Log.d("[GudangGaram]", " CLADOS -> EH_CMD_XREASON_CODE_SELECTED [" + groupPosition + "] [" + childPosition + "] [" + oMatrix.get(groupPosition).get(childPosition).getSItemCode() + "] Reason Selected = " + TReason + " Position = " + position);

        // if reason except blank is selected
        if(hd.XCHECK.isChecked() == true){
            TStatus = "Y";
            // if XQTYR = XQTY
            if(Float.compare(Float.parseFloat(hd.XQTYR.getText().toString()), Float.parseFloat(hd.XQTY.getText().toString())) == 0) {
                 oMatrix.get(groupPosition).get(childPosition).setSReasonCode(" ");
                 oMatrix.get(groupPosition).get(childPosition).setSReasonIndex(0);
            }
            else{
                 oMatrix.get(groupPosition).get(childPosition).setSReasonCode(TReason);
                 oMatrix.get(groupPosition).get(childPosition).setSReasonIndex(position);
            }
        }
        else {
            TStatus = "";
            oMatrix.get(groupPosition).get(childPosition).setSReasonCode(TReason);
            oMatrix.get(groupPosition).get(childPosition).setSReasonIndex(position);
        }

        hd.XREASON_CODE.setSelection(oMatrix.get(groupPosition).get(childPosition).getSReasonIndex());

        // update to database reason and check
        dbHelper.UpdateDOSReason(oMatrix.get(groupPosition).get(childPosition).getSID().toString(),TReason, TStatus);
        Log.d("[GudangGaram]", "Combo Reason Selected Item New (" + oMatrix.get(groupPosition).get(childPosition).getSID().toString() + ") >> " + TReason);
    }

    public void EH_CMD_CHECK(View v, ViewHolderDOS hd, int groupPosition, int childPosition){
        Boolean selection;
        String TStatus = "";
        CheckBox cb = (CheckBox) v ;
        //Log.d("[GudangGaram]", "Qty Received On Line " + hd.XQTYR.getText().toString());
        Log.d("[GudangGaram]", "CLADOS -> EH_CMD_CHECK :: Check Position  (" + groupPosition + ") (" + childPosition + ") " + "[ " + oMatrix.get(groupPosition).get(childPosition).getSItemCode().toString() + " ] :: Qty Received --> "  + oMatrix.get(groupPosition).get(childPosition).getSQtyR());

        selection = ((CheckBox) v).isChecked();

        if(oMatrix.get(groupPosition).get(childPosition).getSReasonIndex() > 0){
              oMatrix.get(groupPosition).get(childPosition).setSReasonCode(" ");
              oMatrix.get(groupPosition).get(childPosition).setSReasonIndex(0);
              hd.XREASON_CODE.setSelection(0);
        }

        if(selection == true){
            // if checked :
            // then check if inputted value not the same with existing data record
            if(Float.compare(Float.parseFloat(hd.XQTYR.getText().toString()),Float.parseFloat(oMatrix.get(groupPosition).get(childPosition).getSQtyR().toString())) == 0) {
                // copy data matrix QTYR = QTY
                oMatrix.get(groupPosition).get(childPosition).setSQtyR(oMatrix.get(groupPosition).get(childPosition).getSQty());
            }
            else{
                // set data record with existing inputted value
                oMatrix.get(groupPosition).get(childPosition).setSQtyR(Float.parseFloat(hd.XQTYR.getText().toString()));
            }
            // assign value QTYR
            hd.XQTYR.setText(oMatrix.get(groupPosition).get(childPosition).getSQtyR().toString());
            hd.XQTYR.requestFocus();

            oMatrix.get(groupPosition).get(childPosition).setEnableReason(false);
            // set checked flag
            child.get(header.get(groupPosition)).get(childPosition).setSCheck(true);
            oMatrix.get(groupPosition).get(childPosition).setSCheck(true);
        }
        else{
            // if uncheck :
            // then record set data matrix QTYR set to 0
            oMatrix.get(groupPosition).get(childPosition).setSQtyR(0.0);
            // assign 0 value to QTYR
            hd.XQTYR.setText(oMatrix.get(groupPosition).get(childPosition).getSQtyR().toString());
            // need mention reason if unselected
            oMatrix.get(groupPosition).get(childPosition).setEnableReason(true);
            // set record set checked flag to false
            oMatrix.get(groupPosition).get(childPosition).setSCheck(false);
            child.get(header.get(groupPosition)).get(childPosition).setSCheck(false);
        }

        if(oMatrix.get(groupPosition).get(childPosition).getSCheck() == true){
            TStatus = "Y";
        }
        else{
            TStatus = "";
        }

        hd.XQTYR.setEnabled(selection);
        hd.XREASON_CODE.setEnabled(oMatrix.get(groupPosition).get(childPosition).getEnableReason());

        dbHelper.UpdateDOSFlag(oMatrix.get(groupPosition).get(childPosition).getSID(),TStatus);
        Log.d("[GudangGaram]", " Group [" + groupPosition + "] [" + childPosition + "] [" + oMatrix.get(groupPosition).get(childPosition).getSItemCode() + "] >> " + oMatrix.get(groupPosition).get(childPosition).getSID() + " : " + TStatus);
    }


    public void EH_CMD_XQTYR_CHANGE(View v, ViewHolderDOS hd, int groupPosition, int childPosition){
        Boolean enabled_reason;
        EditText et = (EditText) v ;
        String tmpVal = "0.0";
        Log.d("[GudangGaram]", "CLADOS -> XQTYR [" + groupPosition + "] [" + childPosition + "] + [" + oMatrix.get(groupPosition).get(childPosition).getSItemCode() + " = " + et.getText());

        enabled_reason = false;
        try{
            if(Float.compare(Float.parseFloat(et.getText().toString()),0) == 0){
                tmpVal = "0.0";
                et.setEnabled(false);
                // untick
                hd.XCHECK.setChecked(false);
                oMatrix.get(groupPosition).get(childPosition).setSCheck(false);
                oMatrix.get(groupPosition).get(childPosition).setEnableReason(true);
            }
            else if(Float.compare(Float.parseFloat(et.getText().toString()), 0) < 0){
                tmpVal = oMatrix.get(groupPosition).get(childPosition).getSQty().toString();
                oMatrix.get(groupPosition).get(childPosition).setEnableReason(false);
            }
            else{
                int retval = Float.compare(Float.parseFloat(et.getText().toString()), Float.parseFloat(oMatrix.get(groupPosition).get(childPosition).getSQty().toString()));
                if (retval == 0) {
                    tmpVal = et.getText().toString();
                    oMatrix.get(groupPosition).get(childPosition).setEnableReason(false);
                }
                else if(retval < 0){
                    tmpVal = et.getText().toString();
                    oMatrix.get(groupPosition).get(childPosition).setEnableReason(true);
                }
                else if(retval > 0){
                    tmpVal = oMatrix.get(groupPosition).get(childPosition).getSQty().toString();
                    oMatrix.get(groupPosition).get(childPosition).setEnableReason(false);
                }
            }
        }
        catch(Exception e){
            tmpVal = oMatrix.get(groupPosition).get(childPosition).getSQty().toString();
            oMatrix.get(groupPosition).get(childPosition).setEnableReason(false);
        }
        finally {
            Log.d("[GudangGaram]", "CLADOS -> XQTYR [" + groupPosition + "] [" + childPosition + "] + [" + oMatrix.get(groupPosition).get(childPosition).getSItemCode() + " = " + et.getText() + " tmpVal = " + tmpVal);
            oMatrix.get(groupPosition).get(childPosition).setSQtyR(Float.parseFloat(tmpVal));
            dbHelper.UpdateDOSQtyReceived(oMatrix.get(groupPosition).get(childPosition).getSID(),tmpVal);
            et.setText(tmpVal);
            hd.XREASON_CODE.setEnabled(oMatrix.get(groupPosition).get(childPosition).getEnableReason());
        }
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ViewHolderDOS hd;

        if(convertView == null){
            Log.d("[GudangGaram]", "CLADOS -> getChildView [" + groupPosition + "][" + childPosition + "] [" + oMatrix.get(groupPosition).get(childPosition).getSItemCode() + "]");
            // Inflating child layout and setting textview
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //convertView = infalInflater.inflate(R.layout.list_dos_lines, parent,false);
            convertView = infalInflater.inflate(R.layout.list_dos_lines,null);

            // init view holder to available to hold value
            hd = new ViewHolderDOS();
            hd.XID          = (TextView) convertView.findViewById(R.id.txt_lvdos_ids);
            hd.XITEM_CODE   = (TextView) convertView.findViewById(R.id.txt_lvdos_itemCode);
            hd.XITEM_DESC   = (TextView) convertView.findViewById(R.id.txt_lvdos_itemDesc);
            hd.XQTY         = (TextView) convertView.findViewById(R.id.txt_lvdos_qty);
            hd.XQTYR        = (EditText) convertView.findViewById(R.id.txt_lvdos_qtyR);
            hd.XUOM         = (TextView) convertView.findViewById(R.id.txt_lvdos_uom);
            hd.XCHECK       = (CheckBox) convertView.findViewById(R.id.chk_lvdos_check);
            hd.XREASON_CODE = (Spinner)  convertView.findViewById(R.id.cbo_lvdos_reason);

            hd.XQTYR.setFocusableInTouchMode(true);

            // populate value to holder
            initComboBox(hd.XREASON_CODE,initListCBReason());
        }
        else{
            hd = (ViewHolderDOS) convertView.getTag();
            hd.XCHECK.setOnClickListener(null);
            hd.XREASON_CODE.setOnItemSelectedListener(null);
        }

        // ============= add Event Listener =====================
        hd.XCHECK.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EH_CMD_CHECK(v,hd, groupPosition,childPosition);
            }
        });

        hd.XREASON_CODE.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 EH_CMD_XREASON_CODE_SELECTED(parent, view,position, id, hd, groupPosition,childPosition);
            }
        });

        hd.XQTYR.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // When focus is lost check that the text field has valid values.
                if (!hasFocus) {
                    EH_CMD_XQTYR_CHANGE(v, hd, groupPosition, childPosition);
                }
            }
        });

        if(oMatrix.get(groupPosition).get(childPosition).getSReasonIndex() == null){
            oMatrix.get(groupPosition).get(childPosition).setSReasonIndex(0);
            hd.XREASON_CODE.setSelection(0);
        }

        // value assignment to the detail slot
        hd.XID.setText(oMatrix.get(groupPosition).get(childPosition).getSID());
        hd.XITEM_CODE.setText(oMatrix.get(groupPosition).get(childPosition).getSItemCode());
        hd.XITEM_DESC.setText(oMatrix.get(groupPosition).get(childPosition).getSItemDesc());
        hd.XQTY.setText(oMatrix.get(groupPosition).get(childPosition).getSQty().toString());
        hd.XQTYR.setText(oMatrix.get(groupPosition).get(childPosition).getSQtyR().toString());
        hd.XQTYR.setEnabled(oMatrix.get(groupPosition).get(childPosition).getSCheck());

        hd.XUOM.setText(oMatrix.get(groupPosition).get(childPosition).getSUOM());
        hd.XCHECK.setChecked(oMatrix.get(groupPosition).get(childPosition).getSCheck());
        hd.XREASON_CODE.setSelection(oMatrix.get(groupPosition).get(childPosition).getSReasonIndex());

        try{
            hd.XREASON_CODE.setEnabled(oMatrix.get(groupPosition).get(childPosition).getEnableReason());
        }
        catch(Exception e){
        }

        convertView.setTag(hd);

        return convertView;
    }

    private List<String> initListCBReason(){
        List<String> olist = new ArrayList<String>();
        olist = dbHelper.getLOV("GGGG_INV_REJECT_REASON_DO");
        return olist;
    }

    private void initComboBox(Spinner spx, List<String> lvi)
    {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(_context, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spx.setAdapter(dataAdapter);
    }

}
